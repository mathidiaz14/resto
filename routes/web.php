<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\BusinessController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ArchingController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CommandController;
use App\Http\Controllers\PrinterController;
use App\Http\Controllers\ReservationController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\SupplierController;
use App\Http\Controllers\TicketController;
use App\Http\Controllers\TableController;
use App\Http\Controllers\RoomController;
use App\Http\Controllers\DeliveryController;
use App\Http\Controllers\FixedController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\DiscountController;
use App\Http\Controllers\RetirementController;
use App\Http\Controllers\NotificationController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () 
{
    return view('welcome');
});

Route::get('show/tables/{id}' 		, function($id)
{
	$room = App\Models\Room::find($id);
    
    if($room != null)
    	return view('table.parts.show_tables', compact('room'));
});

Route::get('consult/tables' 		, function()
{
	return Auth::user()->business->fixeds->where('status','occupied')->count();
});

Route::get('show/tabs/{id}'			, function($id)
{
	$room = App\Models\Room::find($id);
    return view('table.parts.tabs', compact('room'));
});

Route::get('show/commands/{id}'			, function($id)
{
	$fixed 		= App\Models\Fixed::find($id);
	$table      = $fixed->tables->where('status', 'enabled')->first();
    
    return view('table.parts.command_list', compact('table'));
});

Auth::routes();

Route::get('business/check/{id}'			, [BusinessController::class, 'check']);

Route::get('/home'							, [HomeController::class, 'index'])->middleware('waiter');
Route::get('/admin'							, [HomeController::class, 'admin'])->middleware('waiter');

Route::resource('business' 					, BusinessController::class)->middleware('admin');
Route::resource('category'	 				, CategoryController::class)->middleware('cashier');
Route::resource('reservation' 				, ReservationController::class)->middleware('waiter');
Route::resource('setting' 					, SettingController::class)->middleware('cashier');
Route::resource('supplier' 					, SupplierController::class)->middleware('cashier');
Route::resource('ticket' 					, TicketController::class)->middleware('waiter');
Route::resource('room' 						, RoomController::class)->middleware('waiter');
Route::resource('delivery'					, DeliveryController::class)->middleware('cashier');
Route::resource('product' 		 			, ProductController::class)->middleware('cashier');
Route::resource('client' 					, ClientController::class)->middleware('waiter');
Route::resource('user' 						, UserController::class)->middleware('admin');
Route::resource('discount'					, DiscountController::class)->middleware('cashier');

Route::resource('arching' 					, ArchingController::class)->middleware('cashier');
Route::get('arching/print/{id}'				, [ArchingController::class, 'print'])->middleware('cashier');
Route::get('arching/table/{id}'				, [ArchingController::class, 'show_table'])->middleware('cashier');

Route::resource('printer' 					, PrinterController::class)->middleware('cashier');
Route::get('printer/test/{ID}'				, [PrinterController::class, 'test'])->middleware('cashier');

Route::resource('retirement'				, RetirementController::class)->middleware('cashier');
Route::get('retirement/print/{ID}'			, [RetirementController::class, 'print'])->middleware('waiter');
Route::get('retirement/print/arching/{ID}'	, [RetirementController::class, 'print_arching'])->middleware('waiter');

Route::get('notification/query'				, [NotificationController::class, 'query'])->middleware('cashier');
Route::get('notification/all'				, [NotificationController::class, 'all'])->middleware('cashier');
Route::get('notification/read'				, [NotificationController::class, 'read'])->middleware('cashier');
Route::resource('notification'				, NotificationController::class)->middleware('cashier');

Route::post('search' 			 			, [FixedController::class, 'search'])->middleware('waiter');
Route::get('account'      					, [HomeController::class, 'account'])->middleware('waiter');
Route::post('contact'						, [HomeController::class, 'contact']);
Route::post('client/save'					, [ClientController::class, 'save'])->middleware('waiter');
Route::get('product/get/{id}'				, [ProductController::class, 'get'])->middleware('waiter');

Route::resource('fixed'						, FixedController::class)->middleware('admin');
Route::get('fixed/get/{id}'					, [FixedController::class, 'get'])->middleware('waiter');
Route::get('fixed/form/{id}/{type}'			, [FixedController::class, 'form'])->middleware('cashier');
Route::post('fixed/edit/name'				, [FixedController::class, 'edit_name'])->middleware('cashier');

Route::resource('table' 					, TableController::class)->middleware('waiter');
Route::get('table/history/{id}' 			, [TableController::class, 'history'])->middleware('waiter');
Route::get('table/close/{id}'      			, [TableController::class, 'close'])->middleware('cashier');
Route::get('table/charge/{id}'     			, [TableController::class, 'get_charge'])->middleware('cashier');
Route::post('table/charge/{id}'   			, [TableController::class, 'set_charge'])->middleware('cashier');
Route::get('table/print/{id}'      			, [TableController::class, 'print'])->middleware('cashier');
Route::post('table/discount/{id}'      		, [TableController::class, 'discount'])->middleware('cashier');

Route::resource('command' 					, CommandController::class)->middleware('waiter');
Route::get('command/print/{id}'    			, [CommandController::class, 'print'])->middleware('waiter');
Route::get('command/check/{id}' 			, [CommandController::class, 'check'])->middleware('waiter');
Route::post('command/delete/all' 			, [CommandController::class, 'delete_all'])->middleware('waiter');
Route::post('command/observation' 			, [CommandController::class, 'observation'])->middleware('waiter');

