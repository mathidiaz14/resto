<!DOCTYPE HTML>
<!--
    Projection by TEMPLATED
    templated.co @templatedco
    Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
    <head>
        <title>Resto.uy</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="{{asset('landing/assets/css/main.css')}}" />
        <link rel="shortcut icon" type="image/png" href="{{asset('images/logo_negro.png')}}">
    </head>
    <body>

        <!-- Header -->
            <header id="header">
                <div class="inner">
                    <a href="{{url('home')}}" class="logo"><strong>Resto</strong>.uy</a>
                    <nav id="nav">
                        @if(Auth::check())
                            <a href="{{url('home')}}">Ingresar</a> 
                        @else
                            <a href="{{url('login')}}">Iniciar Sesión</a>
                            <a href="{{url('register')}}">Registrarse</a>
                        @endif
                    </nav>
                    <a href="#navPanel" class="navPanelToggle"><span class="fa fa-bars"></span></a>
                </div>
            </header>

        <!-- Banner -->
            <section id="banner">
                <div class="inner">
                    <header>
                        <img src="{{asset('images/logo.png')}}" width="300px" alt="">
                        <br><br>
                        <h1>Gestion de restaurantes en la nube</h1>
                    </header>

                    <div class="flex ">

                        <div>
                            <span class="icon fa-fighter-jet"></span>
                            <h3>Rapido</h3>
                            <p>Registrate y comienza en 5 minutos.</p>
                        </div>

                        <div>
                            <span class="icon fa-wifi"></span>
                            <h3>Inalambrico</h3>
                            <p>Solo necesitas un smartphone.</p>
                        </div>

                        <div>
                            <span class="icon fa-cloud"></span>
                            <h3>En la nube</h3>
                            <p>Tus datos siempre accesibles y seguros.</p>
                        </div>

                    </div>

                    <footer>
                        <a href="{{url('register')}}" class="button">Empieza ahora</a>
                    </footer>
                </div>
            </section>


        <!-- Three -->
            <section id="three" class="wrapper align-center">
                <div class="inner">
                    <div class="flex flex-2">
                        <article>
                            <div class="image round">
                                <img src="{{asset('landing/images/pic01.jpg')}}" alt="Pic 01" />
                            </div>
                            <header>
                                <h3>Control de inventario</h3>
                            </header>
                            <p>Con nuestro sistema podras realizar un exhaustivo seguimiento de tu stock, ya que te avisara cundo llegues al minimo deseado de cada producto.</p>
                        </article>
                        <article>
                            <div class="image round">
                                <img src="{{asset('landing/images/pic02.jpg')}}" alt="Pic 02" />
                            </div>
                            <header>
                                <h3>Sin equipo especializado</h3>
                            </header>
                            <p>El sistema no necesita instalación, solo registrate y empieza a utilizarlo. Si necesitas imprimir comandas, bajate nuestra APP y empieza a imprimir en 5 minutos.</p>
                        </article>
                    </div>
                </div>
            </section>

            <section class="wrapper align-center" style="background: #5d5959; color: white;">
                <div class="inner">
                    <article>
                        <div class="image round">
                            <img src="{{asset('landing/images/download.png')}}" width="100px" />
                        </div>
                        <header>
                            <h3 style="color:white;">RestoAPP Print APP</h3>
                        </header>
                        <p>Descarga nuestra app y podras imprimir en cualquier impresora conectada a cualquier pc, solo registra la impresora y automaticamente la veras en el sistema.</p>
                        <footer>
                            <a href="#" class="button">Descargar</a>
                        </footer>
                    </article>
                </div>
            </section>

        <!-- Footer -->
            <footer id="footer">
                <div class="inner">

                    <h3>Escribenos</h3>
                
                    @if(session('message') != null)
                        <span class="alert-message">{{session('message')}}</span>    
                        <br><br>
                        {{session()->forget('message')}}
                    @endif
                    
                    <form action="{{url('contact')}}" method="post">
                        @csrf
                        <div class="field half first">
                            <label for="name">Nombre</label>
                            <input name="name" id="name" type="text" placeholder="Nombre">
                        </div>
                        <div class="field half">
                            <label for="email">Email</label>
                            <input name="email" id="email" type="email" placeholder="Email">
                        </div>
                        <div class="field">
                            <label for="message">Mensaje</label>
                            <textarea name="message" id="message" rows="6" placeholder="Mensaje"></textarea>
                        </div>
                        <ul class="actions">
                            <li><input value="Enviar" class="button alt" type="submit"></li>
                        </ul>
                    </form>

                    <div class="copyright">
                        Created By <a href="http://mathiasdiaz.uy">MathiasDiaz.uy</a>
                    </div>

                </div>
            </footer>

        <!-- Scripts -->
            <script src="{{asset('landing/assets/js/jquery.min.js')}}"></script>
            <script src="{{asset('landing/assets/js/skel.min.js')}}"></script>
            <script src="{{asset('landing/assets/js/util.js')}}"></script>
            <script src="{{asset('landing/assets/js/main.js')}}"></script>

    </body>
</html>