@php 
    $notifications 	= Auth::user()->business->notifications->where('status', '!=', 'deleted')->sortBydesc('created_at')->take(5);
    $enabled 		= Auth::user()->business->notifications->where('status', 'enabled');
@endphp

<input type="hidden" id="notification_quantity" value="{{Auth::user()->business->notifications->where('status', 'enabled')->count()}}">

<a class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
    <i class="fas fa-envelope"></i>
    @if(count($enabled) > 0)
        <span class="badge bg-green">{{count($enabled)}}</span>
    @endif
</a>

<ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
    @foreach($notifications as $notification)
        <li @if($notification->status != "disabled") style="border:solid 3px #1abb9cf2;" @endif>
            <a href="{{url('notification', $notification->id)}}">
                <span>
                    <span>{{$notification->title}}</span>
                    <span class="time">{{$notification->created_at->format('d/m/Y H:i')}}</span>
                </span>
                <span class="message">
                    {{$notification->description}}
                </span>
            </a>
        </li>
    @endforeach
    <li>
        <div class="text-center">
            <a href="{{url('notification')}}">
                <strong>Ver mas...</strong>
                <i class="fas fa-angle-right"></i>
            </a>
        </div>
    </li>
</ul>