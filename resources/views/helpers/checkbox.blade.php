<label>
    <input type="checkbox" class="js-switch" name="{{$name}}"  id="{{$name}}"
    @if($status == 'checked')
         checked
    @endif />
    {{$text}}
</label>