<div class='input-group'>
    <select name="{{$name}}" id="{{$name}}" class="form-control" @if($status == 'disabled') readonly @endif>
    	@php
			$hour = \Carbon\Carbon::today();
    	@endphp

    	@for($i = 0; $i < (1440 / $min); $i++)
    		@if($hour->format('H:i') == $value)
    			<option value="{{$hour->format('H:i')}}" selected>{{$hour->format('H:i')}}</option>
    		@else
    			<option value="{{$hour->format('H:i')}}">{{$hour->format('H:i')}}</option>
    		@endif

    		@php
				$hour->addMinutes($min);
    		@endphp
    	@endfor
    </select>
    <span class="input-group-addon">
       <i class="fa fa-clock"></i>
    </span>
</div>