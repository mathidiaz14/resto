@php
	$day    = $user->hours->where('day', $name)->first();
	$hour   = \Carbon\Carbon::today();
@endphp

@for($i = 0; $i <= 47; $i++)
	@php 
		$var = str_replace(":", "", $hour->format('H:i'));
	@endphp

	<td class="day @if($day[$var] == 'yes') green-hour @endif " attr-id="{{$name}}_{{$var}}">
		<input type="hidden" name="{{$name}}_{{$var}}" id="{{$name}}_{{$var}}" value="@if($day[$var] == 'yes') yes @else no @endif">
		{{$hour->format('H:i')}}
	</td>

	@php 
		$hour->addMinutes(30); 
	@endphp
@endfor
