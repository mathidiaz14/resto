<!-- Button trigger modal -->
	<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete_{{$id}}">
		<i class="fa fa-trash"></i>
	</button>

	<!-- Modal -->
	<div class="modal fade" id="delete_{{$id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header bg-red-color">
					<div class="col-xs-12 text-right">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<div class="col-xs-12 text-center">
						<i class="fa fa-times-circle fa-3x" style="color:white;"></i>
						<br><br>
					</div>
				</div>
				<div class="modal-body text-center">
					<h3>@lang('message.delete_question')</h3>
				</div>
				<div class="modal-footer">
					<div class="col-xs-6">
						<button type="button" class="btn btn-default btn-block" data-dismiss="modal">@lang('message.no')</button>
					</div>
					<div class="col-xs-6">
						<form action="{{ route($route, $id) }}" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="DELETE">    
                            
                            <button type="submit" class="btn btn-danger btn-block">   
                                @lang('message.yes')
                            </button>
                        </form>
					</div>
				</div>
			</div>
		</div>
	</div>