<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ticket</title>
	<meta charset="UTF-8">
	<meta http-equiv="x-ua-compatible" content="IE=9">
	<style>
		hr
		{
			border: solid 2px black!important
		}

		body
		{
			font-family: monospace, monospace;
			font-size: .8em;
		}

		.text-center
		{
			text-align: center;
		}

		.text-left
		{
			text-align: left;
		}

		.text-left
		{
			text-align: left;
		}

		.col-xs-8
		{
			float:left;
			width: 70%;
		}

		.col-xs-2
		{
			float:left;
			width: 15%;
		}
		
		.col-xs-12
		{
			float:left;
			width: 100%;
		}

		.row:before,
		.row:after {
			display: table;
			content: " ";
		}

		.row:after {
		  clear: both;
		}

	</style>
</head>
<body>
	<!-- caja -->
	<div class="row">
		<div class="col-xs-12 text-center">
			<h2>
				Cierre de caja
			</h2>
			<p>
				<b>Desde:</b> 24/02/2017 18:17:32 <br>
				<b>Hasta:</b> 25/02/2017 02:07:50
			</p>
		</div>
	</div>
		
	<hr>

	<div class="row">
		<div class="col-xs-8 text-left">
			Cantidad de mesas:
		</div>
		<div class="col-xs-2 text-left">
			<b>6</b>
		</div>

		<div class="col-xs-8 text-left">
			Saldo inicial:
		</div>
		<div class="col-xs-2 text-left">
			<b>$650</b>
		</div>

		<div class="col-xs-8 text-left">
			Saldo final en caja:
		</div>
		<div class="col-xs-2 text-left">
			<b>$1770</b>
		</div>
		
		<div class="col-xs-8 text-left">
			Saldo final en APP:
		</div>
		<div class="col-xs-2 text-left">
			<b>$1770</b>
		</div>

		<div class="col-xs-8 text-left">
			Diferencia:
		</div>
		<div class="col-xs-2 text-left">
			<b>$0</b>
		</div>
	</div>

	<hr>

	<div class="row">
		<div class="col-xs-12 text-center">
			<b>Ingresos</b>
		</div>
		<br>

		<div class="col-xs-8">
			Efectivo:
		</div>
		<div class="col-xs-2">
			<b>$2350</b>
		</div>
		<div class="col-xs-8">
			Debito:
		</div>
		<div class="col-xs-2">
			<b>$1030</b>
		</div>
		<div class="col-xs-8">
			Credito:
		</div>
		<div class="col-xs-2">
			<b>$650</b>
		</div>
		<div class="col-xs-8">
			Bonos:
		</div>
		<div class="col-xs-2">
			<b>$0</b>
		</div>
		<div class="col-xs-8">
			Cheques:
		</div>
		<div class="col-xs-2">
			<b>$0</b>
		</div>
		<div class="col-xs-8">
			Tickets:
		</div>
		<div class="col-xs-2">
			<b>$0</b>
		</div>
	</div>

		<hr>

	<div class="row">
		<div class="col-xs-12 text-center">
			<b>Egresos</b>
		</div>
		<br>

		<div class="col-xs-8">
			Combustible:
		</div>
		<div class="col-xs-2">
			<b>$1000</b>
		</div>
		<div class="col-xs-8">
			Productos extra:
		</div>
		<div class="col-xs-2">
			<b>$230</b>
		</div>
	</div>

	<div class="col-xs-12 text-center">
		<p>-- Fin de caja --</p>
	</div>
	<!-- Fin caja -->
	
</body>
</html>


