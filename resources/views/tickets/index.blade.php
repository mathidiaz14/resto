            <!DOCTYPE html>
<html lang='es'>
<head>
    <title>Ticket</title>
    <meta charset='UTF-8'>
    <meta http-equiv='x-ua-compatible' content='IE=9'>
    <style>
        hr
        {
            border: solid 2px black!important
        }

        body
        {
            font-family: monospace, monospace;
            font-size: .8em;
        }

        .text-center
        {
            text-align: center;
        }

        .text-left
        {
            text-align: left;
        }

        .text-left
        {
            text-align: left;
        }

        .col-xs-8
        {
            float:left;
            width: 70%;
        }

        .col-xs-2
        {
            float:left;
            width: 15%;
        }
        
        .col-xs-12
        {
            float:left;
            width: 100%;
        }

        .row:before,
        .row:after {
            display: table;
            content: ' ';
        }

        .row:after {
          clear: both;
        }

    </style>
</head>
<body>
    {!! $content !!}
</body>
</html>