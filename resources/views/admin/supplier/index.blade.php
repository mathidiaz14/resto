@extends('layouts.dashboard')

@section('title')
	@lang('message.suppliers')
@endsection

@section('content')

<div class="row">
	<div class="col-xs-12">
      	<div class="x_panel">
        	<div class="x_title">
        		<div class="row">
        			<div class="col-xs-6">
		          		<h2>@lang('message.suppliers')</h2>
		          		<div class="clearfix"></div>
        			</div>
        			<div class="col-xs-6">
        				<a href="{{url('admin')}}" class="btn btn-info">
	          				<i class="fa fa-chevron-left"></i>
	          				@lang('message.back')
	          			</a>
        			</div>
        		</div>
        	</div>
        	<div class="x_content">
          		<div class="table-responsive">
          			<table class="table table-striped">
          				<tr>
          					<th>@lang('message.name')</th>
          					<th>@lang('message.phone')</th>
          					<th>@lang('message.email')</th>
          					<th>@lang('message.view')</th>
          					<th>@lang('message.edit')</th>
          					<th>@lang('message.delete')</th>
          				</tr>
          				@foreach($suppliers as $supplier)
      							<tr>
      								<td>{{$supplier->name}}</td>
      								<td>{{$supplier->phone}}</td>
      								<td>{{$supplier->email}}</td>
      								<td>
      									<a href="{{url('supplier', $supplier->id)}}" class="btn btn-success">
      										<i class="fa fa-eye"></i>	
      									</a>
      								</td>
      								<td>
      									<a href="{{route('supplier.edit', $supplier->id)}}" class="btn btn-primary">
      										<i class="fa fa-edit"></i>
      									</a>
      								</td>
      								<td>
      									@include('helpers.delete', ['id' => $supplier->id, 'route' => 'supplier.destroy'])
      								</td>
      							</tr>
      						@endforeach
          			</table>
          		</div>
          	</div>
          	<div class="x_footer">
          		<div class="col-xs-6">
          			
          		</div>
          		<div class="col-xs-6 text-right">
					<a href="{{url('supplier/create')}}" class="btn btn-info">
						<i class="fa fa-plus"></i>
						@lang('message.create')
					</a>
          		</div>
          	</div>
        </div>
  	</div>
</div>
@endsection
