@extends('layouts.dashboard')

@section('title')
	@lang('message.suppliers')
@endsection

@section('content')

<div class="row">
	<div class="col-xs-12">
      	<div class="x_panel">
        	<div class="x_title">
          		<h2>@lang('message.suppliers_edit')</h2>
          		<div class="clearfix"></div>
        	</div>
        	<div class="x_content">
          		<form action="{{url('supplier', $supplier->id)}}" method="POST" class="form-horizontal col-xs-12 col-md-10 col-md-offset-1">
          			@csrf
                <input type="hidden" name="_method" value="PATCH">
          			<div class="form-group">
          				<label for="">@lang('message.name')</label>
          				<input type="text" name="name" class="form-control" value="{{$supplier->name}}">
          			</div>
          			<div class="form-group">
          				<label for="">@lang('message.rut')</label>
          				<input type="text" name="rut" class="form-control" value="{{$supplier->rut}}">
          			</div>
          			<div class="form-group">
          				<label for="">@lang('message.address')</label>
          				<input type="text" name="address" class="form-control" value="{{$supplier->address}}">
          			</div>
          			<div class="form-group">
          				<label for="">@lang('message.email')</label>
          				<input type="email" name="email" class="form-control" value="{{$supplier->email}}">
          			</div>
          			<div class="form-group">
          				<label for="">@lang('message.phone')</label>
          				<input type="text" name="phone" class="form-control" value="{{$supplier->phone}}">
          			</div>
          			<div class="form-group">
          				<button class="btn btn-block btn-info">
          					<i class="fa fa-save"></i>
          					@lang('message.save')
          				</button>
          			</div>
          		</form>
          	</div>
          	<div class="x_footer">
          		<div class="col-xs-6">
          			<a href="{{URL::Previous()}}" class="btn btn-info">
          				<i class="fa fa-chevron-left"></i>
          				@lang('message.back')
          			</a>
          		</div>
          	</div>
        </div>
  	</div>
</div>
@endsection
