@extends('layouts.dashboard')

@section('title')
	@lang('message.suppliers')
@endsection

@section('content')

<div class="row">
	<div class="col-xs-12">
  	<div class="x_panel">
    	<div class="x_title">
        <div class="row">
          <div class="col-xs-6">
        		<h2>@lang('message.suppliers')</h2>
        		<div class="clearfix"></div>
          </div>
          <div class="col-xs-6">
            <a href="{{url('supplier')}}" class="btn btn-info">
              <i class="fa fa-chevron-left"></i>
              @lang('message.back')
            </a>
          </div>
        </div>
    	</div>
    	<div class="x_content">
    		<form class="form-horizontal col-xs-12 col-md-10 col-md-offset-1">
    			<div class="form-group">
    				<label for="">@lang('message.name')</label>
    				<input type="text" name="name" class="form-control" value="{{$supplier->name}}" readonly>
    			</div>
    			<div class="form-group">
    				<label for="">@lang('message.rut')</label>
    				<input type="text" name="rut" class="form-control" value="{{$supplier->rut}}" readonly>
    			</div>
    			<div class="form-group">
    				<label for="">@lang('message.address')</label>
    				<input type="text" name="address" class="form-control" value="{{$supplier->address}}" readonly>
    			</div>
    			<div class="form-group">
    				<label for="">@lang('message.email')</label>
    				<input type="email" name="email" class="form-control" value="{{$supplier->email}}" readonly>
    			</div>
    			<div class="form-group">
    				<label for="">@lang('message.phone')</label>
    				<input type="text" name="phone" class="form-control" value="{{$supplier->phone}}" readonly>
    			</div>
    		</form>
        <div class="col-xs-10 col-xs-offset-1">
          <br><br>
          <h2>@lang('message.products_of'): <b>{{$supplier->name}}</b></h2>
          <div class="clearfix"></div>
          <div class="table-responsive">
            <table class="table table-striped">
              <th>@lang('message.name')</th>
              <th>@lang('message.quantity')</th>
              <th>@lang('message.price')</th>
              <th>@lang('message.view')</th>
              <th>@lang('message.edit')</th>
              <th>@lang('message.delete')</th>
              
              @foreach($supplier->products->where('status', '!=', 'deleted') as $product)
                <tr>
                  <td>{{$product->name}}</td>
                  <td>{{$product->quantity}}</td>
                  <td>${{$product->price}}</td>
                  <td>
                    <a href="{{url('product', $product->id)}}" class="btn btn-success">
                      <i class="fa fa-eye"></i> 
                    </a>
                  </td>
                  <td>
                    <a href="{{route('product.edit', $product->id)}}" class="btn btn-primary">
                      <i class="fa fa-edit"></i>
                    </a>
                  </td>
                  <td>
                    @include('helpers.delete', ['id' => $product->id, 'route' => 'product.destroy'])
                  </td>
                </tr>
              @endforeach
            </table>
          </div>
      	</div>
      	<div class="x_footer">
      	</div>
      </div>
	   </div>
   </div>
</div>
@endsection
