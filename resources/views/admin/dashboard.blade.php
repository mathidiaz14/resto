@extends('layouts.dashboard')

@section('title')
	Dashboard
@endsection

@section('content')

@php
	$arching = Auth::user()->business->arches->where('status', 'enabled')->first();
	if ($arching != null)
	{
		if(count($arching->tables) == 0)
			$tables = 1;
		else
			$tables = count($arching->tables);
	}
@endphp

@if($arching != null)
		
	<div class="row tile_count">
		<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
			<span class="count_top"><i class="fas fa-utensils"></i> Mesas atendidas</span>
			<div class="count">{{count($arching->tables)}}</div>
		</div>
		<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
			<span class="count_top"><i class="fas fa-users"></i> Clientes Atendidos</span>
			<div class="count">
				@php 
					$clients = 0;
					foreach($arching->tables as $table)
						$clients += $table->diners; 
					
					echo $clients;

				@endphp
					
			</div>
		</div>
		<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
			<span class="count_top"><i class="fa fa-user"></i></i> Clientes promedio por mesa</span>
			<div class="count">
				{{round($clients / $tables)}}
			</div>
		</div>
		<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
			<span class="count_top"><i class="fa fa-clock"></i> Tiempo promedio de las mesas</span>
			<div class="count">
				@php 
					$timeTotal = 0;
					foreach($arching->tables->where('status', 'disabled') as $table)
						$timeTotal += $table->created_at->diffInMinutes($table->updated_at); 
					
					if ($timeTotal != 0) 
						echo round($timeTotal / $tables);
					else
						echo "0";
				
				@endphp
			</div>
		</div>
		<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
			<span class="count_top"><i class="fa fa-money-bill-wave"></i> Dinero promedio por mesa</span>
			<div class="count">
				$
				@php 
					$priceTotal = 0;
					foreach($arching->tables->where('status', 'disabled') as $table)
					{
						foreach($table->commands as $command)
						{
							$priceTotal += $command->price; 
						}
					}
					 
					echo round($priceTotal / $tables);	
				@endphp
			</div>
		</div>
		<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
			<span class="count_top"><i class="fa fa-money-check-alt"></i> Dinero total facturado</span>
			<div class="count green">
				$
				@php 
					$priceTotal = 0;
					foreach($arching->tables->where('status', 'disabled') as $table)
					{
						foreach($table->commands as $command)
						{
							$priceTotal += $command->price; 
						}
					}
					 
					echo round($priceTotal);	
				@endphp
			</div>
		</div>
	</div>
@endif

<div class="row">
	<div class="col-xs-12">
		@include('helpers.alert')
      	<div class="x_panel tile">
        	<div class="x_title">
          		<div class="row">
          			<div class="col-xs-6">
          				<h3>Panel de control</h3>
          			</div>
          			<div class="col-xs-6 text-right">
          				@if($arching == null)
							<button type="button" class="btn btn-success" data-toggle="modal" data-target="#open_arches">
			                    <i class="fa fa-clock"></i>
			                    @lang('message.open_arches')
				            </button>

				            <div class="modal fade" id="open_arches" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				                <div class="modal-dialog" role="document">
				                    <div class="modal-content">
				                        <div class="modal-header text-left">
				                            <div class="col-sm-9">
				                                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-clock"></i> @lang('message.open_arches')</h4>
				                            </div>
				                            <div class="col-sm-3">
				                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				                                    <span aria-hidden="true">&times;</span>
				                                </button>
				                            </div>
				                        </div>
				                        <div class="modal-body">
				                            <div class="row">
				                                <div class="col-xs-10 col-xs-offset-1">
				                                    <form action="{{url('arching')}}" method="POST" class="form-horizontal">
				                                        @csrf
				                                        <div class="col-xs-12">
				                                            <div class="row">
				                                                <div class="col-xs-12 text-left">
				                                                    <label for="" class="control-label">@lang('message.cash_open_arches')</label>
				                                                    <input type="number" class="form-control virtual" name="quantity" id="open_quantity" value=0 required>
				                                                    <br>
				                                                </div>
				                                            </div>
				                                        </div>
				                                        <div class="col-xs-12 btn-calc">
				                                            <div class="row">
				                                                <div class="col-xs-4"><a class="btn btn-primary btn-block btn_open_calc" value="7"><b>7</b></a></div>
				                                                <div class="col-xs-4"><a class="btn btn-primary btn-block btn_open_calc" value="8"><b>8</b></a></div>
				                                                <div class="col-xs-4"><a class="btn btn-primary btn-block btn_open_calc" value="9"><b>9</b></a></div>
				                                            </div>
				                                            <div class="row">
				                                                <div class="col-xs-4"><a class="btn btn-primary btn-block btn_open_calc" value="4"><b>4</b></a></div>
				                                                <div class="col-xs-4"><a class="btn btn-primary btn-block btn_open_calc" value="5"><b>5</b></a></div>
				                                                <div class="col-xs-4"><a class="btn btn-primary btn-block btn_open_calc" value="6"><b>6</b></a></div>
				                                            </div>
				                                            <div class="row">
				                                                <div class="col-xs-4"><a class="btn btn-primary btn-block btn_open_calc" value="1"><b>1</b></a></div>
				                                                <div class="col-xs-4"><a class="btn btn-primary btn-block btn_open_calc" value="2"><b>2</b></a></div>
				                                                <div class="col-xs-4"><a class="btn btn-primary btn-block btn_open_calc" value="3"><b>3</b></a></div>
				                                            </div>
				                                            <div class="row">
				                                                <div class="col-xs-4"><a class="btn btn-primary btn-block btn_open_calc" value="0">0</a></div>
				                                                <div class="col-xs-8"><a class="btn btn-warning btn-block btn_open_back"><i class="fa fa-arrow-circle-left"></i></a></div>
				                                            </div>
				                                            <br>
				                                            <div class="row">
				                                                <div class="col-xs-12">
				                                                    <button class="btn btn-block btn-info" style="padding: 15px;">
				                                                        <i class="fa fa-clock"></i>
				                                                        @lang('message.open')
				                                                    </button>
				                                                </div>
				                                            </div>
				                                            <br>
				                                        </div>
				                                    </form>
				                                </div>
				                            </div>
				                        </div>
				                    </div>
				                </div>
				            </div>

						@else
							<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#close_arches">
	                            <i class="fa fa-clock"></i>
	                            @lang('message.close_arches')
		                    </button>

			                <div class="modal fade" id="close_arches" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			                    <div class="modal-dialog" role="document">
			                        <div class="modal-content">
			                            <div class="modal-header text-left">
			                                <div class="col-sm-9">
			                                    <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-clock"></i> @lang('message.close_arches')</h4>
			                                </div>
			                                <div class="col-sm-3">
			                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                                        <span aria-hidden="true">&times;</span>
			                                    </button>
			                                </div>
			                            </div>
			                            <div class="modal-body">
			                                <div class="row">
			                                    <div class="col-sm-10 col-sm-offset-1">
			                                        <form action="{{url('arching', $arching->id)}}" method="POST" class="form-horizontal">
			                                            @csrf
			                                            <input type="hidden" name="_method" value="DELETE">
			                                            <div class="col-xs-12">
			                                                <div class="row">
			                                                    <div class="col-xs-12 text-left">
			                                                        <label>@lang('message.cash_close_arches')</label>
			                                                        <input type="number" class="form-control virtual" name="quantity" id="close_quantity" value=0 required>
			                                                        <br>
			                                                    </div>
			                                                </div>
			                                            </div>
			                                            <div class="col-xs-12 btn-calc">
			                                                <div class="row">
			                                                    <div class="col-xs-4"><a class="btn btn-primary btn-block btn_close_calc" value="7"><b>7</b></a></div>
			                                                    <div class="col-xs-4"><a class="btn btn-primary btn-block btn_close_calc" value="8"><b>8</b></a></div>
			                                                    <div class="col-xs-4"><a class="btn btn-primary btn-block btn_close_calc" value="9"><b>9</b></a></div>
			                                                </div>
			                                                <div class="row">
			                                                    <div class="col-xs-4"><a class="btn btn-primary btn-block btn_close_calc" value="4"><b>4</b></a></div>
			                                                    <div class="col-xs-4"><a class="btn btn-primary btn-block btn_close_calc" value="5"><b>5</b></a></div>
			                                                    <div class="col-xs-4"><a class="btn btn-primary btn-block btn_close_calc" value="6"><b>6</b></a></div>
			                                                </div>
			                                                <div class="row">
			                                                    <div class="col-xs-4"><a class="btn btn-primary btn-block btn_close_calc" value="1"><b>1</b></a></div>
			                                                    <div class="col-xs-4"><a class="btn btn-primary btn-block btn_close_calc" value="2"><b>2</b></a></div>
			                                                    <div class="col-xs-4"><a class="btn btn-primary btn-block btn_close_calc" value="3"><b>3</b></a></div>
			                                                </div>
			                                                <div class="row">
			                                                    <div class="col-xs-4"><a class="btn btn-primary btn-block btn_close_calc" value="0">0</a></div>
			                                                    <div class="col-xs-8"><a class="btn btn-warning btn-block btn_close_back"><i class="fa fa-arrow-circle-left"></i></a></div>
			                                                </div>
			                                                <br>
			                                                <div class="row">
			                                                    <div class="col-xs-12">
			                                                        <button class="btn btn-block btn-info" style="padding: 15px;">
			                                                            <i class="fa fa-clock"></i>
			                                                            @lang('message.close_arches')
			                                                        </button>
			                                                    </div>
			                                                </div>
			                                                <br>
			                                            </div>
			                                        </form>
			                                    </div>
			                                </div>
			                            </div>
			                        </div>
			                    </div>
			                </div>

          				@endif
          			</div>
          		</div>
          		<div class="clearfix"></div>
        	</div>
        	<div class="x_content">
          		@if($arching == null)
					<div class="alert alert-success">
						<span>Es necesario comenzar un turno para continuar.</span>
					</div>
          		@else
					<div class="row">
						<div class="col-xs-12">
							<h4>@lang('message.start'): <b>{{$arching->created_at->format('d/m/y H:i')}}</b></h4>
						</div>
						<div class="col-xs-12">
							<h4>@lang('message.user'): <b>{{$arching->user->name}}</b></h4>
						</div>
						<div class="col-xs-12">
							<h4>@lang('message.cash_open_arches'): <b>${{$arching->start}}</b></h4>
						</div>
					</div>
          		@endif
          	</div>
        </div>
  	</div>
</div>

@if($arching != null) 
	<div class="row">
		<div class="col-xs-12">
		  	<div class="x_panel">
		    	<div class="x_title">
		      		<div class="row">
		      			<div class="col-xs-6">
		      				<h4>@lang('message.tables')</b></h4>
		      			</div>
		      			<div class="col-xs-6 text-right">
		      			</div>
		      		</div>
		      		<div class="clearfix"></div>
		    	</div>
		    	<div class="x_content text-center">
		      		<div class="table-responsive">
		      			<table class="table table-striped">
		      				<th class="text-center">@lang('message.table')</th>
		      				<th class="text-center">@lang('message.hour_start')</th>
		      				<th class="text-center">@lang('message.hour_end')</th>
		      				<th class="text-center">@lang('message.waiter')</th>
		      				<th class="text-center">@lang('message.diners')</th>
		      				<th class="text-center">@lang('message.expenses')</th>
		      				<th class="text-center">@lang('message.view')</th>

		      				@foreach($arching->tables->where('status', '!=', 'deleted') as $table)
								<tr>
									<td>Mesa {{$table->fixed->name}}</td>
									<td>{{$table->created_at->format('d/m/Y H:i')}}</td>
									<td>
										@if($table->status == "enabled")
											No finalizo
										@else
											{{$table->updated_at->format('d/m/Y H:i')}}
										@endif
									</td>
									<td>{{$table->user->name}}</td>
									<td>{{$table->diners}}</td>
									<td>$ {{$table->commands->where('status', 'disabled')->sum('price')}}</td>
									<td>
										<a href="{{url('arching/table', $table->id)}}" class="btn btn-success">
	  										<i class="fa fa-eye"></i>	
	  									</a>
									</td>
								</tr>
							@endforeach
		      			</table>
		      		</div>
		      	</div>
		    </div>
		    <hr>
		    <div class="x_panel">
		    	<div class="x_title">
		      		<div class="row">
		      			<div class="col-xs-6">
		      				<h4>@lang('message.retirement')</h4>
		      			</div>
		      			<div class="col-xs-6 text-right">
		      				<a href="{{url('retirement/print/arching', $arching->id)}}" class="btn btn-info" @if($arching->retirements->count() == 0) disabled @endif>
		      					<i class="fa fa-print"></i> Imprimir todos
		      				</a>

		      				<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#retirement">
		                        <i class="fa fa-money-bill"></i>
		                        @lang('message.add_retirement')
		                    </button>

			                <div class="modal fade" id="retirement" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			                    <div class="modal-dialog" role="document">
			                        <div class="modal-content">
			                            <div class="modal-header text-left">
			                                <div class="col-sm-9">
			                                    <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-money-bill"></i> @lang('message.retirement')</h4>
			                                </div>
			                                <div class="col-sm-3">
			                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                                        <span aria-hidden="true">&times;</span>
			                                    </button>
			                                </div>
			                            </div>
			                            <div class="modal-body">
			                                <div class="row">
			                                    <div class="col-sm-10 col-sm-offset-1">
			                                        <form action="{{url('retirement')}}" method="POST" class="form-horizontal text-left">
			                                            @csrf
			                                            <input type="hidden" name="arching_id" value="{{$arching->id}}">
			                                            <div class="col-xs-12">
		                                                    <label>@lang('message.observation')</label>
		                                                    <input type="text" class="form-control virtual" name="observation" id="observation" required>
		                                                    <br>
			                                            </div>
			                                            <div class="col-xs-12">
		                                                    <label>@lang('message.retirement_quantity')</label>
		                                                    <input type="number" class="form-control virtual" name="quantity" id="retirement_quantity" value=0 required>
		                                                    <br>
			                                            </div>
			                                            <div class="col-xs-12 text-right">
			                                            	<button class="btn btn-primary">
			                                            		<i class="fa fa-save"></i>
			                                            		Guardar
			                                            	</button>
			                                            </div>
			                                        </form>
			                                    </div>
			                                </div>
			                            </div>
			                        </div>
			                    </div>
			                </div>
		      			</div>
		      		</div>
		      		<div class="clearfix"></div>
		    	</div>
		    	<div class="x_content text-center">
		      		<div class="table-responsive">
		      			<table class="table table-striped">
		      				<th class="text-center">@lang('message.quantity')</th>
		      				<th class="text-center">@lang('message.observation')</th>
		      				<th class="text-center">@lang('message.user')</th>
		      				<th class="text-center">@lang('message.options')</th>

		      				@foreach($arching->retirements->where('status', '!=', 'deleted') as $retirement)
								<tr>
									<td>${{$retirement->quantity}}</td>
									<td>{{$retirement->observation}}</td>
									<td>{{$retirement->user->name}}</td>
									<td>
										<a href="{{url('retirement/print', $retirement->id)}}" class="btn btn-info">
											<i class="fa fa-print"></i>
										</a>

										@if(Auth::user()->type == "admin")
											@include('helpers.delete', ['id' => $retirement->id, 'route' => 'retirement.destroy'])
										@endif
									</td>
								</tr>
							@endforeach
		      			</table>
		      		</div>
		      		
		      	</div>
		    </div>
		</div>
	</div>
@endif
@endsection

@section('scripts')
	<script>
		$('#open_arches').on('shown.bs.modal', function () 
        {
            $('#quantity').focus();
        });

        $('#close_arches').on('shown.bs.modal', function () 
        {
            $('#quantity').focus();
        });

        $('#retirement').on('shown.bs.modal', function () 
        {
            $('#observation').focus();
        });

        $('.btn_open_calc').click(function()
        {
            var num = $(this).attr('value');
            var text = $('#open_quantity').val();
            
            if(text == "0")
            {
                $('#open_quantity').val(num);
            }else
            {
                $('#open_quantity').val(text + num);
            }
        });

        $('.btn_open_back').click(function()
        {
            $('#open_quantity').val("0");
        });

        $('.btn_close_calc').click(function()
        {
            var num = $(this).attr('value');
            var text = $('#close_quantity').val();
            
            if(text == "0")
            {
                $('#close_quantity').val(num);
            }else
            {
                $('#close_quantity').val(text + num);
            }
        });

        $('.btn_close_back').click(function()
        {
            $('#close_quantity').val("0");
        });
	</script>
@endsection