@extends('layouts.dashboard')

@section('title')
	@lang('message.categories')
@endsection

@section('content')

<div class="row">
	<div class="col-xs-12">
      	<div class="x_panel">
        	<div class="x_title">
          		<h2>@lang('message.products_of'): <b>{{$category->name}}</b></h2>
          		<div class="clearfix"></div>
        	</div>
        	<div class="x_content">
          		<div class="table-responsive">
          			<table class="table table-striped">
          				<th>@lang('message.name')</th>
          				<th>@lang('message.quantity')</th>
          				<th>@lang('message.price')</th>
          				<th>@lang('message.view')</th>
          				<th>@lang('message.edit')</th>
          				<th>@lang('message.delete')</th>
          				
                  @foreach($category->products->where('status', '!=', 'deleted') as $product)
      							<tr>
      								<td>{{$product->name}}</td>
      								<td>{{$product->quantity}}</td>
      								<td>${{$product->price}}</td>
      								<td>
      									<a href="{{url('product', $product->id)}}" class="btn btn-success">
      										<i class="fa fa-eye"></i>	
      									</a>
      								</td>
      								<td>
      									<a href="{{route('product.edit', $product->id)}}" class="btn btn-primary">
      										<i class="fa fa-edit"></i>
      									</a>
      								</td>
      								<td>
      									@include('helpers.delete', ['id' => $product->id, 'route' => 'product.destroy'])
      								</td>
      							</tr>
					        @endforeach
          			</table>
          		</div>
          	</div>
          	<div class="x_footer">
          		<div class="col-xs-6">
          			<a href="{{url('category')}}" class="btn btn-info">
          				<i class="fa fa-chevron-left"></i>
          				@lang('message.back')
          			</a>
          		</div>
          		<div class="col-xs-6 text-right">
					
          		</div>
          	</div>
        </div>
  	</div>
</div>
@endsection
