@extends('layouts.dashboard')

@section('title')
	@lang('message.categories')
@endsection

@section('content')

<div class="row">
	<div class="col-xs-12">
      	<div class="x_panel">
        	<div class="x_title">
          		<h2>@lang('message.categories')</h2>
          		<div class="clearfix"></div>
        	</div>
        	<div class="x_content">
          		<div class="table-responsive">
          			<table class="table table-striped">
          				<tr>
          					<th>@lang('message.name')</th>
          					<th>@lang('message.products')</th>
          					<th>@lang('message.view')</th>
          					<th>@lang('message.edit')</th>
          					<th>@lang('message.delete')</th>
          				</tr>
          				@foreach($categories as $category)
      							<tr>
      								<td>{{$category->name}}</td>
      								<td>{{$category->products->count()}}</td>
      								<td>
      									<a href="{{url('category', $category->id)}}" class="btn btn-success">
      										<i class="fa fa-eye"></i>	
      									</a>
      								</td>
      								<td>
      									<a href="{{route('category.edit', $category->id)}}" class="btn btn-primary">
      										<i class="fa fa-edit"></i>
      									</a>
      								</td>
      								<td>
      									@include('helpers.delete', ['id' => $category->id, 'route' => 'category.destroy'])
      								</td>
      							</tr>
      						@endforeach
          			</table>
          		</div>
          	</div>
          	<div class="x_footer">
          		<div class="col-xs-6">
          			<a href="{{url('admin')}}" class="btn btn-info">
          				<i class="fa fa-chevron-left"></i>
          				@lang('message.back')
          			</a>
          		</div>
          		<div class="col-xs-6 text-right">
					<a href="{{url('category/create')}}" class="btn btn-info">
						<i class="fa fa-plus"></i>
						@lang('message.create')
					</a>
          		</div>
          	</div>
        </div>
  	</div>
</div>
@endsection
