@extends('layouts.dashboard')

@section('title')
	@lang('message.users')
@endsection

@section('content')

<div class="row">
	<div class="col-xs-12">
      	<div class="x_panel">
        	<div class="x_title">
        		<div class="row">
        			<div class="col-xs-6">
		          		<h4>@lang('message.users')</h4>
		          		<div class="clearfix"></div>
        			</div>
        			<div class="col-xs-6 text-right">
        				<a href="{{url('admin')}}" class="btn btn-default">
	          				<i class="fa fa-chevron-left"></i>
	          				@lang('message.back')
	          			</a>
        			</div>
        		</div>
        	</div>
        	<div class="x_content">
        		@include('helpers.alert')
          		<div class="table-responsive">
          			<table class="table table-striped">
          				<th>@lang('message.name')</th>
          				<th>@lang('message.user')</th>
          				<th>@lang('message.type')</th>
          				<th>@lang('message.color')</th>
          				<th>@lang('message.edit')</th>
          				<th>@lang('message.delete')</th>
          				
          				@foreach($users as $user)
							<tr>
								<td>{{$user->name}}</td>
								<td>{{$user->email}}</td>
								<td>
									@if($user->type == "admin")
										Administrador
									@elseif($user->type == "cashier")
										Cajero
									@elseif($user->type == "waiter")
										Mozo
									@endif
								</td>
								<td>
									<div style="height: 30px; width: 50%; background:{{$user->color}}; border-radius: 4px;"></div>
								</td>
								<td>
									<a href="{{route('user.edit', $user->id)}}" class="btn btn-primary">
										<i class="fa fa-edit"></i>
									</a>
								</td>
								<td>
									@include('helpers.delete', ['id' => $user->id, 'route' => 'user.destroy'])
								</td>
							</tr>
						@endforeach
          			</table>
          		</div>
          	</div>
          	<div class="x_footer">
          		<div class="col-xs-6">
          			
          		</div>
          		<div class="col-xs-6 text-right">
					<a href="{{url('user/create')}}" class="btn btn-info">
						<i class="fa fa-plus"></i>
						@lang('message.create')
					</a>
          		</div>
          	</div>
        </div>
  	</div>
</div>
@endsection
