@extends('layouts.dashboard')

@section('title')
	@lang('message.users')
@endsection

@section('content')

<div class="row">
	<div class="col-xs-12">
      	<div class="x_panel">
        	<div class="x_title">
          		<div class="row">
                <div class="col-xs-6">
                  <h4>@lang('message.users_create')</h4>
                  <div class="clearfix"></div>
                </div>
                <div class="col-xs-6 text-right">
                  <a href="{{url('user')}}" class="btn btn-default">
                    <i class="fa fa-chevron-left"></i>
                    @lang('message.back')
                  </a>  
                </div>
              </div>
        	</div>
        	<div class="x_content">
          		<form action="{{url('user')}}" method="POST" class="form-horizontal col-xs-12 col-md-8 col-md-offset-2">
          			@csrf
                @include('helpers.alert')
          			<div class="row">
                  <div class="col-xs-12 col-md-6">
                      <div class="form-group">
                        <label for="">@lang('message.name')</label>
                        <input type="text" id="name" name="name" class="form-control" value="{{old('name')}}" autofocus="" required="">
                      </div>
                  </div>
                  <div class="col-xs-12 col-md-6">
                    <div class="form-group">
                      <label for="">@lang('message.color')</label>
                      <input type="color" name="color" value="#45e8e5" class="form-control">
                    </div>
                  </div>
                </div>
          			<div class="row">
                  <div class="col-xs-12 col-md-6">
                    <div class="form-group">
                      <label for="">@lang('message.user') </label> <small>@lang('message.user_comment')</small>
                      <div class="row">
                        <div class="col-xs-6">
                          <input type="text" name="email" id="email" value="{{old('email')}}" class="form-control" required="">
                        </div>
                        <div class="col-xs-6">
                          <h4><b>@ {{strtolower(Auth::user()->business->name)}}</b></h4>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-12 col-md-6">
                    <div class="form-group">
                      <label for="">@lang('message.password')</label>
                      <input type="password" name="password" class="form-control" required="">
                    </div>
                  </div>
                </div>
          			<div class="row">
                  <div class="col-xs-12 col-md-6">
                    <div class="form-group">
                      <label for="">@lang('message.type')</label>
                      <select name="type" class="form-control">
                        @if(Auth::user()->type == "admin")
                          <option value="admin">Administrador</option>
                        @endif
                        <option value="cashier">Cajero</option>
                        <option value="waiter">Mozo</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-xs-12 col-md-6">
                    <div class="form-group">
                      <label for="">@lang('message.copy_hours')</label>
                      <select name="hour" class="form-control">
                        <option value="null" selected="">Ninguno</option>
                        @foreach(Auth::user()->business->users as $user)
                          <option value="{{$user->id}}">{{$user->name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
                <hr>
                <div class="row">
                  <div class="col-xs-12">
                    <p>@lang('message.user_hours')</p>
                    <div class="table table-responsive">
                      <table class="table table-striped">
                        <tr>
                          <th>@lang('message.day')</th>
                          <th>@lang('message.hour_start')</th>
                          <th>@lang('message.hour_end')</th>
                          <th>@lang('message.activate')</th>
                        </tr>
                        @php 
                          $days   = [
                              'monday',
                              'tuesday',
                              'wednesday',
                              'thursday',
                              'friday',
                              'saturday',
                              'sunday'
                          ];
                        @endphp
                        
                        @foreach($days as $day)
                          <tr>
                            <td>@lang('message.'.$day)</td>
                            <td>
                              @include('helpers.hour_selector', [
                                                'name'    => $day."_start", 
                                                'min'     => "10",
                                                'value'   => "00:00",
                                                'status'  => 'disabled' 
                                                ])
                            </td>
                            <td>
                              @include('helpers.hour_selector', [
                                                'name'    => $day."_end", 
                                                'min'     => "10",
                                                'value'   => "00:00",
                                                'status'  => 'disabled' 
                                                ])
                            </td>
                            <td>
                              @include('helpers.checkbox', [
                                                'name' => $day."_selector", 
                                                'status' => null,
                                                'text' => "Activar"
                                                ])
                            </td>
                          </tr>
                        @endforeach

                      </table>
                    </div>
                  </div>
                </div>
          			<div class="form-group text-right">
          				<button class="btn btn-info">
          					<i class="fa fa-save"></i>
          					@lang('message.save')
          				</button>
          			</div>
          		</form>
          	</div>
          	<div class="x_footer">
          		<div class="col-xs-6">
          			
          		</div>
          	</div>
        </div>
  	</div>
</div>
@endsection

@section('scripts')
	<script>
		$('#email').keyup(function(tecla) 
		{
      $('#email').val($(this).val().toLowerCase().replace(/\s/g, "-").replace(/[^ a-z0-9áéíóúüñ]+/ig,"-"));
    });

    @foreach($days as $day)
      $('#{{$day}}_selector').on('change',function()
      { 

        if($(this).is(':checked'))
        {
          $('#{{$day}}_start').attr('readonly', false);
          $('#{{$day}}_end').attr('readonly', false);
        }else
        {
          $('#{{$day}}_start').attr('readonly', true);
          $('#{{$day}}_end').attr('readonly', true);
        }
      });
    @endforeach
	</script>
@endsection