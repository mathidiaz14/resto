@extends('layouts.dashboard')

@section('title')
	@lang('message.arches')
@endsection

@section('content')

<div class="row">
	<div class="col-xs-12">
      	<div class="x_panel">
        	<div class="x_title">
          		<div class="row">
                <div class="col-xs-6">
                  <h3>@lang('message.arches')</h3>
                </div>
                <div class="col-xs-6 text-right">
                  <a href="{{url('admin')}}" class="btn btn-default">
                    <i class="fa fa-chevron-left"></i>
                    @lang('message.back')
                  </a>
                </div>  
              </div>
          		<div class="clearfix"></div>
        	</div>
        	<div class="x_content">
              @include('helpers.alert')
          		<div class="table-responsive">
          			<table class="table table-striped">
          				<tr>
          					<th>@lang('message.start')</th>
          					<th>@lang('message.end')</th>
          					<th>@lang('message.tables')</th>
          					<th>@lang('message.view')</th>
                    <th>@lang('message.print')</th>
          				</tr>
          				@foreach($arches as $arching)
      							<tr>
      								<td>{{$arching->created_at->format('d/m/Y H:i')}}</td>
      								<td>
      									@if ($arching->end == null)
      										@lang('message.no_end_arches')
      									@else
      										{{$arching->updated_at->format('d/m/Y H:i')}}
      									@endif
      								</td>
      								<td>
      									{{$arching->tables->count()}}
      								</td>
      								<td>
      									<a href="{{url('arching', $arching->id)}}" class="btn btn-success">
      										<i class="fa fa-eye"></i>	
      									</a>
      								</td>
                      <td>
                        @if ($arching->end == null)
                          <a class="btn btn-warning disabled" disabled>
                            <i class="fa fa-print"></i> 
                          </a>
                        @else
                          <a href="{{url('arching/print', $arching->id)}}" class="btn btn-warning">
                            <i class="fa fa-print"></i> 
                          </a>
                        @endif
                      </td>
      							</tr>
      						@endforeach
          			</table>
                <div class="col-xs-12 text-center">
                  {{$arches->links()}}
                </div>
          		</div>
          	</div>
          	<div class="x_footer">
          		<div class="col-xs-6">
          		</div>
          		<div class="col-xs-6 text-right">
					
          		</div>
          	</div>
        </div>
  	</div>
</div>
@endsection
