@extends('layouts.dashboard')

@section('title')
	@lang('message.arches')
@endsection

@section('content')

<div class="row">
	<div class="col-xs-12">
    @include('helpers.alert')
  	<div class="x_panel">
    	<div class="x_title">
      		<div class="row">
      			<div class="col-xs-6">
      				<h4>@lang('message.arches_of'): <b>{{$arching->created_at->format('d/m/Y H:i')}} - @if($arching->end == null) No finalizo @else {{$arching->updated_at->format('d/m/Y H:i')}} @endif</b></h4>
      			</div>
      			<div class="col-xs-6 text-right">
      				<a href="{{URL::Previous()}}" class="btn btn-default">
        				<i class="fa fa-chevron-left"></i>
        				@lang('message.back')
        			</a>
      			</div>
      		</div>
      		<div class="clearfix"></div>
    	</div>
    	<div class="x_content">
    		<div class="row">
          <div class="row tile_count">
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fas fa-money-bill-wave"></i> Saldo inicial</span>
              <div class="count">
                ${{$arching->start}}
              </div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fas fa-money-bill-wave"></i> Saldo final</span>
              <div class="count">
                @if($arching->end == null)
                  --
                @else
                  ${{$arching->end}}
                @endif
              </div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-money-bill-wave"></i></i> Total de ingresos</span>
              <div class="count green">
                ${{{$arching->tables->sum('total')}}}
              </div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-money-bill-wave"></i> Total de egresos</span>
              <div class="count red">
                ${{$arching->retirements->sum('quantity')}}
              </div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-money-bill-wave"></i> Saldo final en la app</span>
              <div class="count">
                @if($arching->end == null)
                  --
                @else
                  ${{($arching->tables->sum('total') - $arching->retirements->sum('quantity')) + $arching->start}}
                @endif
              </div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-money-bill-wave"></i> Diferencia</span>
              <div class="count">
                @if($arching->end == null)
                  --
                @else
                  ${{((($arching->tables->sum('total') - $arching->retirements->sum('quantity')) + $arching->start) - $arching->end)}}
                @endif
              </div>
            </div>
          </div>  
        </div>
        <div class="row">
            <div class="row tile_count">
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fas fa-money-bill-wave"></i> Efectivo</span>
              <div class="count">
                ${{$arching->commands->where('wayOfPay', 'cash')->sum('pay')}}
              </div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fas fa-money-bill-wave"></i> Debito</span>
              <div class="count">
                ${{$arching->commands->where('wayOfPay', 'debit')->sum('pay')}}
              </div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-money-bill-wave"></i></i> Visa</span>
              <div class="count">
                ${{$arching->commands->where('wayOfPay', 'visa')->sum('pay')}}
              </div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-money-bill-wave"></i> Master</span>
              <div class="count">
                ${{$arching->commands->where('wayOfPay', 'master')->sum('pay')}}
              </div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-money-bill-wave"></i> Otros</span>
              <div class="count">
                ${{$arching->commands->where('wayOfPay', 'other')->sum('pay')}}
              </div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-money-bill-wave"></i> Descuentos</span>
              <div class="count">
                @php
                  $discounts = 0;

                  foreach($arching->tables as $table)
                    $discounts += $table->discounts->sum('quantity');

                @endphp
                - ${{$discounts}}
              </div>
            </div>
          </div>  
        </div>
    	</div>
    </div>

    <hr>
    
    <div class="x_panel">
    	<div class="x_title">
      		<div class="row">
      			<div class="col-xs-6">
      				<h4>Mesas [Total: {{$arching->tables->count()}} ]</h4>
      			</div>
      			<div class="col-xs-6 text-right">
      			</div>
      		</div>
      		<div class="clearfix"></div>
    	</div>
    	<div class="x_content text-center">
    		<div class="table-responsive">
          <table class="table table-striped">
            <th class="text-center">@lang('message.table')</th>
            <th class="text-center">@lang('message.hour_start')</th>
            <th class="text-center">@lang('message.hour_end')</th>
            <th class="text-center">@lang('message.waiter')</th>
            <th class="text-center">@lang('message.diners')</th>
            <th class="text-center">@lang('message.income')</th>
            <th class="text-center">@lang('message.view')</th>

            @foreach($arching->tables->where('status', '!=', 'deleted') as $table)
              <tr>
                <td>Mesa {{$table->fixed->name}}</td>
                <td>{{$table->created_at->format('d/m/Y H:i')}}</td>
                <td>
                  @if($table->created_at == $table->updated_at)
                    No finalizo
                  @else
                    {{$table->updated_at->format('d/m/Y H:i')}}
                  @endif
                </td>
                <td>{{$table->user->name}}</td>
                <td>{{$table->diners}}</td>
                <td>$ {{$table->total}}</td>
                <td>
                  <a href="{{url('arching/table', $table->id)}}" class="btn btn-success">
                    <i class="fa fa-eye"></i> 
                  </a>
                </td>
              </tr>
            @endforeach
              <tfoot>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td><b>Total: ${{$arching->tables->sum('total')}}</b></td>
                <td></td>
              </tfoot>
          </table>
        </div>
    	</div>
    </div>

    <div class="x_panel">
      <div class="x_title">
          <div class="row">
            <div class="col-xs-6">
              <h4>Retiros de caja</h4>
            </div>
            <div class="col-xs-6 text-right">
              <a href="{{url('retirement/print/arching', $arching->id)}}" class="btn btn-info">
                <i class="fa fa-print"></i> Imprimir todos
              </a>
            </div>
          </div>
          <div class="clearfix"></div>
      </div>
      <div class="x_content text-center">
        <div class="table-responsive">
          <table class="table table-striped">
            <th class="text-center">@lang('message.quantity')</th>
            <th class="text-center">@lang('message.observation')</th>
            <th class="text-center">@lang('message.user')</th>
            <th class="text-center">@lang('message.options')</th>

            @foreach($arching->retirements->where('status', '!=', 'deleted') as $retirement)
              <tr>
                <td>${{$retirement->quantity}}</td>
                <td>{{$retirement->observation}}</td>
                <td>{{$retirement->user->name}}</td>
                <td>
                  <a href="{{url('retirement/print', $retirement->id)}}" class="btn btn-info">
                    <i class="fa fa-print"></i>
                  </a>

                  @if(Auth::user()->type == "admin")
                    @include('helpers.delete', ['id' => $retirement->id, 'route' => 'retirement.destroy'])
                  @endif
                </td>
              </tr>
            @endforeach
          </table>
        </div>
      </div>
    </div>
	</div>
</div>
@endsection
