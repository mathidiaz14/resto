@extends('layouts.dashboard')

@section('title')
	@lang('message.arches')
@endsection

@section('content')

<div class="row">
	<div class="col-xs-12">
      	<div class="x_panel">
        	<div class="x_title">
          		<div class="row">
          			<div class="col-xs-6">
          				<h4>Mesa {{$table->fixed->name}}</h4>
          			</div>
          			<div class="col-xs-6 text-right">
          				<a href="{{URL::Previous()}}" class="btn btn-default">
	          				<i class="fa fa-chevron-left"></i>
	          				@lang('message.back')
	          			</a>
          			</div>
          		</div>
          		<div class="clearfix"></div>
        	</div>
        	<div class="x_content text-center">
          		<div class="table-responsive">
          			<table class="table table-striped">
          				<th class="text-center">@lang('message.product')</th>
          				<th class="text-center">@lang('message.quantity')</th>
          				<th class="text-center">@lang('message.price')</th>
                  <th></th>
                  <th></th>

          				@foreach($table->commands->where('status', '!=','deleted')->groupBy('product_id') as $command)
      							<tr attr-id="{{$command->first()->id}}">
      								<td>{{$command->first()->product->name}}</td>
      								<td>x {{$command->count()}}</td>
                      <td>${{($command->first()->price * $command->count())}}</td>
                      <td></td>
                      <td></td>
      							</tr>
                    @foreach($command as $item)
                      <tr style="display: none;" class="list_{{$command->first()->id}}">
                        <td>{{$item->name}}</td>
                        <td>x 1</td>
                        <td>$ {{$item->price}}</td>
                        <td>
                          @if($item->status == "enabled")
                            Comandada
                          @elseif($item->status == "disabled")
                            Cobrada
                          @elseif($item->status == "pending")
                            Pendiente de comandar
                          @endif
                        </td>
                        <td>
                          @if($item->wayOfPay == null)
                            --
                          @elseif($item->wayOfPay == "cash")
                            Efectivo
                          @elseif($item->wayOfPay == "debit")
                            Debito
                          @elseif($item->wayOfPay == "visa")
                            Visa
                          @elseif($item->wayOfPay == "master")
                            Master
                          @endif
                        </td>
                      </tr>
                    @endforeach
      						@endforeach

                  @php $discounts = 0; @endphp
                  @foreach($table->discounts->where('status', 'enabled') as $discount)
                    @php
                      if($discount->type == "quantity") 
                        $discounts += $discount->quantity;
                      else 
                        $discounts += ($table->commands->where('status', 'enabled')->sum('price') * $discount->quantity) / 100;
                    @endphp

                    @if($loop->first)
                      <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                      </tr>
                    @endif
                    <tr>
                      <td style="color:green;">Descuento</td>
                      <td style="color:green;">
                        @if($discount->type == "percentage")
                          %{{$discount->quantity}}
                        @endif
                      </td>
                      <td style="color:green;">
                        - $
                        @if($discount->type == "quantity") 
                            {{$discount->quantity}}
                        @else 
                            {{($table->commands->where('status', 'enabled')->sum('price') * $discount->quantity) / 100}}
                        @endif
                      </td>
                      <td></td>
                      <td></td>
                    </tr>
                    
                  @endforeach
                  <tfoot>
                    <td></td>
                    <td></td>
                    <td><h4>Total: $ {{($table->commands->sum('price') - $discounts)}}</h4></td>
                    <td></td>
                    <td></td>
                  </tfoot>
          			</table>
          		</div>
          	</div>
          	<div class="x_footer">
          		<div class="col-xs-6">
          		</div>
          		<div class="col-xs-6 text-right">
					
          		</div>
          	</div>
        </div>
  	</div>
</div>
@endsection

@section('scripts')
  <script>
    $(document).ready(function()
    {
      $('tr').click(function()
      {
        var id = $(this).attr('attr-id');
        $('.list_'+id).show();
      });
    });
  </script>
@endsection