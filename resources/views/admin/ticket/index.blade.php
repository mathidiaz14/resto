@extends('layouts.dashboard')

@section('title')
	Tickets
@endsection

@section('content')

<div class="row">
	<div class="col-xs-12">
      	<div class="x_panel">
        	<div class="x_title">
        		<div class="row">
        			<div class="col-xs-6">
		          		<h2>Tickets</h2>
		          		<div class="clearfix"></div>
        			</div>
        			<div class="col-xs-6 text-right">
        				<a href="{{url('admin')}}" class="btn btn-default">
	          				<i class="fa fa-chevron-left"></i>
	          				@lang('message.back')
	          			</a>
        			</div>
        		</div>
        	</div>
        	<div class="x_content">
          		<div class="table-responsive">
          			<table class="table table-striped">
          				<tr>
          					<th>@lang('message.type')</th>
          					<th>@lang('message.printer')</th>
          					<th>@lang('message.code')</th>
          					<th>@lang('message.date')</th>
          					<th>@lang('message.view')</th>
                    <th>@lang('message.reprint')</th>
          				</tr>
          				@foreach($tickets as $ticket)
      							<tr>
      								<td>{{$ticket->type}}</td>
      								<td>{{$ticket->printer->name}}</td>
      								<td>{{$ticket->code}}</td>
      								<td>{{$ticket->created_at->format('d/m/Y H:i')}}</td>
      								<td>
      									<a href="{{url('ticket', $ticket->id)}}" class="btn btn-success">
      										<i class="fa fa-eye"></i>	
      									</a>
      								</td>
                      <td>
                        <a href="{{route('ticket.edit',$ticket->id )}}" class="btn btn-warning">
                          <i class="fa fa-print"></i>
                        </a>
                      </td>
      							</tr>
      						@endforeach
          			</table>
          			<div class="col-xs-12 text-center">
          				{{$tickets->links()}}
          			</div>
          		</div>
          	</div>
          	<div class="x_footer">
          		<div class="col-xs-6">
          		</div>
          		<div class="col-xs-6 text-right">
          		</div>
          	</div>
        </div>
  	</div>
</div>
@endsection
