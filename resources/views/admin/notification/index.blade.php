@extends('layouts.dashboard')

@section('title')
	@lang('message.notifications')
@endsection

@section('content')

<div class="row">
	<div class="col-xs-12">
      	<div class="x_panel">
        	<div class="x_title">
        		<div class="row">
        			<div class="col-xs-6">
		          		<h3>@lang('message.notifications')</h3>
		          		<div class="clearfix"></div>
        			</div>
        			<div class="col-xs-6 text-right">
        				<a href="{{url('admin')}}" class="btn btn-default">
	          				<i class="fa fa-chevron-left"></i>
	          				@lang('message.back')
	          			</a>
        			</div>
        		</div>
        	</div>
        	<div class="x_content">
        		@include('helpers.alert')
          		<div class="table-responsive">
          			<table class="table table-striped">
          				<th>@lang('message.title')</th>
          				<th>@lang('message.description')</th>
          				<th>@lang('message.type')</th>
          				<th>@lang('message.date')</th>
          				<th>@lang('message.delete')</th>
          				
          				@foreach($notifications as $notification)
      							<tr>
      								<td>{{$notification->title}}</td>
      								<td>{{$notification->description}}</td>
      								<td>{{$notification->type}}</td>
      								<td>{{$notification->created_at->format('d/m/Y H:i')}}</td>
      								<td>
      									@include('helpers.delete', ['id' => $notification->id, 'route' => 'notification.destroy'])
      								</td>
      							</tr>
      						@endforeach
          			</table>
          		</div>
          	</div>
        </div>
  	</div>
</div>
@endsection
