@extends('layouts.dashboard')

@section('title')
	@lang('message.clients')
@endsection

@section('content')

<div class="row">
	<div class="col-xs-12">
      	<div class="x_panel">
        	<div class="x_title">
          		<h2>@lang('message.clients_create')</h2>
          		<div class="clearfix"></div>
        	</div>
        	<div class="x_content">
          		<form action="{{url('client')}}" method="POST" class="form-horizontal col-xs-12 col-md-10 col-md-offset-1">
          			@csrf
          			<div class="form-group">
          				<label for="">@lang('message.name')</label>
          				<input type="text" name="name" class="form-control">
          			</div>
          			<div class="form-group">
          				<label for="">@lang('message.address')</label>
          				<input type="text" name="address" class="form-control">
          			</div>
          			<div class="form-group">
          				<label for="">@lang('message.phone')</label>
          				<input type="text" name="phone" class="form-control">
          			</div>
          			<div class="form-group">
          				<button class="btn btn-block btn-info">
          					<i class="fa fa-save"></i>
          					@lang('message.save')
          				</button>
          			</div>
          		</form>
          	</div>
          	<div class="x_footer">
          		<div class="col-xs-6">
          			<a href="{{URL::Previous()}}" class="btn btn-info">
          				<i class="fa fa-chevron-left"></i>
          				@lang('message.back')
          			</a>
          		</div>
          	</div>
        </div>
  	</div>
</div>
@endsection
