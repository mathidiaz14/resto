@extends('layouts.dashboard')

@section('title')
	@lang('message.clients')
@endsection

@section('content')

<div class="row">
	<div class="col-xs-12">
      	<div class="x_panel">
        	<div class="x_title">
          		<h2>@lang('message.clients')</h2>
          		<div class="clearfix"></div>
        	</div>
        	<div class="x_content">
          		<div class="table-responsive">
          			<table class="table table-striped">
          				<tr>
          					<th>@lang('message.name')</th>
          					<th>@lang('message.address')</th>
          					<th>@lang('message.phone')</th>
          					<th>@lang('message.edit')</th>
          					<th>@lang('message.delete')</th>
          				</tr>
          				@foreach($clients as $client)
      							<tr>
      								<td>{{$client->name}}</td>
      								<td>{{$client->address}}</td>
      								<td>{{$client->phone}}</td>
      								<td>
      									<a href="{{route('client.edit', $client->id)}}" class="btn btn-primary">
      										<i class="fa fa-edit"></i>
      									</a>
      								</td>
      								<td>
      									@include('helpers.delete', ['id' => $client->id, 'route' => 'client.destroy'])
      								</td>
      							</tr>
      						@endforeach
          			</table>
          		</div>
          	</div>
          	<div class="x_footer">
          		<div class="col-xs-6">
          			<a href="{{url('admin')}}" class="btn btn-info">
          				<i class="fa fa-chevron-left"></i>
          				@lang('message.back')
          			</a>
          		</div>
          		<div class="col-xs-6 text-right">
					<a href="{{url('client/create')}}" class="btn btn-info">
						<i class="fa fa-plus"></i>
						@lang('message.create')
					</a>
          		</div>
          	</div>
        </div>
  	</div>
</div>
@endsection
