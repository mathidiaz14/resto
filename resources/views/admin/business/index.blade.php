@extends('layouts.dashboard')

@section('title')
	@lang('message.settings')
@endsection

@section('css')
	
@endsection

@section('content')

<div class="row">
	<div class="col-xs-12">
      	<div class="x_panel">
        	<div class="x_title">
     		<div class="row">
                    <div class="col-xs-6">
                         <h3>@lang('message.settings')</h3>
                    </div>
                    <div class="col-xs-6 text-right">
                         <a href="{{url('admin')}}" class="btn btn-default">
                              <i class="fa fa-chevron-left"></i>
                              @lang('message.back')
                         </a>
                    </div>
               </div>

     		<div class="clearfix"></div>
        	</div>
        	<div class="x_content">
          		<div class="col-sm-12 col-md-8 col-md-offset-2">
          			<form action="{{url('business')}}" method="POST" class="form-horizontal">
          				@csrf
                              <div class="form-group">
                                   <label for="" class="form-label">
                                        @lang('message.api_code')
                                   </label>
                                   <div class="row">
                                        <div class="col-xs-11">
                                             <input type="text" class="form-control" value="{{$business->api_code}}" id="api_code" readonly="">      
                                        </div>
                                        <div class="col-xs-1">
                                             <a class="btn btn-block btn-info btn-copy">
                                                  <i class="fa fa-copy"></i>
                                             </a>    
                                        </div>
                                   </div>
                              </div>
          				<div class="form-group">
          					<label for="" class="form-label">
          						@lang('message.name')
          					</label>
          					<input type="text" class="form-control" name="name" value="{{$business->name}}">
          				</div>
          				<div class="form-group">
          					<label for="" class="form-label">
          						@lang('message.email')
          					</label>
          					<input type="text" class="form-control" name="email" value="{{$business->email}}">
          				</div>
          				<div class="form-group">
          					<label for="" class="form-label">
          						@lang('message.phone')
          					</label>
          					<input type="text" class="form-control" name="phone" value="{{$business->phone}}">
          				</div>
          				<div class="form-group">
          					<label for="" class="form-label">
          						@lang('message.rut')
          					</label>
          					<input type="text" class="form-control" name="rut" value="{{$business->rut}}">
          				</div>

          				<div class="form-group">
          					<label for="" class="form-label">
          						@lang('message.address')
          					</label>
          					<input type="text" class="form-control" name="address" value="{{$business->address}}">
          				</div>

                              <div class="form-group">
                                   <label for="" class="form-label">
                                        @lang('message.printer_admin')
                                   </label>
                                   <select name="printer_admin" id="" class="form-control">
                                        @if($business->printer_admin == NULL)
                                             <option value="NULL" selected>Ninguna</option>
                                        @else
                                             <option value="NULL" selected>Ninguna</option>
                                        @endif
                                        @foreach(Auth::user()->business->printers as $printer)
                                             @if($printer->id == $business->printer_admin)
                                                  <option value="{{$printer->id}}" selected>{{$printer->name}}</option>
                                             @else
                                                  <option value="{{$printer->id}}">{{$printer->name}}</option>
                                             @endif
                                        @endforeach
                                   </select>
                              </div>

                              <div class="form-group">
                                   <label>
                                        <br>
                                        <input type="checkbox" class="js-switch" name="virtual_keyboard"  id="virtual_keyboard"
                                        @if($business->virtual_keyboard != null)
                                             checked
                                        @endif />
                                        @lang('message.virtual_keyboard')
                                   </label>
                              </div>

		          		<div class="form-group text-right">
		          			<button class="btn btn-primary">
		          				<i class="fa fa-save"></i>
		          				Guardar
		          			</button>
		          		</div>
          			</form>
          		</div>
          	</div>
          	<div class="x_footer">
          	</div>
        </div>
  	</div>
</div>
@endsection

@section('scripts')
     <script>
          $('.btn-copy').click(function()
          {
               var $temp = $("<input>")
               $("body").append($temp);
               $temp.val($('#api_code').text()).select();
               document.execCommand("copy");
               console.log('COPIADO');
          });
     </script>
@endsection
