@extends('layouts.dashboard')

@section('title')
	@lang('message.products')
@endsection

@section('content')

<div class="row">
	<div class="col-xs-12">
      	<div class="x_panel">
        	<div class="x_title">
          		<h2>@lang('message.products_create')</h2>
          		<div class="clearfix"></div>
        	</div>
        	<div class="x_content">
          		<form action="{{url('product')}}" method="POST" class="form-horizontal col-xs-12 col-md-10 col-md-offset-1">
          			@csrf
          			<div class="form-group">
          				<label for="">@lang('message.name')</label>
          				<input type="text" name="name" class="form-control">
          			</div>
          			<div class="form-group">
          				<label for="">@lang('message.price')</label>
          				<input type="text" name="price" class="form-control">
          			</div>
          			<div class="form-group">
          				<label for="">@lang('message.quantity')</label>
          				<input type="number" name="quantity" class="form-control">
          			</div>
          			<div class="form-group">
          				<label for="">@lang('message.quantity_min')</label>
          				<br>
          				<div class="row">
          					<div class="col-xs-2">
	          					<div class="col-xs-6">
	          						<input type="radio" name="quantity_min_option" value="yes" class="quantity_min_option">
		          					<label for="">@lang('message.yes')</label>
	          					</div>
	          					<div class="col-xs-6">
			          				<input type="radio" name="quantity_min_option" value="no" class="quantity_min_option" checked>
			          				<label for="">@lang('message.no')</label>
			          			</div>
	          				</div>
							<div class="col-xs-10">
								<input type="number" name="quantity_min" id="quantity_min" class="form-control" disabled>
							</div>
          				</div>
          			</div>
					<div class="form-group">
						<label for="">@lang('message.category')</label>
						<select name="category_id" class="form-control">
							@foreach(Auth::user()->business->categories as $category)
								<option value="{{$category->id}}">{{$category->name}}</option>
							@endforeach
						</select>
						@if(Auth::user()->business->categories->count() == '0')
							<span class="text-danger">@lang('message.no_categories') <a href="{{url('category/create')}}">@lang('message.click_here')</a></span>
						@endif
					</div>
					<div class="form-group">
						<label for="">@lang('message.printer')</label>
						<select name="printer_id" class="form-control">
							@foreach(Auth::user()->business->printers as $printer)
								<option value="{{$printer->id}}">{{$printer->name}}</option>
							@endforeach
						</select>
						@if(Auth::user()->business->printers->count() == '0')
							<span class="text-danger">@lang('message.no_printers') <a href="{{url('product/create')}}">@lang('message.click_here')</a></span>
						@endif
					</div>
					<div class="form-group">
						<label for="">@lang('message.supplier')</label>
						<select name="supplier_id" class="form-control">
							@foreach(Auth::user()->business->suppliers as $supplier)
								<option value="{{$supplier->id}}">{{$supplier->name}}</option>
							@endforeach
						</select>
						@if(Auth::user()->business->suppliers->count() == '0')
							<span class="text-danger">@lang('message.no_suppliers') <a href="{{url('supplier/create')}}">@lang('message.click_here')</a></span>
						@endif
					</div>
          			<div class="form-group">
          				@if((Auth::user()->business->categories->count() == '0') or (Auth::user()->business->printers->count() == '0'))
	          				<button class="btn btn-block btn-info" disabled>
	          					<i class="fa fa-save"></i>
	          					@lang('message.save')
	          				</button>
	          			@else
							<button class="btn btn-block btn-info">
	          					<i class="fa fa-save"></i>
	          					@lang('message.save')
	          				</button>
	          			@endif
          			</div>
          		</form>
          	</div>
          	<div class="x_footer">
          		<div class="col-xs-6">
          			<a href="{{URL::Previous()}}" class="btn btn-info">
          				<i class="fa fa-chevron-left"></i>
          				@lang('message.back')
          			</a>
          		</div>
          	</div>
        </div>
  	</div>
</div>
@endsection

@section('scripts')
	<script>
		$(document).ready(function()
		{
			$('.quantity_min_option').change(function(){
				if ($(this).attr('value') == "yes")
				{
					$("#quantity_min").prop('disabled', false);
				}
				else
				{
					$("#quantity_min").prop('disabled', true);
				}
			});
		});
	</script>
@endsection