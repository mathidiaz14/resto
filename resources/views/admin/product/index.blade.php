@extends('layouts.dashboard')

@section('title')
	@lang('message.products')
@endsection

@section('content')

<div class="row">
	<div class="col-xs-12">
      	<div class="x_panel">
        	<div class="x_title">
          		<h2>@lang('message.products')</h2>
          		<div class="clearfix"></div>
        	</div>
        	<div class="x_content">
          		<div class="table-responsive">
          			<table class="table table-striped">
          				<th>@lang('message.name')</th>
          				<th>@lang('message.quantity')</th>
          				<th>@lang('message.price')</th>
          				<th>@lang('message.category')</th>
          				<th>@lang('message.view')</th>
          				<th>@lang('message.edit')</th>
          				<th>@lang('message.delete')</th>
          				
          				@foreach($products as $product)
      							<tr>
      								<td>{{$product->name}}</td>
      								<td>{{$product->quantity}}</td>
      								<td>${{$product->price}}</td>
      								<td>{{$product->category->name}}</td>
      								<td>
      									<a href="{{url('product', $product->id)}}" class="btn btn-success">
      										<i class="fa fa-eye"></i>	
      									</a>
      								</td>
      								<td>
      									<a href="{{route('product.edit', $product->id)}}" class="btn btn-primary">
      										<i class="fa fa-edit"></i>
      									</a>
      								</td>
      								<td>
      									@include('helpers.delete', ['id' => $product->id, 'route' => 'product.destroy'])
      								</td>
      							</tr>
      						@endforeach
          			</table>
          		</div>
          	</div>
          	<div class="x_footer">
          		<div class="col-xs-6">
          			<a href="{{url('admin')}}" class="btn btn-info">
          				<i class="fa fa-chevron-left"></i>
          				@lang('message.back')
          			</a>
          		</div>
          		<div class="col-xs-6 text-right">
					<a href="{{url('product/create')}}" class="btn btn-info">
						<i class="fa fa-plus"></i>
						@lang('message.create')
					</a>
          		</div>
          	</div>
        </div>
  	</div>
</div>
@endsection
