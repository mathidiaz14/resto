@extends('layouts.dashboard')

@section('title')
	@lang('message.products')
@endsection

@section('content')

<div class="row">
	<div class="col-xs-12">
      	<div class="x_panel">
        	<div class="x_title">
          		<h2>@lang('message.product')</h2>
          		<div class="clearfix"></div>
        	</div>
        	<div class="x_content">
          		<form action="" method="POST" class="form-horizontal col-xs-12 col-md-10 col-md-offset-1">
          			@csrf
          			<input type="hidden" name="_method" value="PATCH">
          			<div class="form-group">
          				<label for="">@lang('message.name')</label>
          				<input type="text" name="name" class="form-control" value="{{$product->name}}" disabled>
          			</div>
          			<div class="form-group">
          				<label for="">@lang('message.price')</label>
          				<input type="text" name="price" class="form-control" value="{{$product->price}}" disabled>
          			</div>
          			<div class="form-group">
          				<label for="">@lang('message.quantity')</label>
          				<input type="number" name="quantity" class="form-control" value="{{$product->quantity}}" disabled>
          			</div>
          			<div class="form-group">
          				<label for="">@lang('message.quantity_min')</label>
          				<br>
          				<div class="row">
          					<div class="col-xs-2">
	          					<div class="col-xs-6">
	          						@if($product->quantity_min != "no")
	          							<input type="radio" name="quantity_min_option" value="yes" class="quantity_min_option" checked disabled>
	          						@else
										<input type="radio" name="quantity_min_option" value="yes" class="quantity_min_option" disabled>
	          						@endif
		          					<label for="">@lang('message.yes')</label>
	          					</div>
	          					<div class="col-xs-6">
	          						@if($product->quantity_min == "no")
		          						<input type="radio" name="quantity_min_option" value="no" class="quantity_min_option" checked disabled>
		          					@else
										<input type="radio" name="quantity_min_option" value="no" class="quantity_min_option" disabled>
		          					@endif
			          				<label for="">@lang('message.no')</label>
			          			</div>
	          				</div>
							<div class="col-xs-10">
								@if($product->quantity_min == "no")
									<input type="number" name="quantity_min" id="quantity_min" class="form-control" disabled>
								@else
									<input type="number" name="quantity_min" id="quantity_min" class="form-control" value="{{$product->quantity_min}}" disabled>
								@endif
							</div>
          				</div>
          			</div>
					<div class="form-group">
						<label for="">@lang('message.category')</label>
						<select name="category_id" class="form-control" disabled>
							@foreach(Auth::user()->business->categories as $category)
								@if($category->id == $product->category_id)
									<option value="{{$category->id}}" selected>{{$category->name}}</option>
								@else
									<option value="{{$category->id}}">{{$category->name}}</option>
								@endif
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<label for="">@lang('message.printer')</label>
						<select name="printer_id" class="form-control" disabled>
							@foreach(Auth::user()->business->printers as $printer)
								@if($printer->id == $product->printer_id)
									<option value="{{$printer->id}}" selected>{{$printer->name}}</option>
								@else
									<option value="{{$printer->id}}">{{$printer->name}}</option>
								@endif
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<label for="">@lang('message.supplier')</label>
						<select name="supplier_id" class="form-control" disabled>
							@foreach(Auth::user()->business->suppliers as $supplier)
								@if($supplier->id == $product->supplier_id)
									<option value="{{$supplier->id}}" selected>{{$supplier->name}}</option>
								@else
									<option value="{{$supplier->id}}">{{$supplier->name}}</option>
								@endif
							@endforeach
						</select>
					</div>
          		</form>
          	</div>
          	<div class="x_footer">
          		<div class="col-xs-6">
          			<a href="{{URL::Previous()}}" class="btn btn-info">
          				<i class="fa fa-chevron-left"></i>
          				@lang('message.back')
          			</a>
          		</div>
          	</div>
        </div>
  	</div>
</div>
@endsection