@extends('layouts.dashboard')

@section('title')
	@lang('message.printers')
@endsection

@section('content')

<div class="row">
	<div class="col-xs-12">
      	<div class="x_panel">
        	<div class="x_title">
        		<div class="row">
        			<div class="col-xs-6">
		          		<h3>@lang('message.printers')</h3>
		          		<div class="clearfix"></div>
        			</div>
        			<div class="col-xs-6 text-right">
        				<a href="{{url('admin')}}" class="btn btn-default">
	          				<i class="fa fa-chevron-left"></i>
	          				@lang('message.back')
	          			</a>
        			</div>
        		</div>
        	</div>
        	<div class="x_content">
          		<div class="table-responsive">
          			<table class="table table-striped">
          				<th>@lang('message.name')</th>
          				<th>@lang('message.code')</th>
          				<th>@lang('message.status')</th>
          				<th>@lang('message.view')</th>
          				<th>@lang('message.edit')</th>
          				<th>@lang('message.test')</th>
          				<th>@lang('message.delete')</th>
          				
          				@foreach($printers as $printer)
							<tr>
								<td>{{$printer->name}}</td>
								<td>{{$printer->code}}</td>
								<td>
									@if(check_printer($printer->id))
										<p style="padding:10px; width:110px; border-radius: 5px; color:white; background: #28bd28; text-align: center; ">Online</p>
									@else
										<p style="padding:10px; width:110px; border-radius: 5px; color:white; background: #e02c2c; text-align: center; ">Fuera de linea</p>
									@endif
								</td>
								<td>
									<a href="{{url('printer', $printer->id)}}" class="btn btn-success">
										<i class="fa fa-eye"></i>
									</a>
								</td>
								<td>
									<a href="{{route('printer.edit', $printer->id)}}" class="btn btn-primary">
										<i class="fa fa-edit"></i>
									</a>
								</td>
								<td>
									@if(\Carbon\Carbon::now()->diffInSeconds($printer->updated_at) < 10)
										<a href="{{url('printer/test', $printer->id)}}" class="btn btn-warning">
											<i class="fa fa-print"></i>
										</a>
									@else
										<a class="btn btn-warning disabled" disabled>
											<i class="fa fa-print"></i>
										</a>
									@endif
								</td>
								<td>
									@include('helpers.delete', ['id' => $printer->id, 'route' => 'printer.destroy'])
								</td>
							</tr>
						@endforeach
          			</table>
          		</div>
          	</div>
          	<div class="x_footer">
          		<div class="col-xs-6">
          			
          		</div>
          		<div class="col-xs-6 text-right">
					<a href="{{url('printer/create')}}" class="btn btn-info">
						<i class="fa fa-plus"></i>
						@lang('message.create')
					</a>
          		</div>
          	</div>
        </div>
  	</div>
</div>
@endsection
