@extends('layouts.dashboard')

@section('title')
	@lang('message.categories')
@endsection

@section('content')

<div class="row">
	<div class="col-xs-12">
      	<div class="x_panel">
        	<div class="x_title">
        		<div class="row">
        			<div class="col-xs-6">
        				<h3>@lang('message.products_of'): <b>{{$printer->name}}</b></h3>
          				<div class="clearfix"></div>	
        			</div>
        			<div class="col-xs-6 text-right">
        				<a href="{{url('printer')}}" class="btn btn-default">
	          				<i class="fa fa-chevron-left"></i>
	          				@lang('message.back')
	          			</a>
        			</div>
        		</div>
        	</div>
        	<div class="x_content">
          		<div class="table-responsive">
          			<table class="table table-striped">
          				<th>@lang('message.name')</th>
          				<th>@lang('message.quantity')</th>
          				<th>@lang('message.price')</th>
          				<th>@lang('message.view')</th>
          				<th>@lang('message.edit')</th>
          				<th>@lang('message.delete')</th>
          				
          				@foreach($printer->products->where('status', '!=', 'deleted') as $product)
							<tr>
								<td>{{$product->name}}</td>
								<td>{{$product->quantity}}</td>
								<td>${{$product->price}}</td>
								<td>
									<a href="{{url('product', $product->id)}}" class="btn btn-success">
										<i class="fa fa-eye"></i>	
									</a>
								</td>
								<td>
									<a href="{{route('product.edit', $product->id)}}" class="btn btn-primary">
										<i class="fa fa-edit"></i>
									</a>
								</td>
								<td>
									<!-- Button trigger modal -->
									<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal">
										<i class="fa fa-trash"></i>
									</button>

									<!-- Modal -->
									<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													
												</div>
												<div class="modal-body text-center">
													<h3>@lang('message.delete_question')</h3>
												</div>
												<div class="modal-footer">
													<div class="col-xs-6">
														<button type="button" class="btn btn-default btn-block" data-dismiss="modal">@lang('message.no')</button>
													</div>
													<div class="col-xs-6">
														<form action="{{ route('product.destroy', $product->id) }}" method="POST">
					                                        {{ csrf_field() }}
					                                        <input type="hidden" name="_method" value="DELETE">
					                                        <button type="submit" class="btn btn-danger btn-block">   
					                                            @lang('message.yes')
					                                        </button>
					                                    </form>
													</div>
												</div>
											</div>
										</div>
									</div>
								</td>
							</tr>
						@endforeach
          			</table>
          		</div>
          	</div>
          	<div class="x_footer">
          		<div class="col-xs-6">
          			
          		</div>
          		<div class="col-xs-6 text-right">
					
          		</div>
          	</div>
        </div>
  	</div>
</div>
@endsection
