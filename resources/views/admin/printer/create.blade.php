@extends('layouts.dashboard')

@section('title')
	@lang('message.printers')
@endsection

@section('content')

<div class="row">
	<div class="col-xs-12">
      	<div class="x_panel">
        	<div class="x_title">
          		<div class="row">
                <div class="col-xs-6">
                  <h2>@lang('message.printers_create')</h2>
                  <div class="clearfix"></div>
                </div>
                <div class="col-xs-6 text-right">
                  <a href="{{URL::Previous()}}" class="btn btn-default">
                    <i class="fa fa-chevron-left"></i>
                    @lang('message.back')
                  </a>  
                </div>
              </div>
        	</div>
        	<div class="x_content">
          		<form action="{{url('printer')}}" method="POST" class="form-horizontal col-xs-12 col-md-6 col-md-offset-3">
          			@csrf
          			<div class="form-group">
          				<label for="">@lang('message.name')</label>
          				<input type="text" name="name" class="form-control">
          			</div>

                @php
                  $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                  
                  $charactersLength = strlen($characters);
                  
                  $randomString = '';

                  for ($i = 0; $i < 10; $i++)
                      $randomString .= $characters[rand(0, $charactersLength - 1)];
                @endphp

          			<div class="form-group">
          				<label for="">@lang('message.code')</label>
          				<input type="text" name="code" class="form-control" value="{{$randomString}}" readonly>
          			</div>
          			<div class="form-group text-right">
          				<button class="btn btn-info">
          					<i class="fa fa-save"></i>
          					@lang('message.save')
          				</button>
          			</div>
          		</form>
          	</div>
          	<div class="x_footer">
          		<div class="col-xs-6">
          			
          		</div>
          	</div>
        </div>
  	</div>
</div>
@endsection
