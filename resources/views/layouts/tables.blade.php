<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <title>Resto.uy</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel=stylesheet href="{{asset('css/style.css')}}">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
        <link rel="stylesheet" href="{{asset('css/keyboard.css')}}">
        <link rel="stylesheet" href="{{asset('css/keyboard-dark.css')}}">
        <link rel="shortcut icon" type="image/png" href="{{asset('images/logo.png')}}">

        <meta name="csrf-token" content="{{ Session::token() }}">
        @yield('css')
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    </head>
    <body>
        <div id="app">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    <img src="{{asset('images/logo.png')}}" alt="" height="40px" style="margin:5px;">
                    </div>

                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="active">
                                <a href="{{url('home')}}">
                                    <span>
                                        <i class="fa fa-home"></i>
                                        @lang('message.home')
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('account')}}">
                                    <span>
                                        <i class="fa fa-user"></i>
                                        @lang('message.account')
                                    </span>
                                </a>
                            </li>
                            @if(Auth::user()->type == "admin")
                                <li>
                                    <a href="{{url('admin')}}">
                                        <span>
                                            <i class="fa fa-tachometer-alt"></i>
                                            @lang('message.dashboard')
                                        </span>
                                    </a>
                                </li>
                            @endif
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="navbar-form navbar-left">
                                    @csrf
                                    <button class="btn btn-primary">
                                        <i class="fa fa-sign-out-alt"></i>
                                        @lang('message.logout')
                                    </button>
                                </form>
                            </li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
            
            @yield('content')

        </div>

        
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

        @if (Auth::user()->business->virtual_keyboard == "YES")
            <script src="{{asset('js/jquery.keyboard.js')}}"></script>
            <script src="{{asset('js/jquery.keyboard.extension-all.js')}}"></script>
        
            <script>
                $('.virtual').keyboard({ layout: 'alpha' }).addTyping();
            </script>
        @endif

        @yield('scripts')

    </body>
</html>