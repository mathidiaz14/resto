<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@lang('message.page_name') - @yield('title')</title>

    <link href="{{asset('dashboard/vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('dashboard/vendors/nprogress/nprogress.css')}}" rel="stylesheet">
    <link href="{{asset('dashboard/vendors/iCheck/skins/flat/green.css')}}" rel="stylesheet">
    <link href="{{asset('dashboard/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css')}}" rel="stylesheet">
    <link href="{{asset('dashboard/vendors/jqvmap/dist/jqvmap.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('dashboard/vendors/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">
    <link href="{{asset('dashboard/vendors/switchery/dist/switchery.min.css')}}" rel="stylesheet">
    <link href="{{asset('dashboard/css/custom.css')}}" rel="stylesheet">
    
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    
    <link rel="shortcut icon" type="image/png" href="{{asset('images/logo.png')}}">
    
    @yield('css')
</head>

<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                        <a href="{{url('room')}}" class="site_title">
                            <img src="{{asset('images/logo.png')}}" alt="" width="50px" style="margin:5px;">
                            <span>@lang('message.page_name')</span>
                        </a>
                    </div>

                    <div class="clearfix"></div>

                    <!-- menu profile quick info -->
                    <div class="profile clearfix">
                        <div class="profile_info">
                            <span>@lang('message.welcome'),</span>
                            <h2>{{Auth::user()->name}}</h2>
                        </div>
                    </div>

                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <ul class="nav side-menu">
                                <li>
                                    <a href="{{url('admin')}}">
                                        <i class="fas fa-tachometer-alt"></i> @lang('message.dashboard') 
                                        <span class="fas fa-chevron-right" style="float:right;"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{url('home')}}">
                                        <i class="fas fa-home "> </i> @lang('message.tables') <span style="float:right;" class="fas fa-chevron-right"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{url('arching')}}">
                                        <i class="fas fa-clock"> </i> @lang('message.arches') <span style="float:right;" class="fas fa-chevron-right"></span>
                                    </a>
                                </li>
                                <li>
                                    <a><i class="fas fa-edit"></i> @lang('message.categories') <span style="float:right;" class="fas fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <li><a href="{{url('category')}}">@lang('message.list')</a></li>
                                        <li><a href="{{url('category/create')}}">@lang('message.create')</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a><i class="fas fa-boxes"></i> @lang('message.products') <span style="float:right;" class="fas fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <li><a href="{{url('product')}}">@lang('message.list')</a></li>
                                        <li><a href="{{url('product/create')}}">@lang('message.create')</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a><i class="fas fa-parachute-box"></i> @lang('message.suppliers') <span style="float:right;" class="fas fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <li><a href="{{url('supplier')}}">@lang('message.list')</a></li>
                                        <li><a href="{{url('supplier/create')}}">@lang('message.create')</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a><i class="fas fa-print"></i> @lang('message.printers') <span style="float:right;" class="fas fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <li><a href="{{url('printer')}}">@lang('message.list')</a></li>
                                        <li><a href="{{url('printer/create')}}">@lang('message.create')</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a><i class="fas fa-users"></i> @lang('message.clients') <span style="float:right;" class="fas fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <li><a href="{{url('client')}}">@lang('message.list')</a></li>
                                        <li><a href="{{url('client/create')}}">@lang('message.create')</a></li>
                                    </ul>
                                </li>
                                @if(Auth::user()->type == "admin")
                                    <li>
                                        <a><i class="fas fa-user"></i> @lang('message.users') <span style="float:right;" class="fas fa-chevron-down"></span></a>
                                        <ul class="nav child_menu">
                                            <li><a href="{{url('user')}}">@lang('message.list')</a></li>
                                            <li><a href="{{url('user/create')}}">@lang('message.create')</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="{{url('business')}}">
                                            <i class="fas fa-tools"></i> @lang('message.settings') <span style="float:right;" class="fas fa-chevron-right"></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('notification')}}">
                                            <i class="fas fa-envelope"></i> @lang('message.notifications') <span style="float:right;" class="fas fa-chevron-right"></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('ticket')}}">
                                            <i class="fas fa-scroll"> </i> @lang('message.tickets') <span style="float:right;" class="fas fa-chevron-right"></span>
                                        </a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                    <!-- /sidebar menu -->
                </div>
            </div>

            <!-- top navigation -->
            <div class="top_nav">
                <div class="nav_menu">
                    <nav>
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fas fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    {{Auth::user()->name}}    
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu pull-right">
                                    <li><a href="javascript:;"> Profile</a></li>
                                    <li><a href="javascript:;">Help</a></li>
                                    
                                    <li>
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                            <i class="fa fa-sign-out pull-right"></i> Cerrar sesión
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </li>
                                </ul>
                            </li>
                            <li role="presentation" class="dropdown" id="notifications">
                                
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        
            <!-- page content -->
            <div class="right_col" role="main">
                <div class="row">
                    <div class="col-xs-12">
                        @include('helpers.alert')
                    </div>
                </div>
                @yield('content')
            </div>
            <!-- /page content -->

            <footer>
                <div class="pull-right">
                    Created by <a href="http://mathiasdiaz.uy" target="_blank">Mathias Díaz.uy</a>
                </div>
                <div class="clearfix"></div>
            </footer>
    
        </div>
    </div>

    <!-- jQuery -->
    <script src="{{asset('dashboard/vendors/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{asset('dashboard/vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{asset('dashboard/vendors/fastclick/lib/fastclick.js')}}"></script>
    <!-- NProgress -->
    <script src="{{asset('dashboard/vendors/nprogress/nprogress.js')}}"></script>
    <!-- Chart.js -->
    <script src="{{asset('dashboard/vendors/Chart.js/dist/Chart.min.js')}}"></script>
    <!-- gauge.js -->
    <script src="{{asset('dashboard/vendors/gauge.js/dist/gauge.min.js')}}"></script>
    <!-- bootstrap-progressbar -->
    <script src="{{asset('dashboard/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js')}}"></script>
    <!-- iCheck -->
    <script src="{{asset('dashboard/vendors/iCheck/icheck.min.js')}}"></script>
    <!-- Skycons -->
    <script src="{{asset('dashboard/vendors/skycons/skycons.js')}}"></script>
    <!-- Flot -->
    <script src="{{asset('dashboard/vendors/Flot/jquery.flot.js')}}"></script>
    <script src="{{asset('dashboard/vendors/Flot/jquery.flot.pie.js')}}"></script>
    <script src="{{asset('dashboard/vendors/Flot/jquery.flot.time.js')}}"></script>
    <script src="{{asset('dashboard/vendors/Flot/jquery.flot.stack.js')}}"></script>
    <script src="{{asset('dashboard/vendors/Flot/jquery.flot.resize.js')}}"></script>
    <!-- Flot plugins -->
    <script src="{{asset('dashboard/vendors/flot.orderbars/js/jquery.flot.orderBars.js')}}"></script>
    <script src="{{asset('dashboard/vendors/flot-spline/js/jquery.flot.spline.min.js')}}"></script>
    <script src="{{asset('dashboard/vendors/flot.curvedlines/curvedLines.js')}}"></script>
    <!-- DateJS -->
    <script src="{{asset('dashboard/vendors/DateJS/build/date.js')}}"></script>
    <!-- JQVMap -->
    <script src="{{asset('dashboard/vendors/jqvmap/dist/jquery.vmap.js')}}"></script>
    <script src="{{asset('dashboard/vendors/jqvmap/dist/maps/jquery.vmap.world.js')}}"></script>
    <script src="{{asset('dashboard/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js')}}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{asset('dashboard/vendors/moment/min/moment.min.js')}}"></script>
    <script src="{{asset('dashboard/vendors/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <!-- Custom Theme Scripts -->
    <script src="{{asset('dashboard/js/custom.js')}}"></script>
    <script src="{{asset('dashboard/vendors/switchery/dist/switchery.min.js')}}"></script>
    
    @yield('scripts')
    
    <script>
        
        $('#notifications').load('{{url("notification/all")}}');
        
        

        $('#notifications').click(function()
        {
            $.get('{{url("notification/read")}}', function()
            {
                $('#notifications').load('{{url("notification/all")}}');
            });
        });


        function load_notifications()
        {
            $.get('{{url("notification/query")}}', function($response)
            {
                if($response > $('#notification_quantity').val())
                {
                    $('#notifications').load('{{url("notification/all")}}');     
                }
            });

            //setTimeout(load_notifications(), 60000);
        }

    </script>
</body>
</html>
