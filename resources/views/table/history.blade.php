@extends('layouts.tables')

@section('content')
<div class="container-fluid table-container">
    <div class="row body">
		<div class="col-xs-12 details_title text-center bg-primary">
            <div class="col-xs-6 col-xs-offset-3">
                <p>@lang('message.commands_table'): {{$table->fixed->name}}</p>
            </div>
            <div class="col-xs-3 text-right">
                <a href="{{URL::Previous()}}" class="btn btn-default">
                    <i class="fa fa-chevron-left"></i>
                    Volver
                </a>
            </div>
        </div>
        <div class="col-xs-12 col-md-10 col-md-offset-1 text-center">
        	<div class="table table-responsive">
        		<br>
        		<table class="table table_border table-striped text-center" >
            			<tr>
            				<th class="text-center">@lang('message.product')</th>
                            <th class="text-center">@lang('message.quantity')</th>
            				<th class="text-center">@lang('message.price')</th>
            				<th class="text-center"></th>
            			</tr>
            			@foreach($commands_list as $commands)
							<tr>
								<td>{{$commands->first()->product->name}}</td>
                                <td>x {{$commands->count()}}</td>
								<td>$ {{$commands->first()->price * $commands->count()}}</td>
								<td>
                                    @if((Auth::user()->type == "admin") or (Auth::user()->type == "cashier"))
                                        @if($commands->count() > 1)                      
                                            <!-- Button trigger modal -->
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#commands_delete_all_{{$commands->first()->id}}">
                                                <i class="fa fa-times"></i>
                                            </button>
                        
                                            <!-- Modal -->
                                            <div class="modal fade" id="commands_delete_all_{{$commands->first()->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        </div>
                                                        <form action="{{url('command/delete/all')}}" method="POST">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="table" value="{{$table->id}}">
                                                            <input type="hidden" name="product" value="{{$commands->first()->product->id}}">
                                                            <input type="hidden" name="status" value="enabled">
                                                            
                                                            <div class="modal-body text-center">
                                                                <h3>@lang('message.delete_question_number')</h3>
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-md-4 col-md-offset-4">
                                                                        <input type="number" name="quantity" value="1" class="form-control" min=1 max={{$commands->count()}}>
                                                                        <br>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <div class="col-xs-6">
                                                                    <button type="button" class="btn btn-default btn-block" data-dismiss="modal">@lang('message.no')</button>
                                                                </div>
                                                                <div class="col-xs-6">
                                                                    <button type="submit" class="btn btn-danger btn-block">   
                                                                        @lang('message.yes')
                                                                    </button>     
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>       
                                        @else
                                            <!-- Button trigger modal -->
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#commands_delete_{{$commands->first()->id}}">
                                                <i class="fa fa-times"></i>
                                            </button>
                        
                                            <!-- Modal -->
                                            <div class="modal fade" id="commands_delete_{{$commands->first()->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        </div>
                                                        <div class="modal-body text-center">
                                                            <h3>@lang('message.delete_question')</h3>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <div class="col-xs-6">
                                                                <button type="button" class="btn btn-default btn-block" data-dismiss="modal">@lang('message.no')</button>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <form action="{{ route('command.destroy', $commands->first()->id) }}" method="POST">
                                                                    {{ csrf_field() }}
                                                                    <input type="hidden" name="_method" value="DELETE">
                                                                    
                                                                    <button type="submit" class="btn btn-danger btn-block">   
                                                                        @lang('message.yes')
                                                                    </button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                       
                                        @endif
                                    @endif
                                </td>
							</tr>
            			@endforeach
                        <tfoot>
                            <td></td>
                            <td></td>
                            <td><b>@lang('message.total'):</b> <b>$</b><b id="total">{{$table->commands->where('status', 'enabled')->sum('price')}}</b></td>
                            <td></td>
                        </tfoot>
            		</table>
        	</div>
        </div>
    </div>
</div>
@endsection




