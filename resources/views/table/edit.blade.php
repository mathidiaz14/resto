@extends('layouts.tables')

@section('content')
<div class="container-fluid table-container">
    <div class="row">
        @include('table.parts.clock')
        <div class="col-sm-12 col-md-9">
            
            {{-- Navegacion de las mesas--}}
            <ul class="nav nav-tabs">
                @foreach($business->rooms->where('status', 'enabled') as $room_all)
                    @if($room->id == $room_all->id)
                        <li role="presentation" class="active">
                            <a class="show_rooms nav-link active">
                                {{$room_all->name}}
                            </a>
                        </li>
                    @else
                        <li role="presentation">
                            <a class="show_rooms nav-link" href="{{route('room.edit', $room_all->id)}}">
                                {{$room_all->name}}
                            </a>
                        </li>
                    @endif
                @endforeach
                <li>
                    <a class="nav-link" data-toggle="modal" data-target="#add_room">
                        <i class="fa fa-plus"></i>
                    </a>

                    <div class="modal fade" id="add_room" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header text-left">
                                    <div class="col-sm-9">
                                        <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-plus"></i> @lang('message.add_room')</h4>
                                    </div>
                                    <div class="col-sm-3">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-sm-10 col-sm-offset-1">
                                            <form action="{{url('room')}}" method="POST" class="form-horizontal">
                                                @csrf
                                                <div class="form-group">
                                                    <h4>@lang('message.name')</h4>
                                                    <input type="text" class="form-control" name="name" id="name">
                                                </div>
                                                <div class="form-group text-right">
                                                    <button class="btn btn-info">
                                                        <i class="fa fa-save"></i>
                                                        @lang('message.save')
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </li>
            </ul>
            {{-- Fin navegacion de las mesas--}}
        </div>
    </div>
    <div class="row body">
        <div class="col-xs-12 col-md-3 details text-left">
            <div class="details_room">
                <div class="col-xs-12 details_title text-center bg-primary">
                    <p>
                        {{$room->name}}
                        
                        @if($room->fixeds->where('status', '!=', 'enabled')->count() == 0)
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal" style="position:absolute; right: 10px;">
                                <i class="fa fa-trash"></i>
                            </button>
                        @else
                            <a class="btn btn-danger" style="position:absolute; right: 10px;" disabled>
                                <i class="fa fa-trash"></i>
                            </a>
                        @endif

                    </p>
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"></h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <h4 class="text-muted">@lang('message.want_eliminate_room')</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <button type="button" class="btn btn-default btn-block" data-dismiss="modal">NO</button>    
                                        </div>
                                        <div class="col-xs-6">
                                            <form action="{{ route('room.destroy', $room->id) }}" method="POST">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="_method" value="DELETE">
                                                <button type="submit" class="btn btn-danger btn-block">   
                                                    SI
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-10 col-xs-offset-1 text-left">
                    <br>
                    <form action="{{url('room' , $room->id)}}" method="POST" class="form-horizontal">
                        @csrf
                        <input type="hidden" name="_method" value="PATCH">
                        <div class="form-group">
                            <label for="" class="label-control">@lang('message.name')</label>
                            <input type="text" class="form-control " name="name" value="{{$room->name}}">
                        </div>
                        <div class="form-group">
                            <button class="btn btn-info btn-block">
                                <i class="fa fa-save"></i>
                                @lang('message.save')
                            </button>
                        </div>
                        <div class="form-group">
                            <a href="{{url('room', $room->id)}}" class="btn btn-block btn-default">
                                <i class="fa fa-chevron-left"></i>
                                Volver
                            </a>
                        </div>
                    </form>
                    <br>
                    <div class="col-xs-12 text-center">    
                        <i class="fa fa-chevron-right visible-md visible-lg"></i>
                        <i class="fa fa-chevron-down visible-sm visible-xs"></i>
                        @lang('message.edit_table')
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-9 tables">
            <table>
                <tbody>
                    @for($y = 0; $y < 10; $y++)
                        <tr>
                            @for($x = 0; $x < 10; $x++)
                                <td class="edited">
                                    @php $flag = true; @endphp

                                    @foreach($room->fixeds->where('status', '!=', 'deleted') as $fixed)
                                        @if(($fixed->x == $x) and ($fixed->y == $y))
                                            
                                            {{-- La fomra de la mesa --}}
                                            <input type="text" id="{{$x}}{{$y}}" class="btn btn_table {{$fixed->type}} {{$fixed->status}}" name="table_{{$fixed->id}}" value="{{$fixed->name}}" table-id="{{$fixed->id}}">
                                            
                                            {{-- Si no esta ocupada muestro boton para eliminar--}}

                                            @if($fixed->status == "enabled")
                                                <form action="{{ route('fixed.destroy', $fixed->id) }}" method="POST">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <button class="btn btn-danger btn_delete">
                                                        <i class="fa fa-times"></i>
                                                    </button>
                                                    </p>
                                                </form>
                                            @endif

                                            {{-- Cambiar forma de la mesa--}}
                                            <div class="btn_forms">
                                                <a class="btn_circle" table-id="{{$fixed->id}}">
                                                    <i class="far fa-circle"></i>
                                                </a>
                                                <a class="btn_standard" table-id="{{$fixed->id}}">
                                                    <i class="far fa-square"></i>
                                                </a>
                                            </div>

                                            @php $flag = false; @endphp
                                        @endif
                                    @endforeach

                                    @if($flag == true)
                                        <input type="text" class="btn btn_table_none" id="{{$x}}{{$y}}" position-x="{{$x}}" position-y="{{$y}}" style="display: none;" value="+">

                                        <form action="" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button class="btn btn-danger btn_delete">
                                                <i class="fa fa-times"></i>
                                            </button>
                                            </p>
                                        </form>
                                        
                                        <div class="btn_forms" style="display: none;">
                                            <a class="btn_circle" table-id="">
                                                <i class="far fa-circle"></i>
                                            </a>
                                            <a class="btn_standard" table-id="">
                                                <i class="far fa-square"></i>
                                            </a>
                                        </div>
                                    @endif
                                </td>
                            @endfor
                        </tr>
                    @endfor
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script>
        
        $(document).ready(function()
        {
            var width = $('.tables').width() / 10;
            $('.tables td').width(width);
            $('.tables td').height(width);

            $('.btn_circle').click(function()
            {
                $.get("{{url('fixed/form')}}"+"/"+$(this).attr('table-id')+"/circle");
                $(this).parent().siblings().addClass('circle');
                $(this).parent().siblings().removeClass('standard');
            });

            $('.btn_standard').click(function()
            {
                $.get("{{url('fixed/form')}}"+"/"+$(this).attr('table-id')+"/standard");
                $(this).parent().siblings().removeClass('circle');
                $(this).parent().siblings().addClass('standard');
            });

            $('.btn_table_none').click(function()
            {
                $.ajax({
                    url: "{{url('fixed')}}",
                    headers:{'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')},
                    type: 'POST',
                    dataType: 'json',
                    data:{
                        'x'             : $(this).attr('position-x'),
                        'y'             : $(this).attr('position-y'),
                        'room_id'       : "{{$room->id}}",
                        'business_id'   : "{{$room->business_id}}",
                    },
                    success:function(res)
                    {
                        $("#"+res.position).val(res.name);
                        $("#"+res.position).siblings('.btn_forms').children('a').attr('table-id', res.id);
                        $("#"+res.position).siblings('.btn_forms').show();
                        $("#"+res.position).siblings('form').attr('action', "{{url('fixed')}}"+"/"+res.id);
                        $("#"+res.position).siblings('form').show();

                    }
                });

                $(this).removeClass('btn_table_none');
                $(this).addClass('btn_table standard enabled');
                $(this).show();
            });

            $('.btn_table').keyup(function()
            {
                $.ajax({
                    url: "{{url('fixed/edit/name')}}",
                    headers:{'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')},
                    type: 'POST',
                    dataType: 'json',
                    data:{
                        'id'            : $(this).attr('table-id'),
                        'name'          : $(this).val(),
                    },
                });
            });
        });

        

    </script>
@endsection

