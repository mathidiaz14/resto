@extends('layouts.tables')

@section('content')
<div class="container-fluid table-container">
    @include('helpers.alert')
    <div class="row body">
		<div class="col-xs-12 details_title text-center bg-primary">
        	<div class="col-xs-6 col-xs-offset-3">
                <p>@lang('message.table'): {{$table->fixed->name}} </p>
            </div>
            <div class="col-xs-3 text-right">
                <a href="{{url('room', $table->fixed->room->id)}}" class="btn btn-default">
                    <i class="fa fa-chevron-left"></i>
                    Volver
                </a>
            </div>
        </div>
        <div class="col-xs-12 col-md-4 details_products">
            <div class="col-xs-12">
            	<div class="table table-responsive" id="command_list">
            		
                    {{--Lista de comandas--}}
            		@include('table.parts.command_list')
            	</div>
            </div>
            <div class="col-xs-12">
            	<p>{{$table->observation}}</p>

                {{--Controles--}}
                
                <div class="col-xs-12 col-md-12 col-lg-6">
                    <a href="{{url('command/print', $table->id)}}" class="btn btn-block btn-primary" style="padding:20px;">
                        <i class="fa fa-print"></i>
                        @lang('message.send_command')
                    </a>
                    
                    <a href="{{url('table/history', $table->id)}}" class="btn btn-block btn-info">
                        <i class="fa fa-history"></i>
                        {{trans('message.history')}}
                    </a>
                    
                    @if($table->commands->where('status','enabled')->count() > 0)

                        @if ((Auth::user()->type == 'admin') or (Auth::user()->type == 'cashier'))
                            <a href="{{url('table/charge', $table->id)}}" class="btn btn-block btn-success">
                                <i class="fa fa-money-bill-alt"></i>
                                {{trans('message.charge_table')}}
                            </a>
                        @else
                            <a class="btn btn-block btn-danger" disabled>
                                <i class="fa fa-money-bill-alt"></i>
                                {{trans('message.charge_table')}}
                            </a>
                        @endif

                    @else

                        <form action="{{route('table.destroy', $table->id)}}" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="DELETE">  
                            <button class="btn btn-block btn-danger" style="margin-top:5px;">
                                <i class="fa fa-times"></i>
                                {{trans('message.close_table')}}
                            </button>
                        </form>
                    @endif
                    
                    <div class="col-xs-12">
                        <hr>
                        <p>Mozo: <b style="color:{{$table->user->color}}">{{$table->user->name}}</b></p>
                        <p>Hora de inicio: <b>{{$table->created_at->format('d/m/Y H:i')}}</b></p>
                        <p>Comenzales: <b>{{$table->diners}}</b></p>

                        @if($table->observation != null)
                            <p>Observación: <b>{{$table->observation}}</b></p>
                        @endif

                        @if($table->client_id != 0)
                            <p>Cliente: <b>{{$table->client->name}}</b> <small>[{{$table->client->phone}}]</small></p>
                            <a href="{{url('client', $table->client->id)}}" class="btn btn-info btn-block" style="margin-bottom: 2em;">
                                <i class="fa fa-eye"></i>
                                Ver historial del cliente
                            </a>
                        @endif
                    </div>
				</div>
				<div class="col-xs-12 col-md-12 col-lg-6 btn-calc">
				    <div class="row">
				        <div class="col-xs-12">
				        	<input type="text" id="text_control" name="text_control" value="0" class="form-control text-center" style="font-size: 2em;" readonly>
				        </div>
				    </div>
                    <br>
				    <div class="row">
				        <div class="col-xs-4 p-1"><a class="btn btn-success btn-block btn_calc" value="7"><b>7</b></a></div>
				        <div class="col-xs-4 p-1"><a class="btn btn-success btn-block btn_calc" value="8"><b>8</b></a></div>
				        <div class="col-xs-4 p-1"><a class="btn btn-success btn-block btn_calc" value="9"><b>9</b></a></div>
				    </div>
				    <div class="row">
				        <div class="col-xs-4 p-1"><a class="btn btn-success btn-block btn_calc" value="4"><b>4</b></a></div>
				        <div class="col-xs-4 p-1"><a class="btn btn-success btn-block btn_calc" value="5"><b>5</b></a></div>
				        <div class="col-xs-4 p-1"><a class="btn btn-success btn-block btn_calc" value="6"><b>6</b></a></div>
				    </div>
				    <div class="row">
				        <div class="col-xs-4 p-1"><a class="btn btn-success btn-block btn_calc" value="1"><b>1</b></a></div>
				        <div class="col-xs-4 p-1"><a class="btn btn-success btn-block btn_calc" value="2"><b>2</b></a></div>
				        <div class="col-xs-4 p-1"><a class="btn btn-success btn-block btn_calc" value="3"><b>3</b></a></div>
				    </div>
				    <div class="row">
				        <div class="col-xs-4 p-1"><a class="btn btn-success btn-block btn_calc" value="0">0</a></div>
				        <div class="col-xs-8 p-1"><a class="btn btn-warning btn-block btn_back"><i class="fa fa-arrow-circle-left"></i></a></div>
				    </div>
				</div>
            </div>
        </div>
        <div class="col-xs-12 col-md-8 products">
            <div class="row">
                <div class="col-xs-12">
                	@foreach(Auth::user()->business->categories as $category)
    					<a class="btn btn-warning btn_category" category="category-{{$category->id}}">{{$category->name}}</a>
                	@endforeach

                    @if($table->client_id != 0)
                        <a class="btn btn-primary btn_category" category="favourite">
                            <i class="fa fa-star"></i>
                            Favoritos del cliente
                        </a>
                    @endif
                </div>
            </div>
			<div class="row">
				@foreach(Auth::user()->business->categories as $category)
					<div class="col-xs-12 category_content" id="category-{{$category->id}}" style="display:none;">
						@foreach($category->products as $product)
							<a class="btn btn-info btn_products" product="{{$product->id}}">
								{{$product->name}}
                                <span class="price">${{$product->price}}</span>
							</a>
						@endforeach		
					</div>
				@endforeach
                
                @if($table->client_id != 0)
                    <div class="col-xs-12 category_content" id="favourite" style="display:none;">
                        @foreach($table->client->commands->groupBy('product_id')->take(5) as $command)
                            <a class="btn btn-primary btn_products" product="{{$command->first()->product->id}}">
                                {{$command->first()->product->name}}
                                <span class="price">${{$command->first()->product->price}}</span>
                            </a>
                        @endforeach     
                    </div>
                @endif
			</div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>

    <script>
    	$(document).ready(function()
    	{
    		$('.btn_category').click(function()
    		{
                $('.category_content').hide();
    			$('#'+$(this).attr('category')).fadeIn();
    		});

    		$('.btn_calc').click(function()
    		{
                var num = $(this).attr('value');
                var text = $('#text_control').val();
                
                if(text == "0")
                {
                	$('#text_control').val(num);
                }else
                {
                	$('#text_control').val(text + num);
                }
            });

            $('.btn_back').click(function()
            {
            	$('#text_control').val("0");
            });

            $('.btn_products').click(function()
            {
                $.ajax(
                {
                    url: "{{url('command')}}",
                    headers:{'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')},
                    type: 'POST',
                    dataType: 'json',
                    data:{
                        'product'  : $(this).attr('product'),
                        'quantity' : $('#text_control').val(),
                        'table' : '{{$table->id}}',
                    },
                    success:function(res)
                    {
                        $('#command_list').load("{{url('show/commands', $fixed->id)}}");
                        $('#text_control').val("0");
                    }
                });
            });
    	});
    </script>
@endsection

