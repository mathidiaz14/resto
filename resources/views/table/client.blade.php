@extends('layouts.tables')

@section('content')
<div class="container table-container">
    @include('helpers.alert')
    <div class="row body">
		<div class="col-xs-12 details_title text-center bg-primary">
        	<div class="col-xs-6 col-xs-offset-3">
                <p>@lang('message.client'): {{$client->name}}</p>     
            </div>
            <div class="col-xs-3 text-right">
                <a href="{{URL::Previous()}}" class="btn btn-default">
                    <i class="fa fa-chevron-left"></i>
                    Volver
                </a>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="col-xs-12">
                <h3>Pedidos Favoritos</h3>
                <hr>
                <div class="table table-responsive">
                    <table class="table table-striped">
                        <tr>
                            <th>Nombre</th>
                            <th>Cantidad</th>
                        </tr>
                        @php
                            $commands = $client->commands->where('status', 'enabled')->groupBy('product_id');
                        @endphp
                        
                        @foreach($commands as $command)
                            <tr>
                                <td>
                                    {{$command->first()->product->name}}
                                </td>
                                <td>
                                    {{$command->count()}}
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="col-xs-12">
                <h3>Ultimas visitas</h3>
                <hr>
                <div class="table table-responsive">
                    <table class="table table-striped">
                        <tr>
                            <th>Mesa</th>
                            <th>Gasto</th>
                            <th>Fecha</th>
                        </tr>
                        @foreach($client->tables->take(5) as $table)
                            <tr>
                                <td>
                                    Mesa: {{$table->fixed->name}}
                                </td>
                                <td>
                                    {{$table->created_at->format('d/m/Y H:i')}}
                                </td>
                                <td>
                                    ${{($table->commands->sum('price') - $table->discounts->sum('quantity'))}}
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
   
@endsection

