@extends('layouts.tables')

@section('content')
<div class="container-fluid">
    @include('helpers.alert')
    <div class="row body">
		<div class="col-xs-12 details_title text-center bg-primary">
        	<p>@lang('message.table'): {{$table->fixed->name}}</p>
        </div>
        <div class="charge">
        	<div class="col-xs-12 col-md-8">
		    	@include('table.parts.charge_command_list')
	        </div>
	        <div class="col-xs-12 col-md-4 btn_charge">
	        	<div class="row">
                    <div class="col-xs-12 col-md-6">
                        <a href="{{url('table', $table->fixed->id)}}" class="btn btn-primary btn-block">
                            <i class="fa fa-home"></i>
                            @lang('message.go_orders')
                        </a>
                    </div>
                    
                    <div class="col-xs-12 col-md-6">
                        <a href="{{url('table/print', $table->id)}}" class="btn btn-warning btn-block">
                            <i class="fa fa-print"></i>
                            @lang('message.print_ticket')
                        </a>    
                    </div>
                
                    <div class="col-xs-12 col-md-6">
                        <form action="{{url('discount')}}" method="post">
                            @csrf
                            <input type="hidden" name="quantity" value="0" class="text_control">
                            <input type="hidden" name="table" value="{{$table->id}}">
                            <input type="hidden" name="type" value="percentage">
                            <button class="btn btn-primary btn-block">
                                <i class="fa fa-percentage"></i>
                                @lang('message.discount_percentage')
                            </button>
                        </form>
                    </div>
                
                    <div class="col-xs-12 col-md-6">
                        <form action="{{url('discount')}}" method="post">
                            @csrf
                            <input type="hidden" name="quantity" value="0" class="text_control">
                            <input type="hidden" name="table" value="{{$table->id}}">
                            <input type="hidden" name="type" value="quantity">
                            <button class="btn btn-primary btn-block">
                                <i class="fa fa-dollar-sign"></i>
                                @lang('message.discount_quantity')
                            </button>
                        </form>
                    </div>
                        

                    @if($table->commands->where('status', 'enabled')->count() == 0)
                        <div class="col-xs-12">
                            <a href="{{url('table/close', $table->id)}}" class="btn btn-danger btn-block">
                                <i class="fa fa-times"></i>
                                @lang('message.close_table')
                            </a>
                        </div>
                    @endif
                </div>
                <hr>
                <div class="row">
                    <form action="{{url('table/charge', $table->id)}}" method="POST">
                        @csrf
                        <input type="hidden" name="text_control" value="0" class="text_control">

                        @foreach($commands_all as $command)
                            <input type="hidden" name="check_{{$command->id}}" id="check_{{$command->id}}" value="no">
                        @endforeach

                        <div class="col-xs-12 col-md-6">
                            <select name="wayOfPay" class="form-control" style="height: 55px;">
                                <option value="cash">@lang('message.cash')</option>
                                <option value="debit">@lang('message.debit')</option>
                                <option value="visa">@lang('message.visa')</option>
                                <option value="master">@lang('message.master')</option>
                                <option value="master">@lang('message.other')</option>
                            </select>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <button class="btn btn-success btn-block">
                                <i class="fa fa-money-bill-alt"></i>
                                @lang('message.pay')
                            </button>
                        </div>
                    </form>      
                </div>
                <hr>
    	        <div class="row">
                    <div class="col-xs-12">    
                        <div class="row">
                            <div class="col-xs-12">
                                <input type="text" name="text_control" value="0" class="form-control text-center text_control">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6"><a class="btn btn-primary btn-block btn_calc_options" value="C"><b>C</b></a></div>
                            <div class="col-xs-3"><a class="btn btn-primary btn-block btn_calc_options" value="back"><b><i class="fa fa-arrow-circle-left"></i></b></a></div>
                            <div class="col-xs-3"><a class="btn btn-primary btn-block btn_calc" value="/"><b>/</b></a></div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3"><a class="btn btn-info btn-block btn_calc" value="7"><b>7</b></a></div>
                            <div class="col-xs-3"><a class="btn btn-info btn-block btn_calc" value="8"><b>8</b></a></div>
                            <div class="col-xs-3"><a class="btn btn-info btn-block btn_calc" value="9"><b>9</b></a></div>
                            <div class="col-xs-3"><a class="btn btn-primary btn-block btn_calc" value="*"><b>*</b></a></div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3"><a class="btn btn-info btn-block btn_calc" value="4"><b>4</b></a></div>
                            <div class="col-xs-3"><a class="btn btn-info btn-block btn_calc" value="5"><b>5</b></a></div>
                            <div class="col-xs-3"><a class="btn btn-info btn-block btn_calc" value="6"><b>6</b></a></div>
                            <div class="col-xs-3"><a class="btn btn-primary btn-block btn_calc" value="-"><b>-</b></a></div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3"><a class="btn btn-info btn-block btn_calc" value="1"><b>1</b></a></div>
                            <div class="col-xs-3"><a class="btn btn-info btn-block btn_calc" value="2"><b>2</b></a></div>
                            <div class="col-xs-3"><a class="btn btn-info btn-block btn_calc" value="3"><b>3</b></a></div>
                            <div class="col-xs-3"><a class="btn btn-primary btn-block btn_calc" value="+"><b>+</b></a></div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6"><a class="btn btn-info btn-block btn_calc" value="0"><b>0</b></a></div>
                            <div class="col-xs-3"><a class="btn btn-warning btn-block btn_calc" value="."><b>.</b></a></div>
                            <div class="col-xs-3"><a class="btn btn-primary btn-block btn_calc_options" value="="><b>=</b></a></div>
                        </div>
                    </div> 
                </div>
            </div>
	    </div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>

    <script>
    	$(document).ready(function()
    	{

            $('#total').click(function()
            {
                $('.text_control').val($('#total').html());
            });

    		$('.btn_calc').click(function()
    		{
                var num = $(this).attr('value');
                var text = $('.text_control').val();
                
                if(text == "0")
                	$('.text_control').val(num);
                else
                    $('.text_control').val(text + num);
            });

            $('.btn_calc_options').click(function()
            {
            	if( $(this).attr('value') == "C")
            	{
            		$('.text_control').val("0");
            	
            	}else if( $(this).attr('value') == "back")
            	{
            		var str = $('.text_control').val();

            		if(str.length == 1)
            			$('.text_control').val("0");
            		else
            			$('.text_control').val(str.substr(0, str.length-1));
            		
            	
            	}else if( $(this).attr('value') == "=")
            	{
            		$('.text_control').val(eval($('.text_control').val()));
            	}
            });

            $('.check_command').click(function()
            {
            	if($('#check_' + $(this).attr('attr-id')).val() == "yes")
                {
                    $('#check_' + $(this).attr('attr-id')).val("no");
                    $(this).removeClass('btn-success');
                    $(this).addClass('btn-primary');
                    
                }else
                {
                    $('#check_' + $(this).attr('attr-id')).val('yes');
                    $(this).removeClass('btn-primary');
                    $(this).addClass('btn-success');
                }
            });
    	});
    </script>
@endsection

