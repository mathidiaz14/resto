@extends('layouts.tables')

@section('content')
    <div class="container-fluid table-container mt-3">
        <div class="row">
            
            @include('table.parts.clock')
            
            <div class="col-12 col-md-9">
                
                {{-- Navegacion de las mesas--}}
                <ul class="nav nav-tabs" id="show_tabs">
                    @include('table.parts.tabs')

                    @if(Auth::user()->type == "admin")
                        {{--Boton de editar sala--}}
                        <li>
                            <a href="{{route('room.edit', $room->id)}}" class="nav-link">
                                <i class="fa fa-edit"></i>
                            </a>
                        </li>
                    @endif
                </ul>
                {{-- Fin navegacion de las mesas--}}

                
            </div>  
        </div>
        <div class="row body">
            <div class="col-xs-12 tables hidden-xs">
                <div id="show_tables">
                    @include('table.parts.show_tables')
                </div>
            </div>

            <div class="col-xs-12 tables visible-xs">
                <div id="show_tables">
                    @include('table.parts.show_tables_mobile')
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <script>
        $(document).ready(function()
        {
             reload();

            function reload()
            {
                $.get("{{url('consult/tables')}}", function(res)
                {
                    if ("{{Auth::user()->business->fixeds->where('status','occupied')->count()}}" != res )
                    {       
                        $('#show_tables').load("{{url('show/tables', $room->id)}}");
                        $('#show_tabs').load("{{url('show/tabs', $room->id)}}");
                    }

                });

                setTimeout(reload, 5000);
            }
        });
    </script>
@endsection