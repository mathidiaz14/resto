@extends('layouts.tables')

@section('content')
<div class="container-fluid table-container">
    @include('helpers.alert')
    <div class="row body">
		<div class="col-xs-12 details_title text-center bg-primary">
        	<div class="col-xs-6 col-xs-offset-3">
                <p>Mi cuenta</p>     
            </div>
            <div class="col-xs-3 text-right">
                <a href="{{url('home')}}" class="btn btn-default">
                    <i class="fa fa-chevron-left"></i>
                    Volver
                </a>
            </div>
        </div>
        <div class="col-xs-12">
            
        </div>
    </div>
</div>
@endsection

@section('scripts')
   
@endsection

