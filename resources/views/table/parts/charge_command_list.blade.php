<div class="table table-responsive">
	{{--Lista de comandas--}}
	<table class="table table_products table-striped text-center table_border">
		<tr>
			<th class="text-center">@lang('message.product')</th>
            <th class="text-center">@lang('message.quantity')</th>
			<th class="text-center">@lang('message.price')</th>
			<th class="text-center">@lang('message.options')</th>
		</tr>
		@foreach($commands_list as $commands)
                
            @if($commands->count() == 1)
                <tr @if($commands->first()->status == "disabled") style="background: #bbdeacba;"@endif >
            @else
                @php 
                    $flag = true;
                    foreach($commands as $command)
                    {  
                        if($command->status == "enabled")
                            $flag = false;
                    }
                @endphp
                
                <tr @if($flag) style="background: #bbdeacba;" @endif>
            @endif

				<td>{{$commands->first()->product->name}}</td>
                <td>
                    x {{$commands->where('status', '!=', 'deleted')->count()}} 
                    <small>[{{$commands->where('status', 'enabled')->count()}} Pendientes]</small>
                </td>
				<td>
                    $ {{$commands->first()->price * $commands->where('status', '!=', 'deleted')->count()}} 
                    <small>[$ {{$commands->first()->price * $commands->where('status', 'enabled')->count()}} Pendiente]            </small>
                </td>
				<td>
                    {{-- botones para seleccionar los productos --}}

                    @if($commands->count() > 1)                      
                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#commands_open_{{$commands->first()->id}}" style="width: 80px;">
                            <i class="fa fa-external-link-alt"></i>
                        </button>
    
                        <!-- Modal -->
                        <div class="modal fade" id="commands_open_{{$commands->first()->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <div class="row">
                                            <div class="col-xs-8 col-xs-offset-2">
                                                <p>Productos detallados</p>
                                            </div>
                                            <div class="col-xs-2 text-right">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="modal-body text-center">
                                        <div class="table table-responsive">
                                            <table class="table table_products table-striped text-center">
                                                <tr>
                                                    <th class="text-center">@lang('message.product')</th>
                                                    <th class="text-center">@lang('message.price')</th>
                                                    <th class="text-center"></th>
                                                </tr>
                                                @foreach($commands as $command)
                                                    @if($command->status == "disabled")
                                                        <tr style="background: #bbdeacba;">
                                                            <td>{{$command->product->name}}</td>
                                                            <td>$ {{$command->price}}</td>
                                                            <td>
                                                                <a class="btn btn-primary" disabled>
                                                                    <i class="fa fa-check"></i>
                                                                </a> 
                                                                <a class="btn btn-danger" disabled>
                                                                    <i class="fa fa-times"></i>
                                                                </a> 
                                                            </td>
                                                        </tr>
                                                    @else
                                                        <tr>
                                                            <td>{{$command->product->name}}</td>
                                                            <td>$ {{$command->price}}</td>
                                                            <td>
                                                                <a class="btn check_command btn-primary" attr-id="{{$command->id}}">
                                                                    <i class="fa fa-check"></i>
                                                                </a>

                                                                @if($command->status == "disabled")
                                                                    <a href="" class="btn btn-danger disabled" disabled>
                                                                        <i class="fa fa-times"></i>
                                                                    </a>
                                                                @else             
                                                                    <!-- Button trigger modal -->
                                                                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#commands_delete_{{$commands->first()->id}}">
                                                                        <i class="fa fa-times"></i>
                                                                    </button>
                                                
                                                                    <!-- Modal -->
                                                                    <div class="modal fade" id="commands_delete_{{$commands->first()->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                                        <div class="modal-dialog" role="document">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                                </div>
                                                                                <div class="modal-body text-center">
                                                                                    <h3>@lang('message.delete_question')</h3>
                                                                                </div>
                                                                                <div class="modal-footer">
                                                                                    <div class="col-xs-6">
                                                                                        <button type="button" class="btn btn-default btn-block" data-dismiss="modal">@lang('message.no')</button>
                                                                                    </div>
                                                                                    <div class="col-xs-6">
                                                                                        <form action="{{ route('command.destroy', $commands->first()->id) }}" method="POST">
                                                                                            {{ csrf_field() }}
                                                                                            <input type="hidden" name="_method" value="DELETE">
                                                                                            
                                                                                            <button type="submit" class="btn btn-danger btn-block">   
                                                                                                @lang('message.yes')
                                                                                            </button>
                                                                                        </form>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>   
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @else
                        @if($commands->first()->status == "disabled")                                        
                            <a class="btn btn-primary disabled" disabled>
                                <i class="fa fa-check"></i>
                            </a>
                        @else
                            <a class="btn check_command btn-primary" attr-id="{{$commands->first()->id}}">
                                <i class="fa fa-check"></i>
                            </a>
                        @endif
                    @endif
                    {{-- Botones de eliminar --}}

                    @if($commands->count() == 1)          
                        @if($commands->first()->status == "disabled")
                            <a href="" class="btn btn-danger disabled" disabled>
                                <i class="fa fa-times"></i>
                            </a>
                        @else             
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#commands_delete_{{$commands->first()->id}}">
                                <i class="fa fa-times"></i>
                            </button>
        
                            <!-- Modal -->
                            <div class="modal fade" id="commands_delete_{{$commands->first()->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        </div>
                                        <div class="modal-body text-center">
                                            <h3>@lang('message.delete_question')</h3>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="col-xs-6">
                                                <button type="button" class="btn btn-default btn-block" data-dismiss="modal">@lang('message.no')</button>
                                            </div>
                                            <div class="col-xs-6">
                                                <form action="{{ route('command.destroy', $commands->first()->id) }}" method="POST">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    
                                                    <button type="submit" class="btn btn-danger btn-block">   
                                                        @lang('message.yes')
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>   
                        @endif
                    @endif
                </td>
			</tr>
		@endforeach
        
        @php 
            $discounts = 0;
        @endphp

        @foreach($table->discounts->where('status', 'enabled') as $discount)
            @php
                if($discount->type == "quantity") 
                    $discounts += $discount->quantity;
                else 
                    $discounts += ($table->commands->where('status', 'enabled')->sum('price') * $discount->quantity) / 100;
            @endphp
            
            @if($loop->first)
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            @endif

            <tr>
                <td><b style="color:green;">@lang('message.discount')</b></td>
                <td>
                    @if($discount->type != "quantity") 
                        <b style="color:green;">
                            % {{$discount->quantity}} 
                        </b>
                    @endif 
                </td>
                <td>
                    <b style="color:green;">
                        - $
                        @if($discount->type == "quantity") 
                            {{$discount->quantity}}
                        @else 
                            {{($table->commands->where('status', 'enabled')->sum('price') * $discount->quantity) / 100}}
                        @endif
                    </b>
                </td>
                <td>
                    @if($table->commands->where('status', 'enabled')->count() == 0)
                        <button type="button" class="btn btn-danger disabled" data-toggle="modal" style="width: 80px;" disabled="">
                            <i class="fa fa-times"></i>
                        </button>
                    @else
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#discount_delete_{{$discount->id}}" style="width: 80px;">
                            <i class="fa fa-times"></i>
                        </button>
    
                        <!-- Modal -->
                        <div class="modal fade" id="discount_delete_{{$discount->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    </div>
                                    <div class="modal-body text-center">
                                        <h3>@lang('message.delete_question')</h3>
                                    </div>
                                    <div class="modal-footer">
                                        <div class="col-xs-6">
                                            <button type="button" class="btn btn-default btn-block" data-dismiss="modal">@lang('message.no')</button>
                                        </div>
                                        <div class="col-xs-6">
                                            <form action="{{ route('discount.destroy', $discount->id) }}" method="POST">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="_method" value="DELETE">
                                                
                                                <button type="submit" class="btn btn-danger btn-block">   
                                                    @lang('message.yes')
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    @endif
                </td>
            </tr>
        @endforeach
        <tfoot>
            <td></td>
            <td></td>
            <td>
                <h3>
                    @lang('message.total'): <b>$</b>
                    
                    @if((($table->commands->where('status', 'enabled')->sum('price')) - $discounts) < 0)
                        <b id="total">0</b>
                    @else
                        <b id="total">{{($table->commands->where('status', 'enabled')->sum('price')) - $discounts}}</b>
                    @endif
                </h3>
            </td>
            <td></td>
        </tfoot>
	</table>
</div>