@php 
    $archie = Auth::user()->business->arches->where('status', 'enabled')->first();
@endphp
<div class="row tables">
    @for($y = 0; $y < 10; $y++)
        @for($x = 0; $x < 10; $x++)
            @foreach($room->fixeds->where('status', '!=', 'deleted') as $table)
                @if(($table->x == $x) and ($table->y == $y))
                    <div class="col-xs-3">
                        @if($archie != null) 
                            @if($table->status == "enabled")
                                <a class="btn btn_table_mobile {{$table->type}} {{$table->status}}" attr-name="{{$table->name}}">
                                    {{$table->name}}
                                </a>
                            @else
                                <a href="{{url('table', $table->id)}}" class="btn btn_table_mobile {{$table->type}} {{$table->status}}" style="border: 4px outset {{$table->tables->first()->user->color}};"  attr-name="{{$table->name}}">
                                    {{$table->name}}
                                </a>

                                @if($table->tables->first()->commands->where('status', 'pending')->count())
                                    <span class="commands_pending text-center">
                                        Hay comandas pendientes
                                    </span>
                                @endif

                            @endif
                        @else
                            <a class="btn btn_table_mobile {{$table->type}} disabled">
                                {{$table->name}}
                            </a>
                        @endif
                    </div>
                @endif
            @endforeach
        @endfor
    @endfor
</div>

<script>
    $(document).ready(function()
    {
        $('.btn_table_mobile').click(function()
        {
            if(!$(this).hasClass('occupied'))
            {
                if($(this).hasClass('table_selected'))
                {
                    $(this).removeClass('table_selected');
                    $(this).removeClass('table_selected_border');
                    
                    $('.details_table').fadeOut(200);
                    $('#table_name').html();
                    $('#table_id').val('');
                }else
                {
                    $('.table_selected').removeClass('table_selected_border');
                    $('.table_selected').removeClass('table_selected');
                    $('.details_table').fadeOut(200);

                    $(this).addClass('table_selected');
                    $(this).addClass('table_selected_border');
                    
                    $('.details_table').fadeIn(200);
                    $('#table_name').html('Mesa: '+$(this).attr('attr-name'));
                    $('#table_id').val($(this).attr('attr-id'));
                }
            }
        });

        $('.close_form').click(function()
        {
            $('.details_table').fadeOut(200);
            $('#table_name').html();
            $('.table_selected_border').removeClass('table_selected_border');
            $('.table_selected').removeClass('table_selected');
            $('.table_selected').removeClass('table_selected');
            $('#table_id').val('');
        });
    });
</script>