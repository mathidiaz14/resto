
@php
	$commands_list = $table->commands->where('status', 'pending')->groupBy('product_id');
@endphp

<table class="table table_products table-striped text-center">
	<tr>
		<th class="text-center">@lang('message.product')</th>
        <th class="text-center">@lang('message.quantity')</th>
		<th class="text-center">@lang('message.price')</th>
		<th class="text-center"></th>
	</tr>
	@foreach($commands_list as $commands)
		<tr>
			<td>
                {{$commands->first()->product->name}}
            </td>
            <td>x {{$commands->count()}}</td>
			<td>$ {{$commands->first()->price * $commands->count()}}</td>
            <td>
                
                {{-- Eliminar varios productos --}}
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#command_observation_{{$commands->first()->id}}">
                    <i class="fa fa-external-link-alt"></i>
                </button>

                <!-- Modal -->
                <div class="modal fade" id="command_observation_{{$commands->first()->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <div class="row">
                                    <div class="col-xs-6 text-left">
                                        <p>Detalles del proudcto</p>
                                    </div>
                                    <div class="col-xs-6">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-body text-left">
                                @foreach($commands as $command)
                                    @if($commands->count() > 1)
                                        # {{$loop->iteration}}
                                    @endif
                                    <div class="row">
                                        <div class="col-xs-12 col-md-8">
                                            <form action="{{url('command/observation')}}" method="POST">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="command" value="{{$command->id}}">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-xs-8">
                                                            <input type="text" name="observation" class="form-control" value="{{$command->observation}}" placeholder="Observación">
                                                        </div>
                                                        <div class="col-xs-4">
                                                            <button class="btn btn-info btn-block">
                                                                <i class="fa fa-save"></i>
                                                                @lang('message.save')
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>    
                                        </div>
                                        <div class="col-xs-12 col-md-4">
                                            <form action="{{ route('command.destroy', $command->id) }}" method="POST">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="_method" value="DELETE">
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-danger btn-block">   
                                                        <i class="fa fa-trash"></i>
                                                        @lang('message.delete')
                                                    </button>
                                                </div>
                                            </form>  
                                        </div>
                                    </div>
                                    <hr>
                                @endforeach

                                @if($commands->count() > 1)
                                    <form action="{{url('command/delete/all')}}" method="POST">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="table" value="{{$table->id}}">
                                        <input type="hidden" name="product" value="{{$commands->first()->product->id}}">
                                        <input type="hidden" name="quantity" value="{{$commands->count()}}">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-4 col-md-offset-8">
                                                <button type="submit" class="btn btn-danger btn-block">   
                                                    @lang('message.delete_all')
                                                </button>     
                                            </div>
                                        </div>
                                    </form>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>                           
            </td>
		</tr>
	@endforeach
    <tfoot>
        <td></td>
        <td></td>
        <td><b>@lang('message.total'):</b> <b>$</b><b id="total">{{$table->commands->where('status', 'pending')->sum('price')}}</b></td>
        <td></td>
    </tfoot>
</table>