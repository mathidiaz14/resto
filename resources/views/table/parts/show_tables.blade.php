@php 
    $archie = Auth::user()->business->arches->where('status', 'enabled')->first();
@endphp
<table>
    <tbody>
        @for($y = 0; $y < 10; $y++)
            <tr>
                @for($x = 0; $x < 10; $x++)
                    <td>
                        @foreach($room->fixeds->where('status', '!=', 'deleted') as $table)
                            @if(($table->x == $x) and ($table->y == $y))
                            	@if($archie != null) 
                            		@if($table->status == "enabled")

                                        @include('table.parts.modal_table', ['table' => $table])

                                    @else
                                    
                						<a href="{{url('table', $table->id)}}" class="btn btn_table {{$table->type}} {{$table->status}}" style="border: 4px outset {{$table->tables->first()->user->color}};"  attr-name="{{$table->name}}">
                							{{$table->name}}
                						</a>

                                        @if($table->tables->first()->commands->where('status', 'pending')->count())
                                            <span class="commands_pending">
                                                Comandas pendientes
                                            </span>
                                        @endif
                                    @endif
                            	@else
                                    <a class="btn btn_table {{$table->type}} disabled">
                                        {{$table->name}}
                                    </a>
                            	@endif
                            @endif
                        @endforeach
                    </td>
                @endfor
            </tr>
        @endfor
    </tbody>
</table>

<script>
    var width = $('.tables').width() / 10;
    $('.tables td').width(width);
    $('.tables td').height(width);

    $(window).resize(function()
    {
        var width = $('.tables').width() / 10;
        $('.tables td').width(width);
        $('.tables td').height(width);
    });

    $('td').click(function()
    {
        if(isEmpty($(this)))
        {
            $('.details_table').fadeOut(200);
            $('#table_name').html();
            $('.table_selected_border').removeClass('table_selected_border');
            $('.table_selected').removeClass('table_selected');
            $('.table_selected').removeClass('table_selected');
            $('#table_id').val('');
        }
    });

    function isEmpty( el ){
        return !$.trim(el.html())
    }
</script>