<div class="col-xs-12 hour text-right">
    {{-- Donde se muestra la hora --}}
</div>

<script>
	function clock()
    {
        var date    = new Date();
        var hours   = date.getHours();
        var minutes = (date.getMinutes() < 10 ? '0' : '') + date.getMinutes();
        var seconds = (date.getSeconds() < 10 ? '0' : '') + date.getSeconds();
        var month   = (date.getMonth() < 10 ? '0' : '') + date.getMonth();
        var day     = (date.getDate() < 10 ? '0' : '') + date.getDate();
        var year    = date.getFullYear();

        var fecha   = new Date(year, month, day);
        var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };

        $('.hour').html( "<p>"+fecha.toLocaleDateString("es-ES", options)+ "</p>" + " <span> " +hours + ":" + minutes + ":" + seconds + "</span>");
         
        setInterval("clock()",1000);    
    }

    clock();

</script>