@foreach(Auth::user()->business->rooms as $rooms)
    @if($room->id == $rooms->id)
        <li role="presentation" class="active">
            <a class="show_rooms nav-link active">
                {{$rooms->name}}
                @if ($rooms->fixeds->where('status',  'occupied')->count() != 0)
                    <span class="tables_occuped">{{$rooms->fixeds->where('status',  'occupied')->count()}}</span>
                @endif

                @php 
                    $count = 0;
                @endphp

                @foreach($rooms->fixeds as $fixed)
                    @foreach($fixed->tables as $table)
                        @foreach($table->commands as $command)
                            @if($command->status == "pending")
                                @php 
                                    $count ++;
                                @endphp
                            @endif
                        @endforeach
                    @endforeach
                @endforeach
                
                @if($count > 0)
                    <span class="tables_pending">{{$count}}</span>
                @endif
            </a>
        </li>
    @else
        <li role="presentation">
            <a class="show_rooms nav-link" href="{{url('room', $rooms->id)}}">
                {{$rooms->name}}
                @if ($rooms->fixeds->where('status', 'occupied')->count() != 0)
                    <span class="tables_occuped">{{$rooms->fixeds->where('status', 'occupied')->count()}}</span>
                @endif

                @php 
                    $count = 0;
                @endphp

                @foreach($rooms->fixeds as $fixed)
                    @foreach($fixed->tables as $table)
                        @foreach($table->commands as $command)
                            @if($command->status == "pending")
                                @php 
                                    $count ++;
                                @endphp
                            @endif
                        @endforeach
                    @endforeach
                @endforeach
                
                @if($count > 0)
                    <span class="tables_pending">{{$count}}</span>
                @endif
            </a>
        </li>
    @endif
@endforeach