<button type="button" class="btn btn_table {{$table->type}} {{$table->status}}" data-toggle="modal" data-target="#modalTable_{{$table->id}}">
    {{$table->name}}
</button>

<div class="modal fade" id="modalTable_{{$table->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content text-left">
            <div class="modal-header">
                <div class="col-sm-9">
                    <h4 class="modal-title" id="exampleModalLabel">
                        Mesa {{$table->name}}
                    </h4>
                </div>
                <div class="col-sm-3">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <div class="modal-body">
                <form action="{{url('table')}}" method="POST" class="form-horizontal">
                    @csrf
                    <input type="hidden" name="table" value="{{$table->id}}">
                    <div class="row">
                        <div class="col-sm-12">
                            <p>Cuantos comensales?</p>
                        </div>
                        <div class="col-sm-8">
                            <input type="number" class="form-control virtual" name="diners" id="diners_{{$table->id}}" value="1" required >
                        </div>
                        <div class="col-sm-2">
                            <a class="btn btn-info btn-block btn_up_{{$table->id}}" data-table="">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                        <div class="col-sm-2">
                            <a class="btn btn-info btn-block btn_down_{{$table->id}}" data-table="">
                                <i class="fa fa-chevron-down"></i>
                            </a>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-12">
                            <p>Quién es el cliente?</p>
                        </div>
                        <div class="col-sm-8">
                            <select name="client" class="form-control client_list">
                                <option value="0">@lang('message.none_client')</option>
                                @foreach(Auth::user()->business->clients as $client)
                                    <option value="{{$client->id}}">{{$client->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <!-- Button trigger modal -->
                            <a class="btn btn-primary btn-block modal_add_client_{{$table->id}}">
                                Agregar cliente
                            </a>
                        </div>
                        <div class="col-sm-12 form_client_{{$table->id}}" style="display: none;">
                            <hr>
                            <div class="col-xs-12 col-md-6">
                                <label for="" class="form-label">
                                    Nombre
                                </label>
                                <input type="text" class="form-control virtual" name="client_name" id="client_name_{{$table->id}}">
                            </div>

                            <div class="col-xs-12 col-md-6">
                                <label for="" class="form-label">
                                    Email
                                </label>
                                <input type="text" class="form-control virtual" name="client_email" id="client_email_{{$table->id}}">
                            </div>

                            <div class="col-xs-12 col-md-6">
                                <label for="" class="form-label">
                                    Telefono
                                </label>
                                <input type="text" class="form-control virtual" name="client_phone" id="client_phone_{{$table->id}}">
                            </div>

                            <div class="col-xs-12 col-md-6">
                                <br>
                                <a class="btn btn-info btn-block btn_save_client_{{$table->id}}">
                                    <i class="fa fa-save"></i>
                                    Guardar
                                </a>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-12">
                            <p>Quien es el mozo?</p>
                        </div>
                        <div class="col-sm-12">
                            <div class="input-group">
                                <div class="btn-group" data-toggle="buttons">
                                    @foreach (Auth::user()->business->users as $waiter)
                                        <label class="btn btn-waiter @if($waiter->id == Auth::user()->id) active @endif" style="background:{{$waiter->color}} ;">
                                            <input type="radio" name="waiter" value="{{$waiter->id}}" autocomplete="off" @if($waiter->id == Auth::user()->id) checked @endif> {{$waiter->name}}
                                        </label>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-12">
                            <p>Observación de la mesa</p>
                        </div>   
                        <div class="col-sm-12">
                            <input type="text" name="observation" class="form-control virtual">
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-12 text-right">
                            <button class="btn btn-info">
                                <i class="fa fa-save"></i>
                                @lang('message.save')
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>

    $('.modal_add_client_{{$table->id}}').click(function()
    {
        $('.form_client_{{$table->id}}').fadeIn();
        $('#client_name_{{$table->id}}').focus();
    });

    $('.btn_up_{{$table->id}}').click(function()
    {
        if($('#diners_{{$table->id}}').val() == "")
            $('#diners_{{$table->id}}').val(1);
        else
            $('#diners_{{$table->id}}').val(parseFloat($('#diners_{{$table->id}}').val()) + 1);
    });

    $('.btn_down_{{$table->id}}').click(function()
    {
        if($('#diners_{{$table->id}}').val() == "")
        {
            $('#diners_{{$table->id}}').val(1);
        }else
        {
            var result = parseFloat($('#diners_{{$table->id}}').val()) + -1;
            
            if(result < 1)
            {
                $('#diners_{{$table->id}}').val(1);
            }else
            {
                $('#diners_{{$table->id}}').val(result);
            }
        }
    });

    $('.btn_save_client_{{$table->id}}').click(function()
    {
        $.ajax({
            url: "{{url('client/save')}}",
            headers:{'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')},
            type: 'POST',
            dataType: 'json',
            data:{
                'name'          : $('#client_name_{{$table->id}}').val(),
                'address'       : $('#client_email_{{$table->id}}').val(),
                'phone'         : $('#client_phone_{{$table->id}}').val(),
            },
            success:function(res)
            {   
                $('.client_list').append("<option value="+res.id+" selected>"+res.name+"</option>");

                $('#client_name_{{$table->id}}').val('');
                $('#client_email_{{$table->id}}').val('');
                $('#client_phone_{{$table->id}}').val('');
                $('.form_client_{{$table->id}}').fadeOut();
            }
        });
    });
</script>  