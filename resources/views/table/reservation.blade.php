@extends('layouts.tables')

@section('content')
<div class="container-fluid table-container">
    <div class="row body">
		<div class="col-xs-12 details_title text-center bg-primary">
        	<div class="col-xs-10 col-xs-offset-1">
        		<p>@lang('message.reservation')</p>
        	</div>
        	<div class="col-xs-1">
        		<button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal">
				 	<i class="fa fa-plus"></i>
				 	Agregar
				</button>

				<!-- Modal -->
				<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body">
				        ...
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				        <button type="button" class="btn btn-primary">Save changes</button>
				      </div>
				    </div>
				  </div>
				</div>
        	</div>
        </div>
        <div class="col-xs-12 col-md-10 col-md-offset-1 text-center">
        	<div class="col-xs-12 text-left">
        		{{$reservations}}
        	</div>
        </div>
    </div>
</div>
@endsection




