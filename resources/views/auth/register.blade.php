@extends('layouts.auth')

@section('css')
    <style>
        .error
        {
            border-color:red;
            background: #eaafaf;
        }

        .success
        {
            border-color:green;
            background: #c9f7cd;
        }

        .login100-form
        {
            padding-top: 100px;
        }

    </style>
@endsection

@section('content')
<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <form method="POST" action="{{ route('register') }}" class="login100-form validate-form">
                @csrf
                <span class="login100-form-title p-b-34 p-t-27">
                    Registrarse
                </span>
                
                <span style="color:red;" role="alert">
                    <strong id="error"></strong>
                </span>

                <div class="wrap-input100 validate-input" id="border_name" data-validate = "Enter username">
                    <input class="input100" type="text" name="name" id="name" placeholder="Nombre" required>
                    <span class="focus-input100" data-placeholder="&#xf207;"></span>
                </div>

                <div class="wrap-input100 validate-input" data-validate = "Enter username">
                    <input class="input100" type="email" name="email" placeholder="Email" required>
                    <span class="focus-input100" data-placeholder="&#xf1f7;"></span>
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="wrap-input100 validate-input" data-validate="Enter password">
                    <input class="input100" type="password" name="password" placeholder="Contraseña" required>
                    <span class="focus-input100" data-placeholder="&#xf191;"></span>
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="wrap-input100 validate-input" data-validate="Enter password">
                    <input class="input100" type="password" name="password_confirmation" placeholder="Confirmar contraseña" required>
                    <span class="focus-input100" data-placeholder="&#xf191;"></span>
                    @if ($errors->has('password_confirmation'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="container-login100-form-btn">
                    <button id="btn_login" class="login100-form-btn" disabled>
                        Entrar
                    </button>
                </div>
            </form>

            <div class="login100-more" style="background-image: url('auth/images/bg-01.jpg');">
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        $('#name').keypress(function(tecla) {
            if(tecla.charCode == 32)
            { 
                $('#name').val($('#name').val() + '_'); 
                return false;
            }

        });

        $('#name').keyup(function()
        {
            $.get("{{url('business/check')}}/"+$('#name').val(), function(result)
            {
                if(result == "no")
                {
                    $('#border_name').removeClass('success');
                    $('#border_name').addClass('error');
                    $('#error').show();
                    $('#error').html('La empresa ya existe, pruebe con otro nombre');
                    $('#btn_login').prop('disabled', true);
                }else
                {
                    $('#border_name').removeClass('error');
                    $('#border_name').addClass('success');
                    $('#error').hide();
                    $('#btn_login').prop('disabled', false);
                }
            });

            $('#name').val($('#name').val().toLowerCase());
        });
    </script>
@endsection
