@extends('layouts.auth')

@section('content')
<form class="login100-form validate-form" method="POST" action="{{ route('login') }}">
    @csrf
    <span class="login100-form-title p-b-43">
        Iniciar Sesión
        <br>
    </span>
    
    @if ($errors->has('email'))
        <small style="color:red;">
            {{ $errors->first('email') }}
        </small>
    @endif
    
    @include('helpers.alert')
    
    <div class="wrap-input100 ">
        <input id="email" type="text" class="input100" name="email" value="{{ old('email') }}" required autofocus>

        <span class="focus-input100"></span>
            <span class="label-input100">Email</span>
        
    </div>
    
    
    <div class="wrap-input100 validate-input" data-validate="Password is required">
        <input id="password" type="password" class="input100" name="password" required>

        <span class="focus-input100"></span>
        <span class="label-input100">Password</span>
    </div>
    
        @if ($errors->has('password'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif

    <div class="flex-sb-m w-full p-t-3 p-b-32">
        <div class="contact100-form-checkbox">
            <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
            <label class="label-checkbox100" for="ckb1">
                Recordarme
            </label>
        </div>

        <div>
            <a href="{{ route('password.request') }}" class="txt1">
                Olvide mi contraseña :(
            </a>
            <br>
            <a href="{{ route('register') }}" class="txt1">
                No tengo una cuenta
            </a>
        </div>
    </div>


    <div class="container-login100-form-btn">
        <button class="login100-form-btn">
            Iniciar Sesión
        </button>
    </div>
</form>
@endsection
