<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fixed extends Model
{
    public function tables()
    {
    	return $this->hasMany('App\Models\Table');
    }

    public function business()
    {
    	return $this->belongsTo('App\Models\Business');
    }

    public function room()
    {
    	return $this->belongsTo('App\Models\Room');
    }
}
