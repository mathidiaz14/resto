<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Business extends Model
{
    protected $fillable = [
        'name', 'email', 'api_code'
    ];

    public function products()
    {
    	return $this->hasMany('App\Models\Product');
    }

    public function settings()
    {
    	return $this->hasMany('App\Models\Setting');
    }

    public function users()
    {
    	return $this->hasMany('App\Models\User');
    }

    public function categories()
    {
    	return $this->hasMany('App\Models\Category');
    }

    public function commands()
    {
    	return $this->hasMany('App\Models\Command');
    }

    public function suppliers()
    {
    	return $this->hasMany('App\Models\Supplier');
    }

    public function rooms()
    {
    	return $this->hasMany('App\Models\Room');
    }

    public function clients()
    {
    	return $this->hasMany('App\Models\Client');
    }

    public function printers()
    {
    	return $this->hasMany('App\Models\Printer');
    }

    public function fixeds()
    {
    	return $this->hasMany('App\Models\Fixed');
    }

    public function reservations()
    {
    	return $this->hasMany('App\Models\Reservation');
    }

    public function tickets()
    {
    	return $this->hasMany('App\Models\Ticket');
    }

    public function arches()
    {
    	return $this->hasMany('App\Models\Arching')->orderBy('created_at', 'DESC');
    }

    public function deliveries()
    {
        return $this->hasMany('App\Models\Delivery');
    }

    public function notifications()
    {
        return $this->hasMany('App\Models\Notification');
    }

    public function retirements()
    {
        return $this->hasMany('App\Models\Retirement');
    }

    public function discounts()
    {
        return $this->hasMany('App\Models\Discount');
    }

    public function hours()
    {
        return $this->hasMany('App\Models\Hour');
    }
}
