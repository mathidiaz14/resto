<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Command extends Model
{
    public function table()
    {
    	return $this->belongsTo('App\Models\Table');
    }

    public function product()
    {
    	return $this->belongsTo('App\Models\Product');
    }

    public function client()
    {
    	return $this->belongsTo('App\Models\Client');
    }

    public function business()
    {
        return $this->belongsTo('App\Models\Business');
    }

    public function arching()
    {
        return $this->belongsTo('App\Models\Arching');
    }

    public function delivery()
    {
        return $this->belongsTo('App\Models\Delivery');
    }

    public function printer()
    {
        return $this->belongsTo('App\Models\Printer');
    }
}
