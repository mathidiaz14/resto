<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
   	public function business()
    {
    	return $this->belongsTo('App\Models\Business');
    }

    public function fixeds()
    {
    	return $this->hasMany('App\Models\Fixed');
    }
}
