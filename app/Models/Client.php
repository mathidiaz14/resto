<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    public function business()
    {
        return $this->belongsTo('App\Models\Business');
    }

    public function commands()
    {
    	return $this->hasMany('App\Models\Command');
    }

    public function reservations()
    {
    	return $this->hasMany('App\Models\Reservation');
    }

    public function deliveries()
    {
        return $this->hasMany('App\Models\Delivery');
    }

    public function tables()
    {
        return $this->hasMany('App\Models\Table');
    }
}
