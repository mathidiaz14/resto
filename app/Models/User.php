<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'type', 'business_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function business()
    {
        return $this->belongsTo('App\Models\Business');
    }

    public function tables()
    {
        return $this->hasMany('App\Models\Table');
    }

    public function archings()
    {
        return $this->hasMany('App\Models\Arching');
    }

    public function deliveries()
    {
        return $this->hasMany('App\Models\Delivery');
    }

    public function histories()
    {
        return $this->hasMany('App\Models\History');
    }

    public function hours()
    {
        return $this->hasMany('App\Models\Hour');
    }

    public function retirements()
    {
        return $this->hasMany('App\Models\Retirement');
    }
}
