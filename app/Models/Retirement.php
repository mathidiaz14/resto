<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Retirement extends Model
{
    public function arching()
    {
    	return $this->belongsTo('App\Models\Arching');
    }

    public function business()
    {
    	return $this->belongsTo('App\Models\Business');
    }

    public function user()
    {
    	return $this->belongsTo('App\Models\User');
    }
}
