<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Table extends Model
{
    public function arching()
    {
    	return $this->belongsTo('App\Models\Arching');
    }

    public function fixed()
    {
    	return $this->belongsTo('App\Models\Fixed');
    }

    public function user()
    {
    	return $this->belongsTo('App\Models\User');
    }

    public function commands()
    {
        return $this->hasMany('App\Models\Command');
    }

    public function client()
    {
        return $this->belongsTo('App\Models\Client');
    }

    public function discounts()
    {
        return $this->hasMany('App\Models\Discount');
    }
}
