<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    public function business()
    {
    	return $this->belongsTo('App\Models\Business');
    }

    public function client()
    {
    	return $this->belongsTo('App\Models\Client');
    }
}
