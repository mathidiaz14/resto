<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    public function business()
    {
        return $this->belongsTo('App\Models\Business');
    }

    public function commands()
    {
    	return $this->hasMany('App\Models\Command');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function client()
    {
        return $this->belongsTo('App\Models\Client');
    }
}
