<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    public function business()
    {
    	return $this->belongsTo('App\Models\Business');
    }

    public function printer()
    {
    	return $this->belongsTo('App\Models\Printer');
    }
}
