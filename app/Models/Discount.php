<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    public function business()
    {
    	return $this->belongsTo('App\Models\Business');
    }

    public function table()
    {
    	return $this->belongsTo('App\Models\Table');
    }
}
