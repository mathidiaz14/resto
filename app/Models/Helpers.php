<?php

function Setting($key)
{
	$setting = App\Models\Setting::where('business_id', Auth::user()->business_id)->where('key', $key)->first();
	
	if ($setting == null)
		return null;
	else
		return $setting->value;
}

function check_printer($id)
{
	$printer = App\Models\Printer::find($id);

	if($printer->updated_at == NULL)
		return false;

	if(\Carbon\Carbon::now()->diffInSeconds($printer->updated_at) < 11)
		return true;

	return false;
}

function ticket_code()
{
	do
    {
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        
        $code   = substr(str_shuffle($permitted_chars), 0, 20);
        $old    = App\Models\Ticket::where('code', $code)->first();
        
    }while($old != null);	

    return $code;
}

function print_ticket($printer_id, $type, $content)
{
	$printer = App\Models\Printer::find($printer_id);
	
    if (($printer->business_id != Auth::user()->business_id) or (!check_printer($printer_id)))
    	return false;
    
    $ticket                 = new App\Models\Ticket();
    $ticket->status         = "pending";
    $ticket->type           = $type;
    $ticket->printer_id     = $printer->id;
    $ticket->printer_code   = $printer->code;
    $ticket->business_api   = Auth::user()->business->api_code;
    
    $ticket->text           = "
		<!DOCTYPE html>
		<html lang='es'>
		<head>
			<title>Ticket</title>
			<meta charset='UTF-8'>
			<meta http-equiv='x-ua-compatible' content='IE=9'>
			<style>
				hr
				{
					border: solid 2px black!important
				}

				body
				{
					font-family: monospace, monospace;
					font-size: .8em;
				}

				.text-center
				{
					text-align: center;
				}

				.text-left
				{
					text-align: left;
				}

				.col-left
				{
					float:left;
					width: 70%;
				}

				.col-right
				{
					float:left;
					width: 30%;
				}

				.col
				{
					float:left;
					width: 100%;
				}

				.col-25
				{
					float:left;
					width: 25%;
				}
				
				.col-30
				{
					float:left;
					width: 30%;
				}

				.col-50
				{
					float:left;
					width: 50%;
				}

				.col-70
				{
					float:left;
					width: 70%;
				}
				
				.row:before,
				.row:after {
					display: table;
					content: ' ';
				}

				.row:after {
				  clear: both;
				}

			</style>
		</head>
		<body>
			".$content."
		</body>
		</html>
		";

    $ticket->business_id    = Auth::user()->business_id;
    $ticket->save();
    
    return true;
}

function check_user_hours()
{
	//Si es Admin no reviso el horario, siempre puede entrar
	if (Auth::user()->type == "admin")
		return true;

	//de otra forma cargo las variables de la hora actual y de las horas que puede entrar el usuario este dia
	$now 		= \Carbon\Carbon::now();
    $day 		= Auth::user()->hours
						->where('day', strtolower($now->Format('l')))
						->first();
    
    $start_hour = \Carbon\Carbon::createFromFormat(
										'Y-m-d H:i', 
										$now->format('Y-d-m'." ".$day->hour_start)
									);
	
	$end_hour 	= \Carbon\Carbon::createFromFormat(
										'Y-m-d H:i', 
										$now->format('Y-d-m'." ".$day->hour_end)
									);


    
    //Si en ese horario el usuario tiene habilitado y esta habilitado el dia lo dejo entrar
    if (($now->between($start_hour,$end_hour)) and ($day->status == 'checked'))
    	return true;

    //de otra forma no
    return false;
}