<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    public function business()
    {
    	return $this->belongsTo('App\Models\Business');
    }

    public function products()
    {
    	return $this->hasMany('App\Models\Product');
    }
}
