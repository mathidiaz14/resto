<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function business()
    {
        return $this->belongsTo('App\Models\Business');
    }

    public function supplier()
    {
    	return $this->belongsTo('App\Models\Supplier');
    }

    public function printer()
    {
    	return $this->belongsTo('App\Models\Printer');
    }

    public function category()
    {
    	return $this->belongsTo('App\Models\Category');
    }

    public function commands()
    {
    	return $this->hasMany('App\Models\Command');
    }
}
