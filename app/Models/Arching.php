<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Arching extends Model
{
    public function business()
    {
    	return $this->belongsTo('App\Models\Business');
    }

    public function user()
    {
    	return $this->belongsTo('App\Models\User');
    }

    public function commands()
    {
        return $this->hasMany('App\Models\Command');
    }

    public function tables()
    {
        return $this->hasMany('App\Models\Table');
    }

    public function retirements()
    {
        return $this->hasMany('App\Models\Retirement');
    }

    public function payments()
    {
        return $this->hasMany('App\Models\Payment');
    }
}
