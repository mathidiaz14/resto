<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Printer extends Model
{
    public function business()
    {
    	return $this->belongsTo('App\Models\Business');
    }

    public function products()
    {
    	return $this->hasMany('App\Models\Product');
    }

    public function tickets()
    {
    	return $this->hasMany('App\Models\Ticket');
    }

    public function commands()
    {
        return $this->hasMany('App\Models\Command');
    }
}
