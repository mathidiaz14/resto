<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\User;

class waiter
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check())  
        {
            if (check_user_hours())
                return $next($request);
            
            Session(['error' => lang('message.error_hour_login')]);
            Auth::logout();
        }

        return redirect('login');
    }
}
