<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class cashier
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check())  
        {
            if(Auth::user()->type == 'waiter')
                return redirect('home');
            
            if (check_user_hours())
                return $next($request);
            
            Session(['error' => lang('message.error_hour_login')]);
            Auth::logout();
        }

        return redirect('login');
    }
}
