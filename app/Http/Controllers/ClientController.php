<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Client;
use Auth;

class ClientController extends Controller
{
    private $path = "admin.client.";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Auth::user()->business->clients->where('status', '!=', 'deleted');

        return view($this->path."index", compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->path."create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $client                 = new Client();
        $client->name           = $request->name;
        $client->address        = $request->address;
        $client->phone          = $request->phone; 
        $client->business_id    = Auth::user()->business->id;
        $client->save();

        return redirect('client');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $client = Client::find($id);

        if ($client->business == Auth::user()->business) 
            return view('table.client', compact('client'));

        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client = Client::find($id);

        if ($client->business == Auth::user()->business) 
        {
            return view($this->path.'edit', compact('client'));
        }

        return redirect('client');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $client = Client::find($id);

        if ($client->business == Auth::user()->business) 
        {
            $client->name           = $request->name;
            $client->address        = $request->address;
            $client->phone          = $request->phone; 
            $client->save();

            return redirect('client');            
        }

        return redirect('client');   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = Client::find($id);

        if ($client->business == Auth::user()->business) 
        {
            $client->status = "deleted";
            $client->save();
            
            return redirect('client');
        }

        return redirect('client');
    }

    public function save(Request $request)
    {
        $client                 = new Client();
        $client->name           = $request->name;
        $client->address        = $request->address;
        $client->phone          = $request->phone;
        $client->business_id    = Auth::user()->business_id;
        $client->save();

        return [
            "id" => $client->id,
            "name" => $client->name,
        ];
    }
}
