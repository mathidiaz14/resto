<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use Auth;

class ProductController extends Controller
{
    private $path = "admin.product.";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Auth::user()->business->products->where('status', '!=', 'deleted');

        return view($this->path.'index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        return view($this->path.'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product                = new Product();
        $product->name          = $request->name;
        $product->price         = $request->price;
        $product->quantity      = $request->quantity;
        
        if ($request->quantity_min_option == "no")
            $product->quantity_min  = "no";
        else
            $product->quantity_min  = $request->quantity_min;

        $product->category_id   = $request->category_id;
        $product->printer_id    = $request->printer_id;
        $product->supplier_id   = $request->supplier_id;
        $product->business_id   = Auth::user()->business->id;
        $product->save();

        return redirect('product');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);

        if ($product->business == Auth::user()->business) 
        {
            return view($this->path."show", compact('product'));
        }

        return redirect('home');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);

        if ($product->business == Auth::user()->business) 
        {
            return view($this->path."edit", compact('product'));
        }

        return redirect('home');   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);

        if ($product->business == Auth::user()->business) 
        {
            $product->name          = $request->name;
            $product->price         = $request->price;
            $product->quantity      = $request->quantity;
            
            if ($request->quantity_min_option == "no")
                $product->quantity_min  = $request->quantity_min_option;
            else
                $product->quantity_min  = $request->quantity_min;

            $product->category_id   = $request->category_id;
            $product->printer_id    = $request->printer_id;
            $product->save();

            return redirect('product');
        }

        return redirect('home');   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);

        if ($product->business == Auth::user()->business) 
        {
            $product->status = "deleted";
            $product->save();
            
            return redirect('product');
        }

        return redirect('home');   
    }

    public function get($id)
    {
        $product = Product::find($id);

        return $product;
    }
}
