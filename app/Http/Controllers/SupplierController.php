<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Supplier;
use Auth;

class SupplierController extends Controller
{
    private $path = "admin.supplier.";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $suppliers = Auth::user()->business->suppliers->where('status', '!=', 'deleted');

        return view($this->path."index", compact('suppliers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->path."create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $supplier               = new Supplier();
        $supplier->name         = $request->name;
        $supplier->rut          = $request->rut;
        $supplier->address      = $request->address;
        $supplier->email        = $request->email;
        $supplier->phone        = $request->phone;
        $supplier->business_id  = Auth::user()->business->id;
        $supplier->save();

        return redirect('supplier');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $supplier = Supplier::find($id);

        if ($supplier->business == Auth::user()->business)
            return view($this->path."show", compact('supplier'));

        return redirect('home');
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $supplier = Supplier::find($id);

        if ($supplier->business == Auth::user()->business)
            return view($this->path."edit", compact('supplier'));

        return redirect('home');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $supplier = Supplier::find($id);

        if ($supplier->business == Auth::user()->business)
        {
            $supplier->name         = $request->name;
            $supplier->rut          = $request->rut;
            $supplier->address      = $request->address;
            $supplier->phone        = $request->phone;
            $supplier->save();

            return redirect('supplier');   
        }

        return redirect('home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $supplier = Supplier::find($id);

        if ($supplier->business == Auth::user()->business)
        {
            $supplier->status = "deleted";
            $supplier->save();
            
            return redirect('supplier');
        }

        return redirect('home');
    }
}
