<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Retirement;
use App\Models\Ticket;
use App\Models\Arching;
use Auth;

class RetirementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $retirement                 = new Retirement();
        $retirement->observation    = $request->observation;
        $retirement->quantity       = $request->quantity;
        $retirement->arching_id     = $request->arching_id;
        $retirement->user_id        = Auth::user()->id;
        $retirement->save();

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $retirement = Retirement::find($id);

        if($retirement->arching->business_id == Auth::user()->business_id)
        {
            $retirement->status = "deleted";
            $retirement->save();
        }

        return back();
    }

    public function print($id)
    {
        if(!check_printer(Auth::user()->business->printer_admin))
        {
            Session(['error' => 'La impresora de la caja no esta en linea, no se pudo imprimir el arqueo.']);
            
            return back();
        }
        
        $retirement = Retirement::find($id);

        if($retirement->arching->business_id != Auth::user()->business_id)
        {
            Session(['error' => 'El retiro no corresponde a su empresa.']);
            return back();   
        }
        
        $content = 
            "<div class='col text-center'>
                <h2>
                    Retiros de caja
                </h2>
            </div><hr>
            <div class='col-70'>
                ".$retirement->observation.":
            </div>
            <div class='col-30'>
                <b>$".$retirement->quantity."</b>
            </div>
            <div class='col-50'>
                <b>".$retirement->user->name."</b>
            </div>
            <div class='col-50'>
                <b>".$retirement->created_at->format('d/m/Y H:i')."</b>
            </div>
            <div class='col text-center'>
                <p>-- Fin de ticket --</p>
            </div>";
        
        if(print_ticket(Auth::user()->business->printer_admin, 'retirament', $content))
            Session(['success' => 'Se envio a imprimir el ticket']);
        else
            Session(['error' => 'No se pudo imprimir el retiro.']);

        
        return back();
    }

    public function print_arching($id)
    {
        
        $arching = Arching::find($id);

        if($arching->business_id != Auth::user()->business_id)
        {
            Session(['error' => 'El retiro no corresponde a su empresa.']);
            return back();   
        }
        
        if($arching->retirements->where('status', '!=', 'deleted')->count() == 0)
        {
            Session(['error' => 'No hay retiros para imprimir.']);   
            return back();   
        }

        $content = "
            <div class='col text-center'>
                <h2>
                    Retiros de caja
                </h2>
            </div><hr>";


        foreach($arching->retirements->where('status', '!=', 'deleted') as $retirement)
        {
            $content .= 
            "<div class='col-70'>
                ".$retirement->observation.":
            </div>
            <div class='col-30'>
                <b>$".$retirement->quantity."</b>
            </div>
            <div class='col-50'>
                <b>".$retirement->user->name."</b>
            </div>
            <div class='col-50'>
                <b>".$retirement->created_at->format('d/m/Y H:i')."</b>
            </div>
            <hr>";
        }

        $content .= "  
            <div class='col text-center'>
                <p>-- Fin de ticket --</p>
            </div>";

        if(print_ticket(Auth::user()->business->printer_admin, 'retirament', $content))
            Session(['success' => 'Se envio a imprimir el ticket']);
        else
            Session(['error' => 'No se pudo imprimir el retiro.']);

        return back();
    }
}
