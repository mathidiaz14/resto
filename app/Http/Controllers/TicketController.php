<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ticket;
use Auth;

class TicketController extends Controller
{
    private $path = "admin.ticket.";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tickets = Auth::user()->business->tickets()->where('status', '!=', 'deleted')->paginate(30);

        return view($this->path."index", compact('tickets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ticket = Ticket::find($id);
        $content = "<!DOCTYPE html>
        <html lang='es'>
        <head>
            <title>Ticket</title>
            <meta charset='UTF-8'>
            <meta http-equiv='x-ua-compatible' content='IE=9'>
            <style>
                hr
                {
                    border: solid 2px black!important
                }

                body
                {
                    font-family: monospace, monospace;
                    font-size: .8em;
                }

                .text-center
                {
                    text-align: center;
                }

                .text-left
                {
                    text-align: left;
                }

                .col-left
                {
                    float:left;
                    width: 70%;
                }

                .col-right
                {
                    float:left;
                    width: 30%;
                }

                .col
                {
                    float:left;
                    width: 100%;
                }

                .col-25
                {
                    float:left;
                    width: 25%;
                }
                
                .col-30
                {
                    float:left;
                    width: 30%;
                }

                .col-50
                {
                    float:left;
                    width: 50%;
                }

                .col-70
                {
                    float:left;
                    width: 70%;
                }
                
                .row:before,
                .row:after {
                    display: table;
                    content: ' ';
                }

                .row:after {
                  clear: both;
                }

            </style>
        </head>
        <body>
            ".$ticket->text."
        </body>
        </html>
        ";;

        return view($this->path."show", compact('content'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ticket = Ticket::find($id);

        print_ticket($ticket->printer->id, 'command', $ticket->text);

        Session(['success' => "El ticket se reimprimio correctamente"]);

        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
