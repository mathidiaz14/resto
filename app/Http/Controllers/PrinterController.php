<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Printer;
use App\Models\Ticket;
use Auth;

class PrinterController extends Controller
{
    private $path = "admin.printer.";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $printers = Auth::user()->business->printers->where('status', 'enabled');
        
        return view($this->path."index", compact("printers"));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->path."create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $printer                = new Printer();
        $printer->name          = $request->name;
        $printer->code          = $request->code;
        $printer->business_id   = Auth::user()->business->id;
        $printer->save();

        return redirect("printer");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $printer = Printer::find($id);

        if ($printer->business == Auth::user()->business) 
        {
            return view($this->path."show", compact('printer'));
        }

        return redirect('home');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $printer = Printer::find($id);

        if ($printer->business == Auth::user()->business) 
        {
            return view($this->path."edit", compact('printer'));
        }

        return redirect('home');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $printer = Printer::find($id);

        if ($printer->business_id == Auth::user()->business_id) 
        {
            $printer->name      = $request->name;
            $printer->address   = $request->address;
            $printer->save();

            return redirect("printer");
        }

        return redirect('home');
    }

    public function test($id)
    {
        $printer = Printer::find($id);

        $content = "<div class='row'>
                        <div class='col text-center'>
                            <h1>
                                RestoAPP.uy
                            </h1>
                        </div>
                        <hr>
                        <div class='col text-center'>
                            <h2>Prueba de la impresora \"".$printer->name."\"</h2>
                        </div>
                    </div>";

        if(print_ticket($id, 'test', $content))
            Session(['success' => 'Se envio una prueba a la impresora: '.$printer->name]);
        else
            Session(['error' => 'No se pudo enviar a imprimir']);
        
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $printer = Printer::find($id);

        if ($printer->business == Auth::user()->business) 
        {
            foreach($printer->products->where('status', '!=', 'deleted') as $product)
            {
                $product->printer_id = null;
                $product->save();
            }
            
            $printer->status = "deleted";
            $printer->save();
            
            return redirect("printer");
        }

        return redirect('home');
    }
}
