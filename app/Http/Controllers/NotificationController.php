<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Notification;
use Auth;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notifications = Auth::user()->business->notifications->where('status', '!=', 'deleted');

        foreach($notifications as $notification)
        {
            $notification->status = "disabled";
            $notification->save();
        }

        return view('admin.notification.index', compact('notifications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $notification           = Notification::find($id);
        $notification->status   = "disabled";
        $notification->save();

        return redirect($notification->link);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $notification = Notification::find($id);

        $notification->delete();

        return back();
    }

    public function query()
    {
        return Auth::user()->business->notifications->where('status', 'enabled')->count();
    }

    public function all()
    {
        return view('helpers.notifications');
    }

    public function read()
    {
        $notifications = Auth::user()->business->notifications->where('status', 'enabled');

        foreach($notifications as $notification)
        {
            $notification->status = "read";
            $notification->save();
        }
    }

}
