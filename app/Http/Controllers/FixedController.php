<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Fixed;
use App\Models\Table;
use Auth;

class FixedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $oldTable = Fixed::where('business_id', $request->business_id)->where('status', '!=', 'deleted')->orderBy('id', 'DESC')->first();

        $table = new Fixed();
        
        if ($oldTable != null)
        {
            $table->name = intval($oldTable->name) + 1;
        }
        else
        {
            $table->name = "1";
        }

        $table->x           = $request->x;
        $table->y           = $request->y;
        $table->type        = "standard";
        $table->status      = "enabled";
        $table->room_id     = $request->room_id;
        $table->business_id = Auth::user()->business->id;
        $table->save();

        return [
            'position'  => $table->x.$table->y,
            'name'      => $table->name,
            'id'        => $table->id, 
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $table = Table::find($id);
        
        $table->name = $request->name;
        $table->type = $request->shape;
        $table->save();

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $fixed = Fixed::find($id);

        if ($fixed->business == Auth::user()->business)
        {
            $fixed->status = "deleted";
            $fixed->save();
        }

        return back();
    }

    public function get($id)
    {
        $table      = Table::find($id);
        
        if ($table->business == Auth::user()->business)
            return $table;
        else
            return null;
    }

    public function form($id, $type)
    {
        $table = Fixed::find($id);

        if($table->business == Auth::user()->business)
        {
            $table->type = $type;
            $table->save();
            return "true";
        }else
        {
            return "false";
        }
    }

    public function search(Request $request)
    {
        $table = Fixed::where('name', $request->name)->where('status', '!=', 'deleted')->first();

        if ($table != null) 
        {
            if ($table->business == Auth::user()->business) 
            {
                if((Auth::user()->business->archings != null) and (Auth::user()->business->archings->last()->status == "enabled"))
                    return redirect('table', $table->id);    
                else
                    return redirect('home');
                
            }
        }

        return redirect('home');
    }

    public function edit_name(Request $request)
    {
        $name = $request->name;

        if ($name == "")
            $name = "0";
        
        $fixed = Fixed::find($request->id);
        $fixed->name = $name;
        $fixed->save();

        return "ok";
    }
}
