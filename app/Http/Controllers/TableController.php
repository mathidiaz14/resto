<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Table;
use App\Models\Fixed;
use App\Models\Room;
use App\Models\Ticket;
use App\Models\Discount;
use App\Models\Arching;
use Auth;

class TableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fixed                  = Fixed::find($request->table);
        $fixed->status          = "occupied";
        $fixed->save();

        $table                  = new Table();
        $table->diners          = $request->diners;
        $table->observation     = $request->observation;
        $table->user_id         = $request->waiter;
        $table->client_id       = $request->client;
        $table->fixed_id        = $request->table;  
        $table->arching_id      = Arching::where('status', 'enabled')->first()->id;
        $table->status          = "enabled";
        $table->save();

        return redirect('table/'.$fixed->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $fixed = Fixed::find($id);

        if ($fixed->business == Auth::user()->business)
        {
            $table      = $fixed->tables->where('status', 'enabled')->first();
            
            if ($table != null)
                return view('table.show', compact('table', 'fixed'));
            else
                return redirect('home');
        }
        
        return redirect('home');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $table = Table::find($id);

        $table->fixed->status = "enabled";
        $table->fixed->save();

        $table->delete();

        return redirect('room/'.$table->fixed->room->id);        
    }

    public function close($id)
    {
        $table = Table::find($id);
        
        if ($table->fixed->business->id == Auth::user()->business->id) 
        {

            $table->fixed->status = "enabled";
            $table->fixed->save();

            $table->status  = "disabled";
            $table->total   = ($table->commands->sum('pay') - $table->discounts->sum('quantity'));
            $table->save();

            return redirect('room/'.$table->fixed->room->id);
        }
        
        return redirect('home');

    }

    public function get_charge($id)
    {
        $table = Table::find($id);
        
        if ($table->fixed->business->id == Auth::user()->business->id) 
        {
            
            $commands_list  = $table->commands->where('status', '!=', 'pending')->where('status', '!=', 'deleted')->groupBy('product_id');
            $commands_all   = $table->commands->where('status', '!=', 'pending')->where('status', '!=', 'deleted');


            return view('table.charge', compact('table', 'commands_list', 'commands_all'));
        }

        return back();   
    }

    public function set_charge(Request $request, $id)
    {   
        $table = Table::find($id);

        $flag  = false;
        $total = 0;
        
        //controlo si hay alguna comanda seleccionada para pagar por separado
        foreach ($table->commands->where('status', '!=', 'deleted') as $command) 
        {
            if ($request['check_'.$command->id] == "yes") 
            {
                $flag   = true;
                $total  += $command->price;
                
                $command->status        = "disabled";
                $command->wayOfPay      = $request->wayOfPay;
                $command->pay           = $command->price;

                $command->save();
            }
        }

        //si $flag esta en false es que no entro en el if anterior, es decir que no hay ninguna comanda seleccionada y por eso las selecciono todas
        if ($flag == false) 
        {
            foreach ($table->commands->where('status', '!=', 'deleted') as $command) 
            {
                if($command->status == "enabled")
                {
                    $total += $command->price;
                    
                    $command->status        = "disabled";
                    $command->wayOfPay      = $request->wayOfPay;
                    $command->pay           = $command->price;

                    $command->save();
                }
            }
        }
            
        return back()->with('total', $total);
    }

    public function history($id)
    {
        $table = Table::find($id);

        if($table->fixed->business == Auth::user()->business)
        {
            $commands_list  = $table->commands->where('status', 'enabled')->groupBy('product_id');
            
            return view("table.history", compact('commands_list', 'table'));
        }

        return back();   
    }

    public function print($id)
    {
        $table          = Table::find($id);
        $total          = 0;
        $total_discount = 0;

        if($table->commands->where('status', 'enabled')->count() == 0)
        {
            Session(["error" => "No hay nada para imprimir en esta mesa"]);
            return back();   
        }

        $content    = "
            <div class='row'>
                <div class='col text-center'>
                    <h3>
                        Cuenta de Mesa ".$table->fixed->name."
                    </h3>
                    <p>
                        ".\Carbon\Carbon::now('America/Montevideo')->format('d/m/Y h:i')."
                    </p>
                </div>
            </div>
        ";

        foreach($table->commands->where('status', 'enabled')->groupBy('product_id') as $commands)
        {
            $content .= 
                "<div class='row'>
                    <div class='text-left' style='float:left;width: 60%;'>
                        <b>".$commands->first()->product_name."</b>
                    </div>
                    <div class='text-center' style='float:left;width: 40%;'>
                        <b>x".$commands->count()." $".$commands->count() * $commands->first()->price."</b>
                    </div>
                </div><hr>";
            
            $total += $commands->count() * $commands->first()->price;
            $flag = true;
        }

        foreach($table->discounts->where('status', '!=', 'deleted') as $discount)
        {
            if($discount->type == "quantity") 
                $total_discount += $discount->quantity;
            else 
                $total_discount += ($table->commands->where('status', 'enabled')->sum('price') * $discount->quantity) / 100;
        }

        if($total_discount != 0)
        {
            $content .= 
            "<div class='row'>
                <div class='col-left text-left'>
                    <b>Sub total:</b>
                </div>
                <div class='col-right text-center'>
                    <b>$ ".$total."</b>
                </div>
            </div>
            <div class='row'>
                <div class='col-left text-left'>
                    <b>Descuento:</b>
                </div>
                <div class='col-right text-center'>
                    <b>$ ".$total_discount."</b>
                </div>
            </div><hr>";
        }

        $content .= 
            "<div class='row'>
                <div class='col-left text-left'>
                    <b>Total:</b>
                </div>
                <div class='col-right text-center'>
                    <b>$ ".($total - $total_discount)."</b>
                </div>
                <br>
                <div class='col-xs-12 text-center'>
                    <p>-- Fin de cuenta --</p>
                </div>
            </div>";
        

        if(Auth::user()->business->printer_admin == null)
        {
            Session(["error" => "No hay una impresora predeterminada para imprimir los tickets de caja, debe seleccionarla desde la sección de opciones."]);
            
            return back();
        }
        
        if(print_ticket(Auth::user()->business->printer_admin, 'table', $content))
            Session(['success' => 'Se envio a imprimir el ticket']);
        else
            Session(['error' => 'No se pudo imprimir el retiro.']);

        return back();   
    }
}
