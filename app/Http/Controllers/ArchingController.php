<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Arching;
use App\Models\Ticket;
use App\Models\Table;
use Carbon\Carbon;
use Auth;

class ArchingController extends Controller
{
    private $path = "admin.arching.";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $arches = Auth::user()->business->arches()->where('status', '!=', 'deleted')->paginate(30);

        return view($this->path."index", compact('arches'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $arching                = new Arching();
        $arching->start         = $request->quantity;
        $arching->status        = "enabled";
        $arching->user_id       = Auth::user()->id;
        $arching->business_id   = Auth::user()->business->id;
        $arching->save();

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $arching = Arching::find($id);

        if($arching->business_id == Auth::user()->business_id)
            return view($this->path."show", compact('arching'));

        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $arching            = Arching::find($id);
        $arching->end       = $request->quantity;
        $arching->status    = "disabled";
        $arching->save();

        return back();
    }

    public function print($id)
    {
        $arching = Arching::find($id);

        if($arching->business_id == Auth::user()->business_id)
        {
            $retirements = "";

            foreach($arching->retirements->where('status', '!=', 'deleted') as $retirement)
            {
                $retirements .= "<div class='col-left'>
                        ".$retirement->observation.":
                    </div>
                    <div class='col-right'>
                        <b>$".$retirement->quantity."</b>
                    </div>";
            }

            $discounts = 0;

            foreach($arching->tables->where('status', '!=', 'deleted') as $table)
                $discounts += $table->discounts->sum('quantity');
            
            $content = "
                <!-- caja -->
                <div class='row'>
                    <div class='col text-center'>
                        <h2>
                            Cierre de caja
                        </h2>
                        <p>
                            <b>Desde:</b> ".$arching->created_at->format('d/m/Y H:i')." <br>
                            <b>Hasta:</b> ".$arching->updated_at->format('d/m/Y H:i')."
                        </p>
                    </div>
                </div>
                    
                <hr>

                <div class='row'>
                    <div class='col-left text-left'>
                        Cantidad de mesas:
                    </div>
                    <div class='col-right text-left'>
                        <b>".$arching->tables->count()."</b>
                    </div>

                    <div class='col-left text-left'>
                        Saldo inicial:
                    </div>
                    <div class='col-right text-left'>
                        <b>$ ".$arching->start."</b>
                    </div>
                    
                    <div class='col-left text-left'>
                        Total de ingresos:
                    </div>
                    <div class='col-right text-left'>
                        <b>$ ".$arching->tables->sum('total')."</b>
                    </div>

                    <div class='col-left text-left'>
                        Total de egresos:
                    </div>
                    <div class='col-right text-left'>
                        <b>$ ".$arching->retirements->sum('quantity')."</b>
                    </div>

                    <div class='col-left text-left'>
                        Saldo final en caja:
                    </div>
                    <div class='col-right text-left'>
                        <b>$ ".$arching->end."</b>
                    </div>
                    
                    <div class='col-left text-left'>
                        Saldo final en APP:
                    </div>
                    <div class='col-right text-left'>
                        <b>$ ".(($arching->tables->sum('total') - $arching->retirements->sum('quantity')) + $arching->start) ."</b>
                    </div>

                    <div class='col-left text-left'>
                        Diferencia:
                    </div>
                    <div class='col-right text-left'>
                        <b>$ ".((($arching->tables->sum('total') - $arching->end) - $arching->retirements->sum('quantity')) + $arching->start)."</b>
                    </div>
                </div>

                <hr>

                <div class='row'>
                    <div class='col text-center'>
                        <b>Detalle de ingresos</b>
                    </div>
                    <hr>
                    <div class='col-left'>
                        Efectivo:
                    </div>
                    <div class='col-right'>
                        <b>$ ".$arching->commands->where('wayOfPay', 'cash')->sum('pay')."</b>
                    </div>
                    <div class='col-left'>
                        Debito:
                    </div>
                    <div class='col-right'>
                        <b>$ ".$arching->commands->where('wayOfPay', 'debit')->sum('pay')."</b>
                    </div>
                    <div class='col-left'>
                        Visa:
                    </div>
                    <div class='col-right'>
                        <b>$ ".$arching->commands->where('wayOfPay', 'visa')->sum('pay')."</b>
                    </div>
                    <div class='col-left'>
                        Master:
                    </div>
                    <div class='col-right'>
                        <b>$ ".$arching->commands->where('wayOfPay', 'master')->sum('pay')."</b>
                    </div>
                    <div class='col-left'>
                        Otros:
                    </div>
                    <div class='col-right'>
                        <b>$ ".$arching->commands->where('wayOfPay', 'other')->sum('pay')."</b>
                    </div>
                    <div class='col-left'>
                        Descuentos:
                    </div>
                    <div class='col-right'>
                        <b>- $".$discounts."</b>
                    </div>
                </div>

                    <hr>

                <div class='row'>
                    <div class='col text-center'>
                        <b>Detalle de egresos</b>
                    </div>
                    <hr>
                    ".$retirements."
                </div>
                
                <div class='col text-center'>
                    <p>-- Fin de caja --</p>
                </div>
            ";

            if(print_ticket(Auth::user()->business->printer_admin, 'arching', $content))
                Session(['error' => 'La impresora de la caja no esta en linea, no se pudo imprimir el arqueo.']);
            else
                Session(['success' => 'Se envio a imprimir el arqueo de caja']);
        }

        return back();
    }

    public function show_table($id)
    {
        $table = Table::find($id);

        if($table->arching->business_id == Auth::user()->business_id)
        {
            return view($this->path."table", compact('table'));
        }

        return back();
    }
}
