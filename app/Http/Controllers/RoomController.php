<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Room;
use App\Models\Table;
use Auth;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $room = Room::first();
        
        return view('table.home', compact('room'));  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $room               = new Room();
        $room->name         = $request->name;
        $room->business_id = Auth::user()->business->id;
        $room->save();

        $business      = Auth::user()->business;

        return view('table.edit', compact('business', 'room'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $business       = Auth::user()->business;
        $room           = Room::find($id);
        
        if ($room->business->id == Auth::user()->business->id)
            return view('table.home', compact('business', 'room'));
        else 
            return redirect('home');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $room       = Room::find($id);
        $business   = Auth::user()->business;

        return view('table.edit', compact('business', 'room'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $room       = Room::find($id);
        $room->name = $request->name;
        $room->save();

        return redirect('room/'.$room->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $room = Room::find($id);

        if ($room->business->id == Auth::user()->business->id) 
        {
            $room->status = "deleted";
            $room->save();
            
            return redirect('home');
        }else
        {
            return back();
        }
    }
}
