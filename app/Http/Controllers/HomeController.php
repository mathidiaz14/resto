<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect('room');
    }

    public function admin()
    {
        if(Auth::user()->type == "admin")
            return view('admin.dashboard');
        else
            return redirect('home');
    }

    public function contact(Request $request)
    {
        $cabeceras = 'From: info@restoapp.uy' . "\r\n" .
        'Reply-To: '.$request->email . "\r\n" .
        'X-Mailer: PHP/' . phpversion();

        //Cuerpo del mensaje
        $msg = "Nuevo contacto desde la web de RestoAPP";
        $msg .= "<br><hr><br>";
        $msg .= "Nombre: ".$request->name;
        $msg .= "Email: ".$request->email;
        $msg .= "Mensaje: <br>".$request->message;

        //mail('info@restoapp.uy', "Nuevo contacto desde la web de RestoAPP", $msg, $cabeceras);

        session(['message' => 'El mensaje se envio correctamente, le contestaremos a la brevedad']);

        return redirect("/#footer");

    }

    public function account()
    {
        $user = Auth::user();

        return view('table.account', compact('user'));
    }
}
