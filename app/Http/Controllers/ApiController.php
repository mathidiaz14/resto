<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Business;
use App\Models\Printer;
use Hash;
use Auth;

class ApiController extends Controller
{
    public function login(Request $request)
    {
        $login = Auth::attempt(['email' => strtolower($request->email), 'password' => $request->password]);

    	if($login)
    	{
            $user = User::where('email', $request->email)->first();

			if($user->type == "admin")
			{
    			return [
	    			"value" 		=> "YES",
	    			"message"		=> $user->business->api_code
	    		];

			}else
			{
				return [
	    			"value" 		=> "NO",
	    			"message"		=> "El usuario debe ser administrador, intentelo nuevamente."		
	    		];
			}
    	}else
    	{
    		return [
    			"value" 		=> "ERROR",
    			"message"		=> "Los datos no coinciden con nuestra base de datos, intentelo nuevamente."		
    		];
    	}
    }


    public function tickets(Request $request)
    {

    	$business = Business::where('api_code', $request->api)->first();

    	if ($business == null)
    	{
    		return [
                "value"         => "NO",
                "message"       => "NO"
            ];

    	}else
    	{
    		$printer = Printer::where('code', $request->printer)->where('status','!=','deleted')->first();

    		if ($printer != null)
            {
                if($printer->business_id == $business->id)
                {
                    $printer->updated_at = \Carbon\Carbon::now('America/Montevideo');
                    $printer->save();
                    
                    $ticket = $printer->tickets->where('status', 'pending')->first();
                    
                    if ($ticket != null)  
                    {
                        $ticket->status = "disabled";
                        $ticket->save();

                        return [
                            "value"         => "YES",
                            "message"       => $ticket->text
                        ];
                    }
                    
                }
            }

            return [
                "value"         => "NO",
                "message"       => "NO"
            ];
    	}
    }

    public function view_ticket($code)
    {
        $ticket     = App\Models\Ticket::where('code', $code)->first();

        if($ticket != null) 
        {
            $content    = $ticket->text;
            return view('tickets.index', compact('content'));
        }
        else
        {
            return null;
        }
    }
}
