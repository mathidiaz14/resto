<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Hour;
use Auth;
use Hash;

class UserController extends Controller
{

    private $path = 'admin.user.';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = Auth::user()->business->users->where('status', '!=', 'deleted');

        return view($this->path.'index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->path.'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $old = User::where('email', $request->email."@".strtolower(Auth::user()->business->name))->first();

        if($old != null)
        {
            Session(['error' => "El email \"".$request->email."@".strtolower(Auth::user()->business->name)."\" ya existe, ingrese otro y pruebe nuevamente."]);

            return back()->withInput();
        }

        $user               = new User();
        $user->name         = $request->name;
        $user->email        = $request->email."@".strtolower(Auth::user()->business->name);
        $user->password     = Hash::make($request->password);
        $user->color        = $request->color;
        $user->type         = $request->type;
        $user->business_id  = Auth::user()->business_id;
        $user->save();

        $days   = [
            'monday',
            'tuesday',
            'wednesday',
            'thursday',
            'friday',
            'saturday',
            'sunday'
        ];

        if($request->hour == 'null')
        {
            foreach($days as $day_name)
            {
                $status     = $request[$day_name."_selector"] == "on" ? "checked" : "null";

                $hour               = new Hour();
                $hour->hour_start   = $request[$day_name."_start"];
                $hour->hour_end     = $request[$day_name."_end"];
                $hour->user_id      = $user->id;
                $hour->business_id  = $user->business->id;
                $hour->day          = $day_name;
                $hour->status       = $status;
                $hour->save();
            }

        }else
        {
            foreach($days as $day_name)
            {
                $old = User::find($request->hour)->hours->where('day', $day_name)->first();

                $hour               = new Hour();
                $hour               = $old->replicate();
                $hour->user_id      = $user->id;
                $hour->save();
            }
        }

        Session(['success' => "Se creo el usuario \"".$user->name."\""]);

        return redirect('user');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        return view($this->path."edit", compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user               = User::findOrFail($id);
        $user->name         = $request->name;
        
        if($request->password != null)
            $user->password     = Hash::make($request->password);

        $user->color        = $request->color;
        $user->type         = $request->type;
        $user->save();
        
        $days   = [
            'monday',
            'tuesday',
            'wednesday',
            'thursday',
            'friday',
            'saturday',
            'sunday'
        ];

        if($request->hour == 'none')
        {
            foreach($days as $day_name)
            {
                $status     = $request[$day_name."_selector"] == "on" ? "checked" : "null";

                $hour               = $user->hours->where('day', $day_name)->first();
                $hour->hour_start   = $request[$day_name."_start"];
                $hour->hour_end     = $request[$day_name."_end"];
                $hour->user_id      = $user->id;
                $hour->business_id  = $user->business->id;
                $hour->day          = $day_name;
                $hour->status       = $status;
                $hour->save();
            }
            
        }else
        {
            foreach($days as $day_name)
            {
                $old = User::find($request->hour)->hours->where('day', $day_name)->first();

                $hour               = $user->hours->where('day', $day_name)->first();
                $hour               = $old->replicate();
                $hour->user_id      = $user->id;
                $hour->save();
            }
        }

        Session(['success' => "Se modifico el usuario \"".$user->name."\""]);

        return redirect('user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->hours()->delete();
        $user->delete();

        Session(['success' => "Se elimino el usuario \"".$user->name."\""]);

        return redirect('user');
    }
}
