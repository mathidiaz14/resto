<?php

namespace App\Http\Controllers;

use Illuminate\Support\Collection as Collection;
use Illuminate\Http\Request;
use App\Models\Command;
use App\Models\Product;
use App\Models\Table;
use App\Models\Notification;
use App\Models\Ticket;
use Auth;

class CommandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product    = Product::find($request->product);
        $table      = Table::find($request->table);
        $quantity   = $request->quantity;

        if($quantity == 0)
            $quantity = 1;

        $commands = new Collection();

        for ($i=0; $i < $quantity; $i++) 
        { 
            $command                = new Command();
            $command->table_id      = $request->table;
            $command->product_id    = $product->id;
            $command->product_name  = $product->name;
            $command->quantity      = "1";
            $command->price         = $product->price;
            $command->status        = "pending";
            $command->status_check  = "false";
            $command->printer_id    = $product->printer_id;
            $command->arching_id    = Auth::user()->business->arches->last()->id;
            $command->client_id     = $table->client_id;
            $command->business_id   = Auth::user()->business->id;
            $command->save();

            $commands->push($command);
        }

        return [
            "id"            => $command->id,
            "name"          => $command->product->name,
            "quantity"      => $quantity,
            "price_total"   => ($command->price * $quantity),
            "commands"      => $commands,
            "table"         => $command->table->id,
            "product"       => $product->id,
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $command = Command::find($id);
        $command->status = "deleted";
        $command->save();

        return back();
    }

    public function print($id)
    {
        $table  = Table::find($id);

        if($table->fixed->business == Auth::user()->business)
        {
            foreach(Auth::user()->business->printers->where('status', '!=', 'deleted') as $printer)
            {
                if (count($printer->commands->where('status', 'pending')) > 0)
                {
                    if(!check_printer($printer->id))
                    {
                        Session(['error' => 'La impresora "'.$printer->name.'" no esta en linea, no se pudieron enviar las comandas.']);
                        return back();
                    }

                    $content = 
                        "<div class='row'>
                            <div class='col text-center'>
                                <h2>
                                    Mesa ".$table->fixed->name."
                                </h2>
                                <p>
                                    ".\Carbon\Carbon::now()->format('d/m/Y h:i')."
                                </p>
                            </div>
                        </div>
                        <hr>
                    ";

                    foreach($printer->commands->where('table_id', $table->id)->where('status', 'pending')->groupBy('product_id') as $commands)
                    {
                        $content .= 
                            "<div class='row'>
                                <div class='col-left text-left'>
                                    <b>".$commands->first()->product_name."</b>
                                </div>
                                <div class='col-right text-left'>
                                    <b>x ".$commands->count()."</b>
                                </div>
                            </div>";

                        foreach($commands as $command)
                        {
                            if($command->observation != null)
                            {
                                $content .= "
                                    <div class='row'>
                                        <div class='col text-left' style='padding-left: 10%;'>
                                           1 - ".$command->observation."
                                        </div>
                                    </div>";
                            }
                        }
                    }

                    $content .= "
                        <hr>
                        <div class='col-xs-12 text-center'>
                            <b>Comanda enviada por: ".$table->user->name."</b>
                        </div>
                        <div class='col-xs-12 text-center'>
                            <p>-- Fin de comanda --</p>
                        </div>
                    ";

                    print_ticket($printer->id, 'command', $content);

                    foreach ($printer->commands->where('table_id', $table->id)->where('status', 'pending') as $command) 
                    {
                        $command->status = 'enabled';
                        $command->save();

                        $product            = $command->product;
                        
                        if ($product->quantity > 0)
                            $product->quantity -= $command->quantity;
                        
                        $product->save();

                        //controlo si llego al minimo de productos, muestro la notificación solo una vez
                        if (($product->quantity_min != null ) and (($product->quantity + $command->quantity) > $product->quantity_min) and ($product->quantity <= $product->quantity_min))
                        {
                            $notification               = new Notification();
                            $notification->title        = "Un producto llego al minimo";
                            $notification->description  = "El producto ".$product->name." llego al minimo, ".$product->quantity." unidades.";
                            $notification->type         = "product";
                            $notification->status       = "enabled";
                            $notification->link         = url('product', $product->id);
                            $notification->business_id  = Auth::user()->business->id;
                            $notification->save();
                        }
                    }
                }
            }
        }

        return back();
    }

    public function check($id)
    {
        $command = Command::find($id);

        if($command->business_id == Auth::user()->business_id) 
        {
            if ($command->status_check == "false")
                $command->status_check = "true";
            else       
                $command->status_check = "false";
            
            $command->save();
        }
    }

    public function delete_all(Request $request)
    {
        $table = Table::find($request->table);

        if ($table->fixed->business == Auth::user()->business) 
        {
            $quantity = 0;

            foreach ($table->commands->where('status', 'pending') as $command) 
            {
                if($command->product_id == $request->product)
                {
                    if($quantity < $request->quantity)
                    {
                        $command->status = "deleted";
                        $command->save();
                        $quantity ++;
                    }
                }
            }
        }

        return back();
    }

    public function observation(Request $request)
    {
        $command                = Command::find($request->command);
        $command->observation   = $request->observation;
        $command->save();

        return back();
    }
}
