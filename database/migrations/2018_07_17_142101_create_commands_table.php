<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commands', function (Blueprint $table) {
            $table->increments('id');
            $table->string('table_id');
            $table->string('product_id');
            $table->string('product_name');
            $table->string('observation')->nullable();
            $table->string('quantity');
            $table->string('price');
            $table->string('status');
            $table->string('pay')->nullable();
            $table->string('wayOfPay')->nullable();
            $table->string('status_check')->default('false')->nullable();
            $table->string('client_id')->nullable();
            $table->string('printer_id');
            $table->string('arching_id');
            $table->string('business_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commands');
    }
}
