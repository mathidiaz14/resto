<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFixedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fixeds', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('x');
            $table->string('y');
            $table->string('type');
            $table->string('status');
            $table->string('room_id');
            $table->string('business_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fixeds');
    }
}
