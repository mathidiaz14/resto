<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        DB::table('businesses')->insert([
            'name' => 'RestoAPP',
            'email' => 'admin@restoapp.uy',
            'phone' => null,
            'rut' => null,
            'api_code' => "hiZdtXZebKMZyn98CISN",
            'printer_admin' => '3',
        	'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('users')->insert([
            'name' => "Administrador",
            'email' => 'admin@restoapp',
            'password' => bcrypt('123456'),
            'type' => 'admin',
            'color' => 'red',
            'business_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('hours')->insert([
            'day'   => 'monday',
            'user_id'   => '1',
            'hour_start' => '00:00',
            'hour_end' => '24:00',
            'status' => 'null',
            'business_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('hours')->insert([
            'day'   => 'tuesday',
            'user_id'   => '1',
            'hour_start' => '00:00',
            'hour_end' => '24:00',
            'status' => 'null',
            'business_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('hours')->insert([
            'day'   => 'wednesday',
            'user_id'   => '1',
            'hour_start' => '00:00',
            'hour_end' => '24:00',
            'status' => 'null',
            'business_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('hours')->insert([
            'day'   => 'thursday',
            'user_id'   => '1',
            'hour_start' => '00:00',
            'hour_end' => '24:00',
            'status' => 'null',
            'business_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('hours')->insert([
            'day'   => 'friday',
            'user_id'   => '1',
            'hour_start' => '00:00',
            'hour_end' => '24:00',
            'status' => 'null',
            'business_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('hours')->insert([
            'day'   => 'saturday',
            'user_id'   => '1',
            'hour_start' => '00:00',
            'hour_end' => '24:00',
            'status' => 'null',
            'business_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('hours')->insert([
            'day'   => 'sunday',
            'user_id'   => '1',
            'hour_start' => '00:00',
            'hour_end' => '24:00',
            'status' => 'null',
            'business_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('rooms')->insert([
            'name' => "Salon",
            'business_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('rooms')->insert([
            'name' => "Terraza",
            'business_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('fixeds')->insert([
            'name' => "1",
            'x' => '1',
            'y' => '0',
            'type' => 'standard',
            'status' => 'enabled',
            'room_id' => '1',
            'business_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('fixeds')->insert([
            'name' => "2",
            'x' => '2',
            'y' => '0',
            'type' => 'standard',
            'status' => 'enabled',
            'room_id' => '1',
            'business_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('fixeds')->insert([
            'name' => "3",
            'x' => '4',
            'y' => '0',
            'type' => 'circle',
            'status' => 'enabled',
            'room_id' => '1',
            'business_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('fixeds')->insert([
            'name' => "1",
            'x' => '1',
            'y' => '0',
            'type' => 'circle',
            'status' => 'enabled',
            'room_id' => '2',
            'business_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('fixeds')->insert([
            'name' => "2",
            'x' => '2',
            'y' => '0',
            'type' => 'circle',
            'status' => 'enabled',
            'room_id' => '2',
            'business_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('fixeds')->insert([
            'name' => "3",
            'x' => '3',
            'y' => '0',
            'type' => 'circle',
            'status' => 'enabled',
            'room_id' => '2',
            'business_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('fixeds')->insert([
            'name' => "4",
            'x' => '4',
            'y' => '0',
            'type' => 'circle',
            'status' => 'enabled',
            'room_id' => '2',
            'business_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('suppliers')->insert([
            'name' => 'Proveedor 1',
            'email' => 'proveedor@email.com',
            'rut' => '123456789',
            'address' => 'Dirección proveedor 1',
            'phone' => '095123456',
            'business_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('categories')->insert([
            'name' => 'Entradas',
            'business_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('categories')->insert([
            'name' => 'Platos principales',
            'business_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('categories')->insert([
            'name' => 'Bebidas',
            'business_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('categories')->insert([
            'name' => 'Postres',
            'business_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('printers')->insert([
            'name' => 'Impresora Cocina',
            'address' => '',
            'code'    => '123456',
            'business_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('printers')->insert([
            'name' => 'Impresora Barra',
            'address' => '',
            'code'    => '654321',
            'business_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('printers')->insert([
            'name' => 'Impresora de caja',
            'address' => '',
            'code'    => '159753',
            'business_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('products')->insert([
            'name' => 'Papas fritas comunes',
            'price' => '100',
            'quantity' => null,
            'quantity_min' => null,
            'category_id' => '1',
            'printer_id' => '1',
            'supplier_id' => null,
            'business_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);        

        DB::table('products')->insert([
            'name' => 'Papas 4 quesos',
            'price' => '200',
            'quantity' => null,
            'quantity_min' => null,
            'category_id' => '1',
            'printer_id' => '1',
            'supplier_id' => null,
            'business_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);        

        DB::table('products')->insert([
            'name' => 'Ensalada',
            'price' => '150',
            'quantity' => null,
            'quantity_min' => null,
            'category_id' => '1',
            'printer_id' => '1',
            'supplier_id' => null,
            'business_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);       

        DB::table('products')->insert([
            'name' => 'Pizza muzzarella',
            'price' => '300',
            'quantity' => null,
            'quantity_min' => null,
            'category_id' => '2',
            'printer_id' => '1',
            'supplier_id' => null,
            'business_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);         

        DB::table('products')->insert([
            'name' => 'Hamburguesa completa',
            'price' => '100',
            'quantity' => null,
            'quantity_min' => null,
            'category_id' => '2',
            'printer_id' => '1',
            'supplier_id' => null,
            'business_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);        

        DB::table('products')->insert([
            'name' => 'Petit Entrecot',
            'price' => '380',
            'quantity' => null,
            'quantity_min' => null,
            'category_id' => '2',
            'printer_id' => '1',
            'supplier_id' => null,
            'business_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('products')->insert([
            'name' => 'Fritas Guarnicion',
            'price' => '50',
            'quantity' => null,
            'quantity_min' => null,
            'category_id' => '2',
            'printer_id' => '1',
            'supplier_id' => null,
            'business_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('products')->insert([
            'name' => 'Ensalada mixta guarnicion',
            'price' => '50',
            'quantity' => null,
            'quantity_min' => null,
            'category_id' => '2',
            'printer_id' => '1',
            'supplier_id' => null,
            'business_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('products')->insert([
            'name' => 'Granajo para dos',
            'price' => '480',
            'quantity' => null,
            'quantity_min' => null,
            'category_id' => '2',
            'printer_id' => '1',
            'supplier_id' => null,
            'business_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('products')->insert([
            'name' => 'Coca cola 600ml',
            'price' => '60',
            'quantity' => '50',
            'quantity_min' => '10',
            'category_id' => '3',
            'printer_id' => '2',
            'supplier_id' => null,
            'business_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('products')->insert([
            'name' => 'Fernet con coca',
            'price' => '200',
            'quantity' => null,
            'quantity_min' => null,
            'category_id' => '3',
            'printer_id' => '2',
            'supplier_id' => null,
            'business_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('products')->insert([
            'name' => 'Agua salus 500',
            'price' => '60',
            'quantity' => '100',
            'quantity_min' => '10',
            'category_id' => '3',
            'printer_id' => '2',
            'supplier_id' => null,
            'business_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('products')->insert([
            'name' => 'Torta de chocolate',
            'price' => '150',
            'quantity' => null,
            'quantity_min' => null,
            'category_id' => '4',
            'printer_id' => '1',
            'supplier_id' => null,
            'business_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('products')->insert([
            'name' => 'Helado',
            'price' => '100',
            'quantity' => null,
            'quantity_min' => null,
            'category_id' => '4',
            'printer_id' => '1',
            'supplier_id' => null,
            'business_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('products')->insert([
            'name' => 'Flan con dulce',
            'price' => '150',
            'quantity' => null,
            'quantity_min' => null,
            'category_id' => '4',
            'printer_id' => '1',
            'supplier_id' => null,
            'business_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);      
    }
}

